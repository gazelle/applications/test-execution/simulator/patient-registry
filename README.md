# Patient Registry

## Patient model API
 
 Define Gazelle patient model to be used by the platform applications as well as the different api to implement to define a patient service.

### APIs

 The current supported APIs are :
 - Patient feed service :
 ```java
    String createPatient(Patient patient) throws PatientFeedException;
```
 - Patient retrieve service
 ```java
    Patient retrievePatient(String uuid) throws PatientRetrieveException ;
```
- Patient search service
 ```java
    List<Patient> search(SearchCriteria searchCriteria) throws SearchException;
```
- Patient search with SearchParameters(The mapping of search parameter can be overridden by a file set up with the property `GZL_DICTIONARY_PATIENT_FIELDS` in the `.env` file)
 ```java
    List<Patient> parametrizedSearch(List<SearchParameter> parameters);
```


 - Domain service
 ```java
    boolean exist(String domainIdentifier);
    void createDomain(String domainIdentifier, String domainName);
```
 - XReference service
 ```java
        PatientAliases search(EntityIdentifier sourceIdentifier, List<String> targetDomains) throws SearchCrossReferenceException;
```

### Dependency
 
 To use the model, add the dependency to the library in the `pom.xml` of the client application.
 
 ```xml
    <dependency>
        <groupId>net.ihe.gazelle</groupId>
        <artifactId>patient-registry-model-api</artifactId>
        <version>...</version>
    </dependency>
```

## Patient service

 The patient service implements the different APIs defined in the `Patient model API` with a database.
 

### Service Usage 

 The service can either be deployed in a container or as a java component and instantiated with constructors or injection.
 The different services accessible through a `GITB processing service` (SOAP) are :
 - patient feed
 - patient search
 - XReference search 

### Dependency
 
 To use the service, add the dependency to the library in the `pom.xml` of the client application.
 
 ```xml
    <dependency>
        <groupId>net.ihe.gazelle</groupId>
        <artifactId>patient-registry-service</artifactId>
        <version>...</version>
    </dependency>
```


## Patient search client

The patient search client is a `GITB Processing Service Client` which will point to a `GITB Processing Service`.
It will be used to search Patients depending on specific characteristics

### Client API
  - The client can point to a distant service
  ```java
    /**
     * Default constructor for the class. The Client will create the GITB Processing Service client based on the URL.
     * @param processingServiceURL : URL of the remote Processing Service.
     */
    public PatientSearchClient(URL processingServiceURL)
```
  - The client can point to an instantiated Patient Search Service
 ```java
    /**
     * Constructor used for test purposes.
     * @param processingService : processing service to be used by the PatientSearchClient.
     */
    public PatientSearchClient(ProcessingService processingService)
```


### Dependency
 
 To use the client, add the dependency to the library in the `pom.xml` of the client application.
 
 ```xml
    <dependency>
        <groupId>net.ihe.gazelle</groupId>
        <artifactId>app.patient-registry-search-client</artifactId>
        <version>...</version>
    </dependency>
```

## Patient feed client
The patient search client is a `GITB Processing Service Client` which will point to a `GITB Processing Service`.
It will be used to order the creation of a patient.

### Client API
  - The client can point to a distant service
  ```java
    /**
     * Default constructor for the class. The Client will create the GITB Processing Service client based on the URL.
     * @param processingServiceURL : URL of the remote Processing Service.
     */
    public PatientFeedClient(URL processingServiceURL)
```
  - The client can point to an instantiated Patient Feed Service
 ```java
    /**
     * Constructor used for test purposes.
     * @param processingService : processing service to be used by the PatientFeedClient.
     */
    public PatientFeedClient(ProcessingService processingService)
```

### Dependency
 
 To use the client, add the dependency to the library in the `pom.xml` of the client application.
 
 ```xml
    <dependency>
            <groupId>net.ihe.gazelle</groupId>
            <artifactId>app.patient-registry-feed-client</artifactId>
            <version>...</version>
    </dependency>
```


## XRef search Client 

The CrossReference search client is a `GITB Processing Service Client` which will point to a `GITB Processing Service`.
It will be used to order the search of a crossReference matching the given parameters.


### Client API

- The client can point to a distant service
```java
    /**
     * Default constructor for the class. The Client will create the GITB Processing Service client based on the URL.
     * @param processingServiceURL : URL of the remote Processing Service.
     */
    public XRefSearchClient(URL processingServiceURL)
```

- The client can point to an instantiated Patient Feed Service
 ```java
    /**
     * Constructor used for test purposes.
     * @param processingService : processing service to be used by the XRefSearchClient.
     */
    public XRefSearchClient(ProcessingService processingService)
```

### Dependency

To use the client, add the dependency to the library in the `pom.xml` of the client application.

 ```xml
    <dependency>
            <groupId>net.ihe.gazelle</groupId>
            <artifactId>patient-registry-xref-search-client</artifactId>
            <version>...</version>
    </dependency>
```