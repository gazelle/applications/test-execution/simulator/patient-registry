package net.ihe.gazelle.app.patientregistryfeedclient.adapter;

import io.qameta.allure.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@Feature("Coverage on exception")
@Story("PatientFeedProcessResponseException")
class PatientFeedProcessResponseExceptionTest {

    /**
     * Dummy Test for coverage
     *
     * @throws PatientFeedProcessResponseException testException
     */
    @Test
    @Description(" PatientFeedProcessResponseException")
    @Severity(SeverityLevel.MINOR)
    void generateException() {
        assertThrows(PatientFeedProcessResponseException.class, () -> {
            throw new PatientFeedProcessResponseException("Test");
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientFeedProcessResponseException testException
     */
    @Test
    @Description(" PatientFeedProcessResponseException")
    @Severity(SeverityLevel.MINOR)
    void generateException1() {
        assertThrows(PatientFeedProcessResponseException.class, () -> {
            throw new PatientFeedProcessResponseException("Test", new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientFeedProcessResponseException testException
     */
    @Test
    @Description(" PatientFeedProcessResponseException")
    @Severity(SeverityLevel.MINOR)
    void generateException2() {
        assertThrows(PatientFeedProcessResponseException.class, () -> {
            throw new PatientFeedProcessResponseException(new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientFeedProcessResponseException testException
     */
    @Test
    @Description(" PatientFeedProcessResponseException")
    @Severity(SeverityLevel.MINOR)
    void generateException4() {
        assertThrows(PatientFeedProcessResponseException.class, () -> {
            throw new PatientFeedProcessResponseException("Test", new IllegalArgumentException("test"), false, true);
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientFeedProcessResponseException testException
     */
    @Test
    @Description(" PatientFeedProcessResponseException")
    @Severity(SeverityLevel.MINOR)
    void generateException3() {
        assertThrows(PatientFeedProcessResponseException.class, () -> {
            throw new PatientFeedProcessResponseException();
        });
    }


}