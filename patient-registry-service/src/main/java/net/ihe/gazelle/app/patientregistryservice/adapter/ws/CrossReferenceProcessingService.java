package net.ihe.gazelle.app.patientregistryservice.adapter.ws;

import com.gitb.ps.Void;
import com.gitb.ps.*;
import net.ihe.gazelle.app.patientregistryapi.adapter.ws.CrossReferenceProcessingWebServiceConstants;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;

@Stateless
@WebService(
        name = CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PROCESSING_RESOURCE_URL,
        serviceName = CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PROCESSING_SERVICE,
        portName = CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PROCESSING_SERVICE_PORT,
        targetNamespace = "http://www.gitb.com/ps/v1/",
        endpointInterface = "com.gitb.ps.ProcessingService")
public class CrossReferenceProcessingService implements ProcessingService {
    private static final GazelleLogger LOGGER = GazelleLoggerFactory.getInstance().getLogger(CrossReferenceProcessingService.class);
    private static final String UNSUPPORTED_OPERATION = "Unsupported Operation";

    @Inject
    CrossReferenceSearchProcessingService crossReferenceSearchProcessingService;

    /**
     * Default constructor, used for injection.
     */
    public CrossReferenceProcessingService() {
    }

    /**
     * Constructor with dependencies.
     *
     * @param crossReferenceSearchProcessingService application service to use to process X-Ref search requests.
     */
    public CrossReferenceProcessingService(CrossReferenceSearchProcessingService crossReferenceSearchProcessingService) {
        this.crossReferenceSearchProcessingService = crossReferenceSearchProcessingService;
    }

    @Override
    public GetModuleDefinitionResponse getModuleDefinition(Void parameters) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION);
    }

    @Override
    public ProcessResponse process(ProcessRequest processRequest) {
        LOGGER.info("The CrossReference GITB Service received a request");
        if (processRequest != null) {
            if (CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_SEARCH_OPERATION.equals(processRequest.getOperation())) {
                LOGGER.info("The CrossReference GITB Service received a Search XReference request");
                return crossReferenceSearchProcessingService.processPatientCrossReferenceSearch(processRequest);
            } else {
                LOGGER.error("ProcessRequest.operation shall be %s !",
                        CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_SEARCH_OPERATION);
                throw new UnsupportedOperationException(String.format("ProcessRequest.operation shall be %s !",
                        CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_SEARCH_OPERATION));
            }
        } else {
            throw new IllegalArgumentException("ProcessRequest shall not be null !");
        }
    }

    @Override
    public BeginTransactionResponse beginTransaction(BeginTransactionRequest parameters) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION);
    }

    @Override
    public Void endTransaction(BasicRequest parameters) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION);
    }
}
