package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service;

import net.ihe.gazelle.app.patientregistryservice.adapter.dao.EntityManagerProducer;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientIdentifierDB;
import net.ihe.gazelle.app.patientregistryservice.application.dao.DomainDAO;
import net.ihe.gazelle.app.patientregistryservice.application.dao.IdentifierDAO;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.DomainSearchException;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.PatientIdentifierException;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.*;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 * DAO for Identifier Objects.
 */
@Named("IdentifierDAO")
public class IdentifierDAOImpl implements IdentifierDAO {

    public static final String ERROR_IN_THE_SOURCE_IDENTIFIER_IT_DOES_NOT_MATCH_ANY_IDENTITY = "Error in the sourceIdentifier : it does not match " +
            "any identity";
    private EntityManager entityManager;
    private DomainDAO domainDAO;

    /**
     * Private constructor to hide the implicit one.
     */
    private IdentifierDAOImpl() {
    }

    /**
     * Constructor for the class. Takes an EntityManager as argument. It will be used to perform actions on Database.
     *
     * @param entityManager {@link EntityManager} to perform actions on Database.
     */
    @Inject
    public IdentifierDAOImpl(@EntityManagerProducer.InjectEntityManager EntityManager entityManager, DomainDAO domainDAO) {
        if (entityManager == null || domainDAO == null) {
            throw new IllegalArgumentException("EntityManager cannot be null");
        }
        this.entityManager = entityManager;
        this.domainDAO = domainDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PatientIdentifierDB retrieve(String fullPatientIdentifier, String identifierTypeCode) throws PatientIdentifierException {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(PatientIdentifierDB.class);
        From from = criteriaQuery.from(PatientIdentifierDB.class);


        Path<String> fullPatientIdPath = from.get("fullPatientId");
        Path<String> identifierTypeCodePath = from.get("identifierTypeCode");

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(criteriaBuilder.equal(fullPatientIdPath, fullPatientIdentifier));
        predicates.add(criteriaBuilder.equal(identifierTypeCodePath, identifierTypeCode));

        Predicate finalPredicate = criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));

        TypedQuery<PatientIdentifierDB> query = entityManager.createQuery(criteriaQuery.select(from).where(finalPredicate).distinct(true));
        try {
            return query.getSingleResult();
        } catch (NonUniqueResultException e) {
            throw new PatientIdentifierException(ERROR_IN_THE_SOURCE_IDENTIFIER_IT_DOES_NOT_MATCH_ANY_IDENTITY, e);
        }catch (NoResultException exception){
            return null;
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public PatientIdentifierDB searchForPatientIdentifier(Integer hierarchicDesignatorDBID, String idNumber) throws PatientIdentifierException {
        try {
            TypedQuery<PatientIdentifierDB> queryForAPatient = entityManager.createNamedQuery("PatientIdentifierDB.findByHierarchicDomainAndId",
                    PatientIdentifierDB.class);

            queryForAPatient.setParameter("domain_id", hierarchicDesignatorDBID);
            queryForAPatient.setParameter("id_number", idNumber);
            return queryForAPatient.getSingleResult();
        } catch (NoResultException exception) {
            throw new PatientIdentifierException(ERROR_IN_THE_SOURCE_IDENTIFIER_IT_DOES_NOT_MATCH_ANY_IDENTITY);
        } catch (NonUniqueResultException exception) {
            throw new PatientIdentifierException("Duplicated PatientIdentifier");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PatientIdentifierDB createPatientIdentifier(PatientIdentifierDB patientIdentifierDB) throws PatientIdentifierException {
        try {
            PatientIdentifierDB patientIdentifierFromDB =  retrieve(patientIdentifierDB.getFullPatientId(), patientIdentifierDB.getIdentifierTypeCode());
            if(patientIdentifierFromDB == null){
                entityManager.persist(patientIdentifierDB);
                entityManager.flush();
                return patientIdentifierDB;
            }
            return patientIdentifierFromDB;
        } catch (PatientIdentifierException exception) {
            throw new PatientIdentifierException("Unable to create new PatientIdentifier", exception);
        } catch (PersistenceException exception) {
            throw new PatientIdentifierException("Unable to create new PatientIdentifier", exception);
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public PatientIdentifierDB readPatientIdentifier(Integer patientIdentifierId) throws PatientIdentifierException {
        if (patientIdentifierId == null) {
            throw new IllegalArgumentException("null parameter");
        }
        try {
            TypedQuery<PatientIdentifierDB> queryForAPatient =
                    entityManager.createQuery("select pi from PatientIdentifierDB pi where pi.id=" + patientIdentifierId,
                            PatientIdentifierDB.class);
            return queryForAPatient.getSingleResult();
        } catch (NonUniqueResultException | NoResultException exception) {
            throw new PatientIdentifierException(ERROR_IN_THE_SOURCE_IDENTIFIER_IT_DOES_NOT_MATCH_ANY_IDENTITY);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deletePatientIdentifier(String identifierValue, String systemIdentifier) throws PatientIdentifierException {
        try {
            entityManager.remove(searchForPatientIdentifier(domainDAO.searchForHierarchicDesignator(systemIdentifier), identifierValue));
            entityManager.flush();
        } catch (PersistenceException | DomainSearchException e) {
            throw new PatientIdentifierException("Unexpected Exception deleting PatientIdentifier !", e);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deletePatientIdentifier(Integer identifierID) throws PatientIdentifierException {
        try {
            PatientIdentifierDB patientIdentifierDB = readPatientIdentifier(identifierID);
            entityManager.remove(patientIdentifierDB);
            entityManager.flush();
        } catch (PatientIdentifierException exception) {
            throw new PatientIdentifierException("Error during deletion ", exception);
        }
    }

}
