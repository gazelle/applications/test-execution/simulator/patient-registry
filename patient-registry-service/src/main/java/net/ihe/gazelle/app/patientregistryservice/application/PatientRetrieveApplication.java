package net.ihe.gazelle.app.patientregistryservice.application;

import net.ihe.gazelle.app.patientregistryapi.application.PatientRetrieveException;
import net.ihe.gazelle.app.patientregistryapi.application.PatientRetrieveService;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryservice.application.dao.PatientDAO;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.PatientNotFoundException;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.PatientReadException;

import javax.inject.Inject;

/**
 * Implementation of PatientRetrieveService.
 */
public class PatientRetrieveApplication implements PatientRetrieveService {

    PatientDAO patientDAO;

    /**
     * Default constructor for the class.
     *
     * @param patientDAO DAO used to read patients.
     */
    @Inject
    public PatientRetrieveApplication(PatientDAO patientDAO) {
        this.patientDAO = patientDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Patient retrievePatient(String uuid) throws PatientRetrieveException {
        try {
            return patientDAO.readPatient(uuid);
        } catch (PatientReadException e) {
            throw new PatientRetrieveException("Exception trying to retrieve Patient !", e);
        } catch (PatientNotFoundException e) {
            throw new PatientRetrieveException("Patient not found in Registry !", e);
        }
    }
}
