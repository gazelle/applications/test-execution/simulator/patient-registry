package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.converter;

import net.ihe.gazelle.app.patientregistryapi.business.*;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.GenderCodeDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;

import java.util.Date;
import java.util.List;

/**
 * Converter for Names from business model to Database model and back.
 */
public class PatientConverter {
    private static PatientConverter INSTANCE;
    /**
     * Private constructor to hide implicit one.
     */
    private PatientConverter(){
    }

    public static PatientConverter getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new PatientConverter();
        }

        return INSTANCE;
    }
    /**
     * Map a business {@link Patient} object to a corresponding to {@link PatientDB} entity to use during Database access.
     * @param patient : Patient to map
     * @return mapped {@link PatientDB}
     */
    public static PatientDB toPatientDB(Patient patient) {
        PatientDB patientDB = new PatientDB();
        patientDB.setUuid(patient.getUuid());
        patient.getIdentifiers().forEach((identifier -> IdentifierConverter.toPatientIdentifierDB(identifier, patientDB)));
        patientDB.setStillActive(patient.isActive());
        if (patient.getMultipleBirthOrder() != null && patient.getMultipleBirthOrder() > 0) {
            patientDB.setMultipleBirthIndicator(true);
            patientDB.setBirthOrder(patient.getMultipleBirthOrder());
        }
        List<PersonName> names = patient.getNames();
        if(!names.isEmpty()) {
            NameConverter.mapNameToPatientDB(names.get(0), patientDB);
        }

        patientDB.setGenderCode(GenderCodeDB.fromGenderCode(patient.getGender()));
        patientDB.setDateOfBirth(patient.getDateOfBirth());

        Date dateOfDeath = patient.getDateOfDeath();
        if (dateOfDeath != null) {
            patientDB.setPatientDead(true);
            patientDB.setPatientDeathTime(dateOfDeath);
        }

        patient.getContactPoints().forEach((contactPoint -> ContactPointConverter.toContactPointDB(contactPoint,patientDB)));
        patient.getAddresses().forEach((address -> AddressConverter.toAddressDB(address, patientDB)));
        patient.getObservations().forEach((observation -> mapObservationToPatientDB(observation,patientDB)));

        return patientDB;
    }

    /**
     * Map a business {@link Observation} object to corresponding fields in {@link PatientDB} entity to use during Database access.
     * @param observation : observation to map
     * @param patient : Patient entity where to map
     */
    private static void mapObservationToPatientDB(Observation observation, PatientDB patient) {
        switch (observation.getObservationType()) {
            case RACE:
                patient.setRaceCode(observation.getValue());
                break;
            case WEIGHT:
                patient.setWeight(Integer.parseInt(observation.getValue()));
                break;
            case HEIGHT:
                patient.setSize(Integer.parseInt(observation.getValue()));
                break;
            case RELIGION:
                patient.setReligionCode(observation.getValue());
                break;
            case BLOOD_GROUP:
                patient.setBloodGroup(observation.getValue());
                break;
            case MARITAL_STATUS:
                patient.setMaritalStatus(observation.getValue());
                break;
            case MOTHERS_MAINDEN_NAME:
                patient.setMotherMaidenName(observation.getValue());
                break;
            case VIP_INDICATOR:
                patient.setVipIndicator(observation.getValue());
                break;
            case IDENTITY_RELIABILITY_CODE:
                patient.setIdentityReliabilityCode(observation.getValue());
                break;
            default:
        }
    }

    /**
     * Map a {@link PatientDB} entity to a business {@link Patient} object.
     * @param patientDB : entity to map
     * @return mapped business object.
     */
    public static Patient toPatient(PatientDB patientDB) {
        Patient patient = new Patient();
        patient.setUuid(patientDB.getUuid());
        //If all entries in the list of patient identifiers are eliminated, which would leave PID-3
        //empty, then the corresponding PID segment group shall not be present in the response at all.
        patientDB.getPatientIdentifiers().forEach(patientIdentifierDB -> patient.addIdentifier(IdentifierConverter.toPatientIdentifier(patientIdentifierDB)));

        PersonName personName = NameConverter.mapPatientDbToName(patientDB);
        if (personName.getFamily()!=null || personName.getPrefix()!=null || personName.getSuffix()!=null
                || personName.getUse()!=null || !personName.getGivens().isEmpty()){
            patient.addName(personName);
        }

        patient.setActive(patientDB.getStillActive());

        if(patientDB.getGenderCode() != null) {
            patient.setGender(patientDB.getGenderCode().toGenderCode());
        }
        patient.setDateOfBirth(patientDB.getDateOfBirth());
        if (patientDB.getBirthOrder() != null) {
            patient.setMultipleBirthOrder(Integer.valueOf(patientDB.getBirthOrder().toString()));
        }
        patient.setDateOfDeath(patientDB.getPatientDeathTime());

        patientDB.getAddressList().forEach(patientAddress -> patient.addAddress(AddressConverter.toAddress(patientAddress)));
        patientDB.getPhoneNumbers().forEach(patientPhoneNumber -> patient.addContactPoint(ContactPointConverter.toContactPoint(patientPhoneNumber)));

        if (patientDB.getEmail() != null && !patientDB.getEmail().isEmpty()) {
            patient.addContactPoint(new ContactPoint(ContactPointType.EMAIL, ContactPointUse.EMAIL, patientDB.getEmail()));
        }
        addObservationIfValued(patient, patientDB.getAccountNumber(), ObservationType.ACCOUNT_NUMBER);
        addObservationIfValued(patient, patientDB.getMotherMaidenName(), ObservationType.MOTHERS_MAINDEN_NAME);
        addObservationIfValued(patient, patientDB.getBirthPlaceName(), ObservationType.BIRTHPLACE_NAME);
        addObservationIfValued(patient, patientDB.getBloodGroup(), ObservationType.BLOOD_GROUP);
        addObservationIfValued(patient, patientDB.getReligionCode(), ObservationType.RELIGION);
        addObservationIfValued(patient, patientDB.getRaceCode(), ObservationType.RACE);
        addObservationIfValued(patient, patientDB.getMaritalStatus(), ObservationType.MARITAL_STATUS);
        addObservationIfValued(patient, patientDB.getWeight(), ObservationType.WEIGHT);
        addObservationIfValued(patient, patientDB.getSize(), ObservationType.HEIGHT);
        return patient;
    }

    /**
     * Map corresponding value to a business {@link Observation} object.
     * @param patient : Patient where to map
     * @param value : value of the observation
     * @param observationTypeEnum : type of the observation
     */
    private static void addObservationIfValued(Patient patient, Object value,
                                        ObservationType observationTypeEnum) {
        if (value != null) {
                Observation observation = new Observation(observationTypeEnum, value.toString());
                patient.addObservation(observation);
        }
    }
}
