package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient;

import net.ihe.gazelle.app.patientregistryapi.application.PatientFieldsMatcherService;
import net.ihe.gazelle.app.patientregistryapi.application.SearchCrossReferenceException;
import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import net.ihe.gazelle.app.patientregistryapi.application.SearchQueryBuilder;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.EntityManagerProducer;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.converter.PatientConverter;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientIdentifierDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.references.CrossReferenceDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search.PatientSearchDAO;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search.SearchQueryBuilderImpl;
import net.ihe.gazelle.app.patientregistryservice.application.dao.DomainDAO;
import net.ihe.gazelle.app.patientregistryservice.application.dao.IdentifierDAO;
import net.ihe.gazelle.app.patientregistryservice.application.dao.PatientCrossReferenceDAO;
import net.ihe.gazelle.app.patientregistryservice.application.dao.PatientDAO;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.*;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Implementation of the Patient DAO
 */
@Named("PatientDAO")
public class PatientDAOImpl implements PatientDAO {

    public static final String PAT_IDENTITY_X_REF_MGR = "PAT_IDENTITY_X_REF_MGR";
    public static final String ERROR_PATIENT_UUID_CAN_NOT_BE_NULL_OR_EMPTY = "Error : Patient uuid can not be null or empty ";
    public static final String UNEXPECTED_EXCEPTION_DELETING_PATIENT = "Unexpected Exception deleting Patient !";
    public static final String ERROR_PATIENT_CAN_NOT_BE_NULL = "Error : Patient can not be null";
    private static final GazelleLogger log = GazelleLoggerFactory.getInstance().getLogger(PatientDAOImpl.class);
    SearchQueryBuilder<PatientDB> searchQueryBuilder;
    private EntityManager entityManager;
    private DomainDAO domainDAO;
    private IdentifierDAO identifierDAO;
    private PatientSearchDAO patientSearchDAO;
    private PatientCrossReferenceDAO patientCrossReferenceDAO;


    /**
     * Private Constructor to hide implicit one.
     */
    private PatientDAOImpl() {
    }

    /**
     * Complete constructor, also used for injection
     *
     * @param entityManager    {@link EntityManager} to use by DAO.
     * @param patientSearchDAO {@link PatientSearchDAO} concrete SearchDAO for Patients.
     * @param domainDAO        {@link DomainDAO} to retrieve Domains from database.
     * @param identifierDAO    {@link IdentifierDAO} to retrieve Identifiers from database.
     */
    @Inject
    public PatientDAOImpl(@EntityManagerProducer.InjectEntityManager EntityManager entityManager, PatientSearchDAO patientSearchDAO,
                          DomainDAO domainDAO, IdentifierDAO identifierDAO, PatientCrossReferenceDAO patientCrossReferenceDAO, SearchQueryBuilder<PatientDB> searchQueryBuilder) {
        this.entityManager = entityManager;
        this.patientSearchDAO = patientSearchDAO;
        this.domainDAO = domainDAO;
        this.identifierDAO = identifierDAO;
        this.patientCrossReferenceDAO = patientCrossReferenceDAO;
        this.searchQueryBuilder = searchQueryBuilder;

    }

    private static void checkIfMalformed(String identifier, PatientIdentifierDB patientIdentifierDB) throws PatientCreationException {
        if (StringUtils.isEmpty(identifier) || patientIdentifierDB.getDomain() == null || patientIdentifierDB.getDomain().getUniversalID() == null) {
            throw new PatientCreationException("One patientIdentifier is malformed");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPatientExisting(String uuid) throws PatientReadException {
        if (uuid == null || uuid.isBlank()) {
            throw new IllegalArgumentException(ERROR_PATIENT_UUID_CAN_NOT_BE_NULL_OR_EMPTY);
        }
        try {
            TypedQuery<String> query = entityManager.createNamedQuery("PatientDB.findUUID", String.class);
            query.setParameter("uuid", uuid);
            query.getSingleResult();
            return true;
        } catch (NoResultException e) {
            return false;
        } catch (NonUniqueResultException e) {
            log.warn(String.format("Multiple Patients exists with UUID : %s", uuid));
            return true;
        } catch (PersistenceException e) {
            throw new PatientReadException("Unexpected Persistence Exception !", e);
        }
    }

    @Override
    public void createPatientDB(PatientDB patientDB) throws PatientCreationException {
        if (patientDB == null) {
            throw new IllegalArgumentException(ERROR_PATIENT_CAN_NOT_BE_NULL);
        }
        try {
            patientDB.setPatientIdentifiers(dealWithPatientIdentifier(patientDB.getPatientIdentifiers()));
            entityManager.persist(patientDB);
            entityManager.flush();
        } catch (PersistenceException e) {
            throw new PatientCreationException("Unexpected Exception persisting Patient !", e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createPatient(Patient patient) throws PatientCreationException {
        if (patient == null) {
            throw new IllegalArgumentException(ERROR_PATIENT_CAN_NOT_BE_NULL);
        }
        try {
            PatientDB patientDB = PatientConverter.toPatientDB(patient);
            patientDB.getPatientIdentifiers().addAll(dealWithPatientIdentifier(patientDB.getPatientIdentifiers()));
            entityManager.persist(patientDB);
            entityManager.flush();
        } catch (PersistenceException e) {
            throw new PatientCreationException("Unexpected Exception persisting Patient !", e);
        }
    }

    /**
     * This private method deals with the list of the Patient Identifier.
     *
     * @param patientIdentifierDBS List of PatientIdentifier
     * @return List of identifier from DB
     * @throws PatientCreationException If an error occurred to define correct
     */
    private List<PatientIdentifierDB> dealWithPatientIdentifier(List<PatientIdentifierDB> patientIdentifierDBS) throws PatientCreationException {
        Iterator<PatientIdentifierDB> iterator = patientIdentifierDBS.iterator();
        List<PatientIdentifierDB> identifiersFromDB = new ArrayList<>();
        while (iterator.hasNext()) {
            PatientIdentifierDB patientIdentifierDB = iterator.next();
            String identifier = patientIdentifierDB.getIdentifier();
            checkIfMalformed(identifier, patientIdentifierDB);
            try {
                PatientIdentifierDB patientIdentifierFromDB;
                patientIdentifierDB.setDomain(domainDAO.retrieve(patientIdentifierDB.getDomain().getUniversalID()));
                patientIdentifierDB.setFullPatientIdentifierIfEmpty();
                patientIdentifierFromDB = identifierDAO.retrieve(patientIdentifierDB.getFullPatientId(),
                        patientIdentifierDB.getIdentifierTypeCode());
                if (patientIdentifierFromDB == null) {
                    patientIdentifierFromDB = identifierDAO.createPatientIdentifier(patientIdentifierDB);
                }
                identifiersFromDB.add(patientIdentifierFromDB);
                iterator.remove();
            } catch (DomainSearchException e) {
                throw new PatientCreationException("System not found with given UniversalID", e);
            } catch (PatientIdentifierException exception) {
                String msg = exception.getCause() instanceof NonUniqueResultException
                        ? "The PatientIdentifier is duplicated in database"
                        : "Error During Patient Creation";
                throw new PatientCreationException(msg, exception);
            }
        }
        return identifiersFromDB;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Patient> readPatient(SearchCriteria searchCriteria) throws PatientReadException {
        try {
            return patientSearchDAO.search(searchCriteria);
        } catch (SearchException e) {
            throw new PatientReadException("Exception performing the read operation for requested criteria !", e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public List<Patient> searchPatients(List<SearchParameter> parameters) throws PatientSearchException {
        try {
            if (parameters == null) {
                throw new IllegalArgumentException("Error : Search parameters can not be null");
            }
            TypedQuery<PatientDB> query = searchQueryBuilder.build(parameters);
            List<PatientDB> patientDBS = query.getResultList();
            List<Patient> patients = new ArrayList<>();

            for (PatientDB patientDB : patientDBS) {
                patients.add(PatientConverter.toPatient(patientDB));
            }

            return patients;

        } catch (SearchException e) {
            throw new PatientSearchException("Error during search", e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Patient readPatient(String uuid) throws PatientReadException, PatientNotFoundException {
        if (uuid == null || uuid.isBlank()) {
            throw new IllegalArgumentException(ERROR_PATIENT_UUID_CAN_NOT_BE_NULL_OR_EMPTY);
        }
        try {
            PatientDB patient = readPatientDB(uuid);
            return PatientConverter.toPatient(patient);
        } catch (PersistenceException e) {
            throw new PatientReadException("Unexpected Persistence Exception !", e);
        }
    }


    /**
     * Read a {@link PatientDB} from database based on its UUID
     *
     * @param uuid value of the Patient's UUID
     * @return the {@link PatientDB}
     * @throws PatientNotFoundException if an error occurred during the search
     * @throws PatientReadException     if an error occurred during the search
     */
    @Override
    public PatientDB readPatientDB(String uuid) throws PatientReadException, PatientNotFoundException {
        if (StringUtils.isBlank(uuid)) {
            throw new IllegalArgumentException(ERROR_PATIENT_UUID_CAN_NOT_BE_NULL_OR_EMPTY);
        }
        try {
            TypedQuery<PatientDB> query = entityManager.createNamedQuery("PatientDB.findByUuid", PatientDB.class);
            query.setParameter("uuid", uuid);
            return query.getSingleResult();
        } catch (NonUniqueResultException e) {
            throw new PatientReadException(String.format("Found multiple results for uuid : %s", uuid), e);
        } catch (NoResultException e) {
            throw new PatientNotFoundException(String.format("No result found for uuid : %s", uuid), e);
        }

    }

    @Override
    public PatientDB readFirstPatientByIdentifier(EntityIdentifier identifier) {
        List<PatientDB> patientDBS = getPatientDBS(identifier);
        if (patientDBS == null) return null;
        return patientDBS.get(0); // FIXME : Handle if many results
    }

    @Override
    public List<PatientDB> readAllPatientsByIdentifier(EntityIdentifier identifier) {
        return getPatientDBS(identifier);
    }

    private List<PatientDB> getPatientDBS(EntityIdentifier identifier) {
        TypedQuery<PatientDB> query;
        if (identifier.getSystemIdentifier() != null) {
            query = entityManager.createNamedQuery("PatientDB.findByPatientIdentifierAndSystem", PatientDB.class);
            query.setParameter("system_identifier", identifier.getSystemIdentifier());
        } else {
            query = entityManager.createNamedQuery("PatientDB.findByPatientIdentifier", PatientDB.class);
        }
        query.setParameter("patient_identifier", identifier.getValue());
        List<PatientDB> patientDBS = query.getResultList();
        if (patientDBS.isEmpty()) {
            return null;
        }
        return patientDBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deletePatient(Patient patient) throws PatientDeleteException {
        if (patient == null) {
            throw new IllegalArgumentException(ERROR_PATIENT_CAN_NOT_BE_NULL);
        }
        String uuid = patient.getUuid();
        if (uuid == null || uuid.isBlank()) {
            throw new IllegalArgumentException(ERROR_PATIENT_UUID_CAN_NOT_BE_NULL_OR_EMPTY);
        }
        try {
            entityManager.remove(readPatientDB(uuid));
            entityManager.flush();
        } catch (PersistenceException | PatientReadException | PatientNotFoundException e) {
            throw new PatientDeleteException(UNEXPECTED_EXCEPTION_DELETING_PATIENT, e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deletePatient(String patientUuid) throws PatientDeleteException {
        if (StringUtils.isEmpty(patientUuid)) {
            throw new IllegalArgumentException(ERROR_PATIENT_UUID_CAN_NOT_BE_NULL_OR_EMPTY);
        }
        try {
            PatientDB patientDB = readPatientDB(patientUuid);
            if (patientDB.getPixReference() != null) {
                handleDeletionCrossReferenceForPatient(patientDB);
            }
            handlePatientIdentifierDeletion(patientDB);
            entityManager.remove(patientDB);
            entityManager.flush();
        } catch (PersistenceException | PatientReadException | PatientNotFoundException e) {
            throw new PatientDeleteException(UNEXPECTED_EXCEPTION_DELETING_PATIENT, e);
        }
    }

    @Override
    public void deletePatientDB(PatientDB patientDB) throws PatientDeleteException {
        if (patientDB == null) {
            throw new IllegalArgumentException(ERROR_PATIENT_CAN_NOT_BE_NULL);
        }
        try {
            if (patientDB.getPixReference() != null) {
                handleDeletionCrossReferenceForPatient(patientDB);
            }
            handlePatientIdentifierDeletion(patientDB);
            entityManager.remove(patientDB);
            entityManager.flush();
        } catch (PersistenceException e) {
            throw new PatientDeleteException(UNEXPECTED_EXCEPTION_DELETING_PATIENT, e);
        }
    }

    private void handlePatientIdentifierDeletion(PatientDB patientDB) throws PatientDeleteException {
        try {
            List<PatientIdentifierDB> patientIdentifiers = new ArrayList<>(patientDB.getPatientIdentifiers());
            for (PatientIdentifierDB patientIdentifierDB : patientIdentifiers) {
                breakLinkBetweenPatientAndPatientIdentifier(patientDB, patientIdentifierDB);
            }
        } catch (PersistenceException exception) {
            throw new PatientDeleteException("Error during PatientIdentifier deletion", exception);
        }
    }

    private void breakLinkBetweenPatientAndPatientIdentifier(PatientDB patientDB, PatientIdentifierDB patientIdentifierDB) throws PersistenceException {
        patientDB.removePatientIdentifiers(patientIdentifierDB);
        if (patientIdentifierDB.getPatients().isEmpty()) {
            entityManager.remove(patientIdentifierDB);
        }
    }

    private void handleDeletionCrossReferenceForPatient(PatientDB patientDB) throws PatientDeleteException {
        try {
            CrossReferenceDB crossReferenceDBS = patientCrossReferenceDAO.searchForPatientAliasesWithPatientDB(patientDB.getPixReference().getId());
            deleteEmptyCrossReference(crossReferenceDBS, patientDB);
        } catch (SearchCrossReferenceException | DeletionCrossReferenceException searchCrossReferenceException) {
            throw new PatientDeleteException("Error During Cross-reference Deletion", searchCrossReferenceException);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PatientDB updatePatientDB(PatientDB patientDB) throws PatientUpdateException {
        if (patientDB == null) {
            throw new IllegalArgumentException(ERROR_PATIENT_CAN_NOT_BE_NULL);
        }
        try {
            patientDB.setPatientIdentifiers(dealWithPatientIdentifier(patientDB.getPatientIdentifiers()));
            entityManager.merge(patientDB);
            entityManager.flush();
            return patientDB;
        } catch (PersistenceException exception) {
            throw new PatientUpdateException("Unexpected UpdateException", exception);
        } catch (PatientCreationException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * For each crossRef, delete it if the patient given as parameter is the only member of this crossRef
     *
     * @param crossReferenceDB to Proceed
     * @param patientDB        Patient that will be deleted
     * @throws DeletionCrossReferenceException if an error occurred
     */
    private void deleteEmptyCrossReference(CrossReferenceDB crossReferenceDB, PatientDB patientDB) throws DeletionCrossReferenceException {

        if (crossReferenceDB.getPatients().size() == 1 && crossReferenceDB.getPatients().contains(patientDB)) {
            patientCrossReferenceDAO.deleteCrossReference(crossReferenceDB.getId());
        } else {
            patientDB.removePixReference();
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PatientDB> retrieveMatchingPatients(PatientDB patientDB) throws PatientNotFoundException {
        if (patientDB == null) {
            throw new IllegalArgumentException(ERROR_PATIENT_CAN_NOT_BE_NULL);
        }
        try {
            TypedQuery<PatientDB> query = entityManager.createNamedQuery("PatientDB.findForSimulatedActorMatchingPatient", PatientDB.class);
            query.setParameter("patientGenderCode", patientDB.getGenderCode());
            query.setParameter("patientBirth", patientDB.getDateOfBirth());
            query.setParameter("actorKeyword", PAT_IDENTITY_X_REF_MGR);
            return query.getResultList();
        } catch (NoResultException e) {
            throw new PatientNotFoundException("No result found for patient", e);
        }
    }
}
