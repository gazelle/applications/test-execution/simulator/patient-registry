package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.converter;

import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PatientAliases;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.references.CrossReferenceDB;

import java.util.ArrayList;
import java.util.List;

/**
 * Converter for Names from business model to Database model and back.
 */
public class PatientAliasesConverter {

    /**
     * Private constructor to hide implicit one.
     */
    private PatientAliasesConverter() {
    }

    /**
     * Method to convert Patient Aliases to CrossReferenceDB
     *
     * @param patientAliases Xref object to be converted
     * @return CrossReferenceDB
     */
    public static CrossReferenceDB toCrossReferenceDB(PatientAliases patientAliases) {
        CrossReferenceDB crossReferenceDB = new CrossReferenceDB();

        List<PatientDB> patientDB = new ArrayList<>();
        for (Patient patient : patientAliases.getMembers()) {
            patientDB.add(PatientConverter.toPatientDB(patient));
        }
        crossReferenceDB.setPatients(patientDB);
        crossReferenceDB.setLastModifier(patientAliases.getLastModifier());
        crossReferenceDB.setLastChanged(patientAliases.getLastModifierDateTime());

        return crossReferenceDB;
    }

    /**
     * Method to convert  CrossReferenceDB to Patient Aliases
     *
     * @param crossReferenceDB to be converted
     * @return PatientAliases
     */
    public static PatientAliases toPatientAliases(CrossReferenceDB crossReferenceDB) {
        PatientAliases patientaliases = new PatientAliases();

        List<Patient> patients = new ArrayList<>();
        for (PatientDB patientDB : crossReferenceDB.getPatients()) {
            patients.add(PatientConverter.toPatient(patientDB));
        }
        patientaliases.setMembers(patients);
        patientaliases.setLastModifier(crossReferenceDB.getLastModifier());
        patientaliases.setLastModifierDateTime();

        return patientaliases;
    }

    /**
     * Convert a list of PatientAliases
     *
     * @param crossReferenceDBS list to convert
     * @return a List of PatientAliases
     */
    public static List<PatientAliases> toListOfPatientAliases(List<CrossReferenceDB> crossReferenceDBS) {
        List<PatientAliases> patientAliases = new ArrayList<>();
        crossReferenceDBS.forEach(crossReference -> {
            PatientAliases patientAlias = toPatientAliases(crossReference);
            patientAliases.add(patientAlias);
        });
        return patientAliases;
    }
}
