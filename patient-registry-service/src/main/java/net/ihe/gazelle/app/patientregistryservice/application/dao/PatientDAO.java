package net.ihe.gazelle.app.patientregistryservice.application.dao;


import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.*;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;

import java.util.List;

/**
 * API for the Patient DAO
 */
public interface PatientDAO {

    /**
     * Checks if a patient exists with the requested UUID.
     *
     * @param uuid UUID to check
     * @return a boolean indicating whether or not a Patient exists with the requested UUID.
     * @throws PatientReadException if the Database Request fails.
     */
    boolean isPatientExisting(String uuid) throws PatientReadException;

    /**
     * Create a Patient in database
     *
     * @param patient Business Patient to persist in Database.
     * @throws PatientCreationException if the persist operation fails.
     */
    void createPatient(Patient patient) throws PatientCreationException;

    /**
     * Create a Patient in database
     *
     * @param patientDB  Patient to persist in Database.
     * @throws PatientCreationException if the persist operation fails.
     */
    void createPatientDB(PatientDB patientDB) throws PatientCreationException;

    /**
     * Search for Patients matching given criteria.
     *
     * @param searchCriteria Criteria to match for returned Patients.
     * @return the list of Patients matching requested criteria.
     * @throws PatientReadException if the Search request fails.
     */
    List<Patient> readPatient(SearchCriteria searchCriteria) throws PatientReadException;


    /**
     * Search for Patients matching given parameters.
     *
     * @param parameters Parameters to match for returned Patients.
     * @return the list of Patients matching requested criteria.
     */
    List<Patient> searchPatients(List<SearchParameter> parameters) throws PatientSearchException;


    /**
     * Search for Patients with a specific UUID.
     *
     * @param uuid UUID of the patient
     * @return the Patients matching requested UUID.
     * @throws PatientReadException     if the Search request fails.
     * @throws PatientNotFoundException if the Patient does not exist.
     */
    Patient readPatient(String uuid) throws PatientReadException, PatientNotFoundException;

    /**
     * Search for Patients with a specific UUID.
     *
     * @param uuid UUID of the patient
     * @return the Patients matching requested UUID.
     * @throws PatientReadException     if the Search request fails.
     * @throws PatientNotFoundException if the Patient does not exist.
     */
    PatientDB readPatientDB(String uuid) throws PatientReadException, PatientNotFoundException;


    /**
     * Delete a patient from Database
     *
     * @param patient Patient to delete from base.
     * @throws PatientDeleteException if the delete request fails.
     */
    void deletePatient(Patient patient) throws PatientDeleteException;

    /**
     * Delete a patient from Database
     *
     * @param patientUuid Patient uuid to delete from base.
     * @throws PatientDeleteException if the delete request fails.
     */
    void deletePatient(String patientUuid) throws PatientDeleteException;


    /**
     * Delete a patient from Database
     *
     * @param patientDB Patient to delete from base.
     * @throws PatientDeleteException if the delete request fails.
     */
    void deletePatientDB(PatientDB patientDB) throws PatientDeleteException;

    PatientDB updatePatientDB(PatientDB patientDB) throws PatientUpdateException;


    /**
     * Search for a list of patient matching genderCode and DateOfBirth
     * @param patient parameter to retrieve List
     * @return list of Matching patient from PIX Manager Actor
     * @throws PatientNotFoundException if an error occurred
     */
    List<PatientDB> retrieveMatchingPatients(PatientDB patient) throws PatientNotFoundException;


    /**
     * Search for a patient matching the given identifier
     * @param identifier to retrieve Patient
     * @return Patient matching the identifier
     * @throws PatientNotFoundException if an error occurred
     */
    PatientDB readFirstPatientByIdentifier(EntityIdentifier identifier) throws PatientNotFoundException;

    /**
     * Search for all patients matching the given identifier
     * @param identifier to retrieve Patient
     * @return List of Patients matching the identifier
     * @throws PatientNotFoundException if an error occurred
     */
    List<PatientDB> readAllPatientsByIdentifier(EntityIdentifier identifier) throws PatientNotFoundException;


}
