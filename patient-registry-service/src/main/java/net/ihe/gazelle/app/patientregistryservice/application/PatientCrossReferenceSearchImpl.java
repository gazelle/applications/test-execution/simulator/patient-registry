package net.ihe.gazelle.app.patientregistryservice.application;

import net.ihe.gazelle.app.patientregistryapi.application.PatientCrossReferenceSearch;
import net.ihe.gazelle.app.patientregistryapi.application.SearchCrossReferenceException;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PatientAliases;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.converter.PatientConverter;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.references.CrossReferenceDB;
import net.ihe.gazelle.app.patientregistryservice.application.dao.DomainDAO;
import net.ihe.gazelle.app.patientregistryservice.application.dao.PatientCrossReferenceDAO;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

public class PatientCrossReferenceSearchImpl implements PatientCrossReferenceSearch {
    private PatientCrossReferenceDAO patientCrossReferenceDAO;
    private DomainDAO domainDAO;
    private static final GazelleLogger LOGGER = GazelleLoggerFactory.getInstance().getLogger(PatientCrossReferenceSearchImpl.class);

    @Inject
    public PatientCrossReferenceSearchImpl(
            @Named("PatientCrossReferenceDAO") PatientCrossReferenceDAO patientCrossReferenceDAO, DomainDAO domainDAO) {
        this.patientCrossReferenceDAO = patientCrossReferenceDAO;
        this.domainDAO = domainDAO;
    }

    /**
     * Search for a patient alias by a source identifier
     * Verify that the source identifier is valid
     * Call the DAO to get a list of patient alias
     * Merge those patient aliases into one
     * Any patient from that patient alias that do not match the list of target domain will be deleted
     *
     * @param sourceIdentifier the search key
     * @return PatientAliases corresponding to the given parameters
     * @throws SearchCrossReferenceException if an error occurred during the search
     */
    @Override
    public PatientAliases search(EntityIdentifier sourceIdentifier, List<String> targetDomains) throws SearchCrossReferenceException {
        checkSourceIdentifier(sourceIdentifier);
        boolean usableTargetList = checkTargetDomains(targetDomains);
        PatientDB patientDB ;

        try {
            String systemIdentifier = sourceIdentifier.getSystemIdentifier();
            String id = sourceIdentifier.getValue();
            patientDB = patientCrossReferenceDAO.searchForPatientDBWithSourceIdentifier(systemIdentifier,
                    id);
            checkRetrievedPatientDB(patientDB);
            LOGGER.info("Patient with uuid : %s has been retrieved for system %s and id %s", patientDB.getUuid(), systemIdentifier, id);
            if (patientDB.getPixReference() == null) {
                LOGGER.error("Patient does not  have any CrossReference");
                return convertPatientDBToPatientAliases(patientDB);
            } else {
                LOGGER.info("Retrieve XReference");
                CrossReferenceDB crossReferenceDBS =
                        patientCrossReferenceDAO.searchForPatientAliasesWithPatientDB(patientDB.getPixReference().getId());
                LOGGER.info("Retrieved XReference");
                return processingOnResult(crossReferenceDBS, patientDB, usableTargetList, targetDomains);
            }
        } catch (SearchCrossReferenceException searchCrossReferenceException) {
            LOGGER.error(searchCrossReferenceException.getMessage());
            throw searchCrossReferenceException;
        } catch (Exception exception) {
            LOGGER.error(exception, "Internal Error occurred");
            throw new SearchCrossReferenceException("An error occurred during CrossReference Search", exception);
        }
    }


    /**
     * Check if the source identifier is valid
     * Throw an exception if the source identifier :
     * - is null
     * - has mandatory variables missing
     * - is unknown
     *
     * @param sourceIdentifier to check its content
     * @throws SearchCrossReferenceException if source identifier doesn't contain mandatory field
     */
    private void checkSourceIdentifier(EntityIdentifier sourceIdentifier) throws SearchCrossReferenceException {
        if (sourceIdentifier == null) {
            throw new SearchCrossReferenceException("The source identifier cannot be null");
        }
        String systemIdentifier = sourceIdentifier.getSystemIdentifier();
        String value = sourceIdentifier.getValue();
        if (systemIdentifier == null || systemIdentifier.isBlank()) {
            throw new SearchCrossReferenceException("The system identifier from sourceIdentifier cannot be null or empty");
        }
        if (value == null || value.isBlank()) {
            throw new SearchCrossReferenceException("The value from sourceIdentifier cannot be null or empty");
        }
    }

    /**
     * @param crossReference XRef list return by DAO
     * @param patientDB         Patient matching the source identifier
     * @return PatientAliases filtered patientAliases
     */
    private PatientAliases processingOnResult(CrossReferenceDB crossReference, PatientDB patientDB, boolean usableTargetDomains,
                                              List<String> targetDomains) {

        PatientAliases patientAliases = new PatientAliases();
        List<Patient> patients = new ArrayList<>();

        List<PatientDB> patientList = crossReference.getPatients();
        if (patientList != null && !patientList.isEmpty()) {
            for (PatientDB patientInCrossRef : crossReference.getPatients()) {
                if (!patientInCrossRef.getUuid().equals(patientDB.getUuid())) {
                    patients.add(PatientConverter.toPatient(patientInCrossRef));
                }
            }
        }

        patientAliases.setMembers(patients);
        patientAliases.setLastModifier("Generated");
        patientAliases.setLastModifierDateTime();
        if (usableTargetDomains) {
            removePatientFromPatientAliasWithNonMatchingTargetDomain(patientAliases, targetDomains);
        }
        LOGGER.info("Search is done, prepare the response");
        return patientAliases;

    }

    /**
     * Convert patient DB in case of no matching X-ref
     *
     * @param patientDB from DB
     * @return result from DB in a PatientAliases
     */
    private PatientAliases convertPatientDBToPatientAliases(PatientDB patientDB) {
        PatientAliases patientAliases = new PatientAliases();
        patientAliases.addMember(PatientConverter.toPatient(patientDB));
        return patientAliases;
    }


    private void checkRetrievedPatientDB(PatientDB patientDB) {
        if (patientDB != null) {
            String uuid = patientDB.getUuid();
            if (uuid == null || uuid.isBlank()) {
                throw new IllegalArgumentException("The uuid of patientDB shall not be null");
            }
        }
    }

    /**
     * Delete every patient (of a patient aliases) where its uuid does not match the system identifier of any target domain currently in the list
     *
     * @param patientAlias  to be filtered by target system
     * @param targetDomains to filter
     * @return filtered PatientAliases matching target domains
     */
    private PatientAliases removePatientFromPatientAliasWithNonMatchingTargetDomain(PatientAliases patientAlias, List<String> targetDomains) {

        List<Patient> patients = new ArrayList<>();
        for (Patient patient : patientAlias.getMembers()) {
            for (EntityIdentifier entityIdentifier : patient.getIdentifiers()) {
                if (targetDomains.contains(entityIdentifier.getSystemIdentifier())) {
                    patients.add(patient);
                    break;
                }
            }
        }
        patientAlias.setMembers(patients);
        return patientAlias;
    }

    /**
     * The method is used to check the list of Target domain.
     * If the list is empty, the boolean with be set at false to avoid process with
     * targetDomain.
     * The method check if all item of target domain exist. i
     * If one is invalid an error is raised
     * If all target domain are valid, the boolean is set to true to proceed with targetDomains
     *
     * @param targetDomains to be check
     * @return usableTargetDomains
     * @throws SearchCrossReferenceException if an error occurred
     */
    private boolean checkTargetDomains(List<String> targetDomains) throws SearchCrossReferenceException {
        if (targetDomains == null || targetDomains.isEmpty()) {
            return false;
        }
        for (String targetDomain : targetDomains) {
            if (targetDomain.isBlank()) {
                throw new SearchCrossReferenceException("One of the target domain does not exist");
            }
            if (!domainDAO.exist(targetDomain)) {
                LOGGER.error("Domain %s does not exist", targetDomain);
                throw new SearchCrossReferenceException("One of the target domain does not exist");
            }
        }
        return true;
    }
}
