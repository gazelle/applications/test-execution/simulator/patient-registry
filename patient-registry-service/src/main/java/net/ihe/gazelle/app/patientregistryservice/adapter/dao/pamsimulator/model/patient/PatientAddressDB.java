package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient;

import javax.persistence.*;

/**
 * <b>Class Description : </b>PatientAddressDB<br>
 * <br>
 *
 * @author Anne-Gaelle Berge / IHE-Europe development Project
 * @version 1.0 - 09/11/16
 *
 * @since 2.2.0
 */

@Entity
@DiscriminatorValue("patient_address")
public class PatientAddressDB extends RawAddressDB {

    @ManyToOne(targetEntity = PatientDB.class)
    @JoinColumn(name = "patient_id")
    private PatientDB patient;

    @Column(name = "is_main_address")
    private boolean mainAddress;


    /**
     * <p>Constructor for PatientAddressDB.</p>
     */
    public PatientAddressDB() {
        super();
        this.mainAddress = true;
    }



    /**
     * <p>isMainAddress.</p>
     *
     * @return a boolean.
     */
    public boolean isMainAddress() {
        return mainAddress;
    }

    /**
     * <p>Setter for the field <code>mainAddress</code>.</p>
     *
     * @param mainAddress a boolean.
     */
    public void setMainAddress(boolean mainAddress) {
        this.mainAddress = mainAddress;
    }


    /**
     * <p>Getter for the field <code>patient</code>.</p>
     *
     * @return a {@link AbstractPatientDB} object.
     */
    public AbstractPatientDB getPatient() {
        return patient;
    }

    /**
     * <p>Setter for the field <code>patient</code>.</p>
     *
     * @param patient a {@link AbstractPatientDB} object.
     */
    public void setPatient(PatientDB patient) {
        this.patient = patient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PatientAddressDB)) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        PatientAddressDB that = (PatientAddressDB) o;

        return mainAddress == that.mainAddress;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (mainAddress ? 1 : 0);
        return result;
    }
}
