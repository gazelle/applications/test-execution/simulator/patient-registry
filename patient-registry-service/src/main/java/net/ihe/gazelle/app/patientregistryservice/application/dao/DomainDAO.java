package net.ihe.gazelle.app.patientregistryservice.application.dao;

import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.HierarchicDesignatorDB;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.DomainSearchException;

/**
 * API for the Domain DAO
 */
public interface DomainDAO {

    /**
     * Retrieve a Domain by its Identifier
     *
     * @param domainIdentifier identifier of the Domain to retrieve.
     * @return retrieved Domain if exists, or null.
     * @throws DomainSearchException if an error occurred during the retrieve method
     */
    HierarchicDesignatorDB retrieve(String domainIdentifier) throws DomainSearchException;

    /**
     * Delete a domain from Database.
     *
     * @param domainIdentifier identifier of the domain to delete.
     */
    void delete(String domainIdentifier);

    /**
     * Checks the Domain exists in the Patient Registry
     *
     * @param domainIdentifier Identifier of the domain to check.
     * @return a boolean indication whether or not the Domain is known by the Patient Registry
     */
    boolean exist(String domainIdentifier);

    /**
     * Creates the Domain in the Patient Registry so it can be used on new Patient Identifiers.
     *
     * @param domainIdentifier Identifier for the Domain to create
     * @param domainName       Name of the domain to create
     */
    void createDomain(String domainIdentifier, String domainName);


    /**
     * Search for a HierarchicDesignatorDB given a system identifier
     *
     * @param systemIdentifier to be check
     * @return Id of the matching HierarchicDesignatorDB to the systemIdentifier
     * @throws DomainSearchException if an error occurred during the search
     */
    Integer searchForHierarchicDesignator(String systemIdentifier) throws DomainSearchException;

}
