package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search;

import net.ihe.gazelle.app.patientregistryapi.application.PatientFieldsMatcherService;
import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import net.ihe.gazelle.app.patientregistryapi.application.SearchQueryBuilder;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.EntityManagerProducer;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.GenderCodeDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class SearchQueryBuilderImpl implements SearchQueryBuilder<PatientDB> {

    private final PatientFieldsMatcherService patientFieldsMatcherService;
    private final EntityManager entityManager;


    @Inject
    public SearchQueryBuilderImpl(@EntityManagerProducer.InjectEntityManager EntityManager entityManager, PatientFieldsMatcherService patientFieldsMatcherService) {
        this.patientFieldsMatcherService = patientFieldsMatcherService;
        this.entityManager = entityManager;
    }


    public void addQuery(SearchParameter searchParameter, List<String> queries, Map<String, Object> values) throws SearchException {
        ModifierProviderImpl modifierProvider = new ModifierProviderImpl();
        String queryKey = patientFieldsMatcherService.getMapValue(searchParameter.getKey());
        String queryOperator = modifierProvider.getQueryOperator(searchParameter.getModifier());
        getQueryValue(queryOperator, searchParameter.getValue(), searchParameter.getClassType(), values);
        setUpQuery(queryOperator, queryKey, queries, values);
    }


    @Override
    public TypedQuery<PatientDB> build(List<SearchParameter> searchParameters) throws SearchException {
        List<String> queries = new ArrayList<>();
        Map<String, Object> values = new HashMap<>();

        for (SearchParameter param : searchParameters) {
            this.addQuery(param, queries, values);
        }

        Map<String, List<String>> groupedQueries = new HashMap<>();
        for (String query : queries) {
            String key = query.split(" ")[0];
            groupedQueries.computeIfAbsent(key, k -> new ArrayList<>()).add(query);
        }

        List<String> processedQueries = new ArrayList<>();
        for (Map.Entry<String, List<String>> entry : groupedQueries.entrySet()) {
            String joinedQueries = entry.getValue().size() > 1 ? "(" + String.join(" OR ", entry.getValue()) + ")" : entry.getValue().get(0);
            processedQueries.add(joinedQueries);
        }

        String querySQL = handleJoinAndBuild(processedQueries);

        TypedQuery<PatientDB> query = entityManager.createQuery(querySQL, PatientDB.class);
        for (Map.Entry<String, Object> entry : values.entrySet()) {
            query.setParameter(entry.getKey(), entry.getValue());

        }

        return query;

    }

    private String handleJoinAndBuild(List<String> queries) {
        String querySQL;
        String joinAddress = "";
        String joinIdentifier = "";

        if (String.join(" ", queries).contains("a."))
            joinAddress = "LEFT JOIN PatientAddressDB  a ON p = a.patient ";

        if (String.join(" ", queries).contains("i."))
            joinIdentifier = ", IN (p.patientIdentifiers) i ";

        String avoidGenderError = "p.genderCode IN ('F', 'M', 'O', 'U')";

        if (queries.isEmpty()) {
            querySQL = "SELECT p FROM PatientDB p " + joinAddress + joinIdentifier +
                    " WHERE " + avoidGenderError;
        } else {
            querySQL = "SELECT p FROM PatientDB p " + joinAddress + joinIdentifier +
                    " WHERE " + String.join(" AND ", queries) + " AND " + avoidGenderError;
        }

        return querySQL;
    }

    public void getQueryValue(String queryModifier, String rawValue, String classType, Map<String, Object> values) throws SearchException {
        String rawKey = "value" + (values.size() + 1);

        switch (classType) {
            case "String":
                handleStringValue(queryModifier, rawValue, rawKey, values);
                break;
            case "Integer":
                handleIntegerValue(rawValue, rawKey, values);
                break;

            case "Date":
                handleDateValue(queryModifier, rawValue, rawKey, values);
                break;

            case "GenderCode":
                handleGenderCodeValue(rawValue, rawKey, values);
                break;

            case "Boolean":
                handleBooleanValue(rawValue, rawKey, values);
                break;
            default:
                throw new SearchException("Class type not supported");


        }
    }

    private void handleBooleanValue(String rawValue, String rawKey, Map<String, Object> values) {
        values.put(rawKey, Boolean.parseBoolean(rawValue));

    }

    private void handleGenderCodeValue(String rawValue, String rawKey, Map<String, Object> values) {
        switch (rawValue) {
            case "female":
                values.put(rawKey, GenderCodeDB.F);
                break;
            case "male":
                values.put(rawKey, GenderCodeDB.M);
                break;
            case "other":
                values.put(rawKey, GenderCodeDB.O);
                break;
            default:
                values.put(rawKey, GenderCodeDB.U);
                break;

        }

    }

    private void handleDateValue(String queryModifier, String rawValue, String rawKey, Map<String, Object> values) throws SearchException {
        SimpleDateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
        try {
            Date date = formatter.parse(rawValue);
            if (queryModifier.equals("BETWEEN")) {
                Date start = new Date(date.getTime() - (7 * 24 * 60 * 60 * 1000));
                Date end = new Date(date.getTime() + (7 * 24 * 60 * 60 * 1000));
                values.put("startDate", start);
                values.put("endDate", end);

            } else {
                values.put(rawKey, date);
            }
        } catch (ParseException e) {
            throw new SearchException(e);
        }

    }

    private void handleIntegerValue(String rawValue, String rawKey, Map<String, Object> values) {
        values.put(rawKey, Integer.parseInt(rawValue));

    }

    private void handleStringValue(String queryModifier, String rawValue, String rawKey, Map<String, Object> values) {
        if (queryModifier.equals("LIKE")) {
            values.put(rawKey, "%" + rawValue + "%");
        } else {
            values.put(rawKey, rawValue);
        }

    }

    private void setUpQuery(String queryOperator, String queryKey, List<String> queries, Map<String, Object> values) {
        String query;
        if (queryOperator.equals("BETWEEN")) query = queryKey + " " + queryOperator + " :startDate AND :endDate";
        else query = queryKey + " " + queryOperator + " :" + "value" + values.size();
        queries.add(query);
    }


}
