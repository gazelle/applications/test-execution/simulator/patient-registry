package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient;

import javax.persistence.*;

/**
 * Created by aberge on 09/03/17.
 *
 * @author aberge
 * @version $Id: $Id
 */
@Entity
@Table(name = "pam_phone_number", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {"id"}))
@SequenceGenerator(name = "pam_phone_number_sequence", sequenceName = "pam_phone_number_id_seq", allocationSize = 1)
public class PatientPhoneNumberDB extends AbstractPhoneNumberDB {

    @Column(name = "dtype")
    private String dtype = "patient_phone_number";

    @ManyToOne(targetEntity = PatientDB.class)
    @JoinColumn(name = "patient_id")
    private PatientDB patient;

    @Column(name = "is_principal")
    private boolean principal;

    /**
     * <p>Constructor for PatientPhoneNumberDB.</p>
     */
    public PatientPhoneNumberDB(){
        super();
        this.principal = true;
    }

    /**
     * <p>Constructor for PatientPhoneNumberDB.</p>
     *
     * @param inPatient a {@link PatientDB} object.
     */
    public PatientPhoneNumberDB(PatientDB inPatient) {
        super();
        this.principal = true;
        this.patient = inPatient;
    }

    /**
     * <p>Getter for the field <code>patient</code>.</p>
     *
     * @return a {@link PatientDB} object.
     */
    public PatientDB getPatient() {
        return patient;
    }

    /**
     * <p>Setter for the field <code>patient</code>.</p>
     *
     * @param patient a {@link PatientDB} object.
     */
    public void setPatient(PatientDB patient) {
        this.patient = patient;
    }

    /**
     * <p>isPrincipal.</p>
     *
     * @return a boolean.
     */
    public boolean isPrincipal() {
        return principal;
    }

    /**
     * <p>Setter for the field <code>principal</code>.</p>
     *
     * @param principal a boolean.
     */
    public void setPrincipal(boolean principal) {
        this.principal = principal;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        PatientPhoneNumberDB that = (PatientPhoneNumberDB) o;

        if (principal != that.principal) {
            return false;
        }
        return patient != null ? patient.equals(that.patient) : that.patient == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (patient != null ? patient.hashCode() : 0);
        result = 31 * result + (principal ? 1 : 0);
        return result;
    }
}
