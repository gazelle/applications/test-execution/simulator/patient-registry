package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.references;


import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "pix_cross_reference", schema = "public")
@SequenceGenerator(name = "pix_cross_reference_sequence", sequenceName = "pix_cross_reference_id_seq", allocationSize = 1)
@NamedQuery(name = "CrossReferenceDB.findByCrossRefId", query = "SELECT cr FROM CrossReferenceDB cr WHERE :id = id")
public class CrossReferenceDB {

    @Id
    @NotNull
    @GeneratedValue(generator = "pix_cross_reference_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Integer id;

    @OneToMany(mappedBy = "pixReference", fetch = FetchType.LAZY)
    private List<PatientDB> patients;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_changed")
    private Date lastChanged;

    @Column(name = "last_modifier")
    private String lastModifier;

    public CrossReferenceDB(String username) {
        this.lastChanged = new Date();
        this.lastModifier = username;
    }

    public CrossReferenceDB() {
        this.lastChanged = new Date();
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<PatientDB> getPatients() {
        if (this.patients == null) {
            this.patients = new ArrayList<>();
        }

        return patients;
    }

    public void setPatients(List<PatientDB> patients) {
        this.patients = patients;
    }

    public Date getLastChanged() {
        if (lastChanged != null) {
            return (Date) lastChanged.clone();
        }
        return null;
    }

    public void setLastChanged(Date lastChanged) {
        if (lastChanged != null) {
            this.lastChanged = (Date) lastChanged.clone();
        } else {
            this.lastChanged = null;
        }
    }

    public String getLastModifier() {
        return lastModifier;
    }

    public void setLastModifier(String lastModifier) {
        this.lastModifier = lastModifier;
    }

    public Integer getId() {
        return id;
    }

    public CrossReferenceDB updateReference(EntityManager entityManager, String user) {
        this.lastChanged = new Date();
        this.lastModifier = user;
        return entityManager.merge(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CrossReferenceDB)) {
            return false;
        }

        CrossReferenceDB that = (CrossReferenceDB) o;

        if (patients != null ? !patients.equals(that.patients) : that.patients != null) {
            return false;
        }
        if (lastChanged != null ? !lastChanged.equals(that.lastChanged) : that.lastChanged != null) {
            return false;
        }
        return lastModifier != null ? lastModifier.equals(that.lastModifier) : that.lastModifier == null;
    }

    @Override
    public int hashCode() {
        int result = patients != null ? patients.hashCode() : 0;
        result = 31 * result + (lastChanged != null ? lastChanged.hashCode() : 0);
        result = 31 * result + (lastModifier != null ? lastModifier.hashCode() : 0);
        return result;
    }
}
