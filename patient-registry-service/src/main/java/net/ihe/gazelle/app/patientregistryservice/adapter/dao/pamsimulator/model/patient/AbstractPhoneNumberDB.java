package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient;

import javax.persistence.*;

/**
 * Created by aberge on 03/07/17.
 */

@MappedSuperclass
public abstract class AbstractPhoneNumberDB {

    @Id
    @GeneratedValue(generator = "pam_phone_number_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "value")
    private String value;

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private PhoneNumberTypeDB type;


    public AbstractPhoneNumberDB() {
        this.type = PhoneNumberTypeDB.HOME;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public PhoneNumberTypeDB getType() {
        return type;
    }

    public void setType(PhoneNumberTypeDB type) {
        this.type = type;
    }



    public Integer getId() {

        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AbstractPhoneNumberDB)) {
            return false;
        }

        AbstractPhoneNumberDB that = (AbstractPhoneNumberDB) o;

        if (value != null ? !value.equals(that.value) : that.value != null) {
            return false;
        }
        return type == that.type;
    }

    @Override
    public int hashCode() {
        int result = value != null ? value.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
