package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.converter;

import net.ihe.gazelle.app.patientregistryapi.business.Address;
import net.ihe.gazelle.app.patientregistryapi.business.AddressUse;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.AddressTypeDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientAddressDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;

import java.util.Iterator;

import static net.ihe.gazelle.app.patientregistryapi.business.AddressUse.PRIMARY_HOME;

/**
 * Converter for Addresses from business model to Database model and back.
 */
public class AddressConverter {

    /**
     * Private constructor to hide implicit one.
     */
    private AddressConverter() {
    }

    /**
     * Map a {@link PatientAddressDB} entity  to a business {@link Address} object.
     *
     * @param patientAddress : entity to map
     * @return mapped business object.
     */
    public static Address toAddress(PatientAddressDB patientAddress) {
        Address address = new Address();
        address.setCity(patientAddress.getCity());
        address.setCountryIso3(patientAddress.getCountryCode());
        address.setState(patientAddress.getState());
        address.setPostalCode(patientAddress.getZipCode());
        if (patientAddress.getAddressLine() != null && (!patientAddress.getAddressLine().isBlank())) {
                address.addLine(patientAddress.getAddressLine());

        }
        address.setUse(mapUse(patientAddress.getAddressType()));
        return address;
    }

    /**
     * Map Address type from the database to the business Model.
     *
     * @param addressType : database value of the Address Use.
     * @return Database value for the Address Use.
     */
    private static AddressUse mapUse(AddressTypeDB addressType) {
        if (addressType != null) {
            switch (addressType) {
                case HOME:
                    return AddressUse.HOME;
                case BAD_ADDRESS:
                    return AddressUse.BAD;
                case OFFICE:
                    return AddressUse.WORK;
                case LEGAL:
                    return AddressUse.LEGAL;
                case MAILING:
                    return AddressUse.MAILING;
                case BIRTH:
                    return AddressUse.BIRTH_PLACE;
                case BIRTH_RESIDENCE:
                    return AddressUse.BIRTH_RESIDENCE;
                case ORIGIN:
                    return AddressUse.COUNTRY_OF_ORIGIN;
                default:
                    return null;
            }
        }
        return null;
    }

    /**
     * Map a business {@link Address} object to a {@link PatientAddressDB} entity to use during Database access, and assign it to
     * the corresponding Patient in both direction.
     *
     * @param address   : business object to map
     * @param patientDB : Patient entity containing the address to map.
     * @return the mapped {@link PatientAddressDB}
     */
    public static PatientAddressDB toAddressDB(Address address, PatientDB patientDB) {
        PatientAddressDB patientAddress = toAddressDB(address);

        if (patientDB != null) {
            patientAddress.setPatient(patientDB);
            patientDB.addPatientAddress(patientAddress);
        }

        return patientAddress;
    }

    /**
     * Map a business {@link Address} object to a {@link PatientAddressDB} entity to use during Database access.
     *
     * @param address : business object to map
     * @return the mapped {@link PatientAddressDB}
     */
    public static PatientAddressDB toAddressDB(Address address) {
        PatientAddressDB patientAddress = new PatientAddressDB();
        Iterator<String> lineIterator = address.getLines().iterator();
        if (lineIterator.hasNext()) {
            String line = lineIterator.next();
            if (lineIterator.hasNext()) {
                line = line + lineIterator.next();
            }
            patientAddress.setAddressLine(line);
        }
        patientAddress.setZipCode(address.getPostalCode());
        patientAddress.setCity(address.getCity());
        patientAddress.setCountryCode(address.getCountryIso3());
        patientAddress.setState(address.getState());
        patientAddress.setAddressType(mapUse(address.getUse()));
        patientAddress.setMainAddress(PRIMARY_HOME.equals(address.getUse()));
        return patientAddress;
    }

    /**
     * Map Address Use from the business to the Database Model.
     *
     * @param addressUse : business value of the Address Use.
     * @return Database value for the Address Use.
     */
    private static AddressTypeDB mapUse(AddressUse addressUse) {
        if (addressUse != null) {
            switch (addressUse) {
                case PRIMARY_HOME:
                case HOME:
                    return AddressTypeDB.HOME;
                case BAD:
                    return AddressTypeDB.BAD_ADDRESS;
                case WORK:
                    return AddressTypeDB.OFFICE;
                case LEGAL:
                    return AddressTypeDB.LEGAL;
                case MAILING:
                    return AddressTypeDB.MAILING;
                case BIRTH_PLACE:
                    return AddressTypeDB.BIRTH;
                case BIRTH_RESIDENCE:
                    return AddressTypeDB.BIRTH_RESIDENCE;
                case COUNTRY_OF_ORIGIN:
                    return AddressTypeDB.ORIGIN;
                default:
                    return null;
            }
        }
        return null;
    }
}
