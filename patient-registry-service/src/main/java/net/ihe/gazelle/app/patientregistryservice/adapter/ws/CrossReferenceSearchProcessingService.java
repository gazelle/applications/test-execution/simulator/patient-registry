package net.ihe.gazelle.app.patientregistryservice.adapter.ws;

import com.gitb.core.AnyContent;
import com.gitb.ps.ProcessRequest;
import com.gitb.ps.ProcessResponse;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import net.ihe.gazelle.app.patientregistryapi.adapter.ws.CrossReferenceProcessingWebServiceConstants;
import net.ihe.gazelle.app.patientregistryapi.application.PatientCrossReferenceSearch;
import net.ihe.gazelle.app.patientregistryapi.application.SearchCrossReferenceException;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.PatientAliases;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.lib.gitbutils.adapter.GITBAnyContentType;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperAnyContentToObject;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperObjectToAnyContent;
import net.ihe.gazelle.lib.gitbutils.adapter.MappingException;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

/**
 * Processing service for Patient X-Ref Search requests.
 */
public class CrossReferenceSearchProcessingService {
    private static final GazelleLogger LOGGER = GazelleLoggerFactory.getInstance().getLogger(CrossReferenceSearchProcessingService.class);
    private final MapperAnyContentToObject mapper;
    private final PatientCrossReferenceSearch crossReferenceSearch;


    /**
     * Constructor for the class with all needed parameters
     *
     * @param crossReferenceSearch : Application service to search CrossReference
     */
    @Inject
    public CrossReferenceSearchProcessingService(PatientCrossReferenceSearch crossReferenceSearch) {
        this.mapper = new MapperAnyContentToObject();
        this.crossReferenceSearch = crossReferenceSearch;
    }

    /**
     * Process the Cross Reference Patient Search .
     *
     * @param processRequest : GITB request received.
     * @return ProcessResponse corresponding to the received request
     */
    public ProcessResponse processPatientCrossReferenceSearch(ProcessRequest processRequest) {
        LOGGER.info("Proceed with the XReferenceSearch request");
        List<AnyContent> searchParameter = processRequest.getInput();
        if (searchParameter.size() != 2) {
            LOGGER.error("ProcessRequest.input shall contain two parameters to retrieve Xref !");
            throw new IllegalArgumentException("ProcessRequest.input shall contain two parameters to retrieve Xref !");
        }
        try {
            PatientAliases patientAliases;
            AnyContent sourceIdentifier = searchParameter.get(0);
            AnyContent targetDomains = searchParameter.get(1);

            if (targetDomains == null) {
                if (!CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_NAME.equals(sourceIdentifier.getName())) {
                    LOGGER.error("ProcessRequest.inputs  shall have the name " + CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_NAME
                            + " and " + CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_TARGET_NAME + " for a "
                            + CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_SEARCH_OPERATION + " operation !");

                    throw new IllegalArgumentException(String.format("ProcessRequest.inputs  shall have the name '%s' and '%s' for a %s operation !",
                            CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_NAME,
                            CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_TARGET_NAME,
                            CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_SEARCH_OPERATION));
                }
                LOGGER.info("Search with sourceIdentifier");
                patientAliases = crossReferenceSearch.search(mapper.getObject(searchParameter.get(0), EntityIdentifier.class),
                        new ArrayList<>());
            } else {
                if (!CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_NAME.equals(sourceIdentifier.getName()) ||
                        !CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_TARGET_NAME.equals(targetDomains.getName())) {
                    LOGGER.error("ProcessRequest.inputs  shall have the name " + CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_NAME
                            + " and " + CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_TARGET_NAME + " for a "
                            + CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_SEARCH_OPERATION + " operation !");
                    throw new IllegalArgumentException(String.format("ProcessRequest.inputs  shall have the name '%s' and '%s' for a %s operation !",
                            CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_NAME,
                            CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_TARGET_NAME,
                            CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_SEARCH_OPERATION));
                }
                LOGGER.info("Search with sourceIdentifier and targetDomains ");
                patientAliases = crossReferenceSearch.search(mapper.getObject(searchParameter.get(0), EntityIdentifier.class),
                        mapper.getObjectCollection(searchParameter.get(1), ArrayList.class, String.class));
            }
            LOGGER.info("Create GITB Response");
            return createPatientSearchProcessResponse(patientAliases);
        } catch (MappingException e) {
            LOGGER.error(e, "Mapping exception");
            throw new IllegalArgumentException("Cannot decode Request inputs as SourceIdentifier !", e);
        } catch (SearchCrossReferenceException e) {
            LOGGER.error(e, "SearchCrossReferenceException has been raised");
            return ProcessResponseWithReportCreator.createFailedProcessResponse(e, searchParameter.get(0));
        }
    }


    /**
     * Create a {@link ProcessResponse} to a {@link ProcessRequest} sent to this service after a successful processing.
     * The response will contain a {@link TAR} report containing the result Status for the processing operation,
     * as well as a single Output containing the X-ref returned.
     *
     * @param xRefPatient : List of {@link PatientAliases} returned by the search service.
     * @return the process response to return to the WebService
     */
    private ProcessResponse createPatientSearchProcessResponse(PatientAliases xRefPatient) throws SearchCrossReferenceException {
        AnyContent patientAliasesAnyContent;
        if (xRefPatient == null) {
            throw new IllegalArgumentException("Successful operation shall return a not null Patient Aliases as an output !");
        }
        try {
            patientAliasesAnyContent =
                    new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_OUTPUT_NAME,
                            xRefPatient);
            if (patientAliasesAnyContent == null) {
                patientAliasesAnyContent = new AnyContent();
                patientAliasesAnyContent.setName(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_OUTPUT_NAME);
                patientAliasesAnyContent.setType(GITBAnyContentType.OBJECT.getGitbType());
            }
        } catch (MappingException e) {
            throw new SearchCrossReferenceException("Search request response cannot be mapped to GITB AnyContent !", e);
        }
        ProcessResponse processResponse = new ProcessResponse();
        processResponse.getOutput().add(patientAliasesAnyContent);
        processResponse.setReport(ProcessResponseWithReportCreator.createReport(TestResultType.SUCCESS));
        return processResponse;
    }

}
