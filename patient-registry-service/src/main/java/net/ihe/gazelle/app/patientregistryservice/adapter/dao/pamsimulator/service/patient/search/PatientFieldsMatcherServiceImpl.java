package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search;

import net.ihe.gazelle.app.patientregistryapi.application.PatientFieldsMatcherService;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PatientFieldsMatcherServiceImpl implements PatientFieldsMatcherService {
    private String filePath = null;
    private Map<String, String> mapDict = new HashMap<>();


    public PatientFieldsMatcherServiceImpl() {
        this.filePath = System.getenv().get("GZL_DICTIONARY_PATIENT_FIELDS");
        initMap();
    }

    public PatientFieldsMatcherServiceImpl(String filePath) {
        this.filePath = filePath;
        initMap();


    }

    private void initMap() {
        if (filePath == null) {
            mapDict = getDefaultmap();
            return;
        }

        try {
            mapDict = getMapFromFile(filePath);
        } catch (IOException e) {
            mapDict = getDefaultmap();
        }


    }

    public Map<String, String> getMapFromFile(String filePath) throws IOException {
        Path path = Paths.get(filePath);
        try (BufferedReader reader = Files.newBufferedReader(path)) {
            Stream<String> lines = reader.lines();
            return lines.map(line -> line.split(","))
                    .collect(Collectors.toMap(data -> data[0], data -> data[1]));
        }
    }

    public Map<String, String> getDefaultmap() {
        Map<String, String> defaultMapDict = new HashMap<>();
        defaultMapDict.put("_id", "p.uuid");
        defaultMapDict.put("given", "p.firstName");
        defaultMapDict.put("active", "p.stillActive");
        defaultMapDict.put("family", "p.lastName");
        defaultMapDict.put("identifier-value", "i.identifier");
        defaultMapDict.put("identifier-domain-id", "i.domain.universalID");
        defaultMapDict.put("telecom", "p.email");
        defaultMapDict.put("address", "a.addressLine");
        defaultMapDict.put("address-city", "a.city");
        defaultMapDict.put("address-country", "a.countryCode");
        defaultMapDict.put("address-postalcode", "a.zipCode");
        defaultMapDict.put("address-state", "a.state");
        defaultMapDict.put("gender", "p.genderCode");
        defaultMapDict.put("birthdate", "p.dateOfBirth");
        defaultMapDict.put("mothersMaidenName", "p.motherMaidenName");
        return defaultMapDict;
    }


    public String getMapValue(String key) {
        return mapDict.get(key);
    }


}
