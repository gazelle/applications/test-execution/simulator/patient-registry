package net.ihe.gazelle.app.patientregistryservice.application;

import net.ihe.gazelle.app.patientregistryapi.application.PatientSearchService;
import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryservice.application.dao.PatientDAO;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.PatientReadException;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.PatientSearchException;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Implementation of PatientSearchService
 */
@Named("searchApplicationService")
public class PatientSearchApplication implements PatientSearchService {

    private PatientDAO patientDAO;

    /**
     * Default constructor for the class.
     *
     * @param patientDAO DAO used to read database searching for patients.
     */
    @Inject
    public PatientSearchApplication(@Named("PatientDAO") PatientDAO patientDAO) {
        this.patientDAO = patientDAO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Patient> search(SearchCriteria searchCriteria) throws SearchException {
        if (searchCriteria != null) {
            try {
                return patientDAO.readPatient(searchCriteria);
            } catch (PatientReadException e) {
                throw new SearchException("Error reading Patient from Database !", e);
            }
        } else {
            throw new SearchException("SearchCriteria shall not be null !");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Patient> parametrizedSearch(List<SearchParameter> parameters) throws SearchException {
        if (parameters != null) {
            try {
                return patientDAO.searchPatients(parameters);
            } catch (PatientSearchException e) {
                throw new SearchException(e);
            }

        } else {
            throw new SearchException("List<SearchParameter> shall not be null !");
        }
    }
}
