package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>PatientIdentifier class.</p>
 *
 * @author aberge
 * @version $Id: $Id
 */
@Entity
@Table(name = "pam_patient_identifier", schema = "public", uniqueConstraints = @UniqueConstraint(columnNames = {
        "full_patient_id", "identifier_type_code"}))
@SequenceGenerator(name = "pam_patient_identifier_sequence", sequenceName = "pam_patient_identifier_id_seq", allocationSize = 1)
@NamedQuery(name = "PatientIdentifierDB.findByHierarchicDomainAndId", query = "SELECT pi FROM PatientIdentifierDB pi WHERE pi.domain.id = " +
        ":domain_id AND pi.identifier = :id_number")
@NamedQuery(name = "PatientIdentifierDB.findByFullPatientId", query = "SELECT pi FROM PatientIdentifierDB pi WHERE pi.fullPatientId = " +
        ":fullPatientId")
public class PatientIdentifierDB {

    @Id
    @GeneratedValue(generator = "pam_patient_identifier_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id")
    private Integer id;

    @Column(name = "full_patient_id")
    private String fullPatientId;

    /**
     * PID-3-1 (ST) or extension (HL7v3)
     */
    @Column(name = "id_number")
    private String identifier;

    /**
     * PID-3-4 (HD)
     */
    @ManyToOne(optional = true)
    @JoinColumn(name = "domain_id")
    private HierarchicDesignatorDB domain;

    /**
     * used to fill PID-3-5 (ID)
     */
    @Column(name = "identifier_type_code")
    private String identifierTypeCode;

    public List<PatientDB> getPatients() {
        return patients;
    }

    public void setPatients(List<PatientDB> patients) {
        this.patients = patients;
    }

    @ManyToMany(mappedBy = "patientIdentifiers", targetEntity = PatientDB.class)
    private List<PatientDB> patients = new ArrayList<>();

    /**
     * <p>Constructor for PatientIdentifier.</p>
     */
    public PatientIdentifierDB() {

    }

    /**
     * <p>Constructor for PatientIdentifier.</p>
     *
     * @param patientId          a {@link String} object.
     * @param identifierTypeCode a {@link String} object.
     */
    public PatientIdentifierDB(String patientId, String identifierTypeCode) {
        this.fullPatientId = patientId;
        if ((identifierTypeCode != null) && !identifierTypeCode.isEmpty()) {
            this.identifierTypeCode = identifierTypeCode;
        } else {
            this.identifierTypeCode = null;
        }
    }

    /**
     * <p>setFullPatientIdentifierIfEmpty.</p>
     */
    @PrePersist
    @PreUpdate
    public void setFullPatientIdentifierIfEmpty() {
        if (this.fullPatientId == null) {
            fullPatientId = (this.getIdentifier() != null ? this.getIdentifier() : "");
            if (domain != null) {
                fullPatientId = fullPatientId.concat("^^^");
                if (domain.getNamespaceID() != null) {
                    fullPatientId = fullPatientId.concat(domain.getNamespaceID());
                }
                fullPatientId = fullPatientId.concat("&");
                if (domain.getUniversalID() != null) {
                    fullPatientId = fullPatientId.concat(domain.getUniversalID());
                }
                fullPatientId = fullPatientId.concat("&");
                if (domain.getUniversalIDType() != null) {
                    fullPatientId = fullPatientId.concat(domain.getUniversalIDType());
                }
            }
        }
    }

    /**
     * Constructor for copy
     *
     * @param pid a {@link PatientIdentifierDB} object.
     */
    public PatientIdentifierDB(PatientIdentifierDB pid) {
        this.fullPatientId = pid.getFullPatientId();
        this.identifier = pid.getIdentifier();
        this.identifierTypeCode = pid.getIdentifierTypeCode();
        this.domain = pid.getDomain();
    }

    /**
     * <p>Setter for the field <code>fullPatientId</code>.</p>
     *
     * @param patientId a {@link String} object.
     */
    public void setFullPatientId(String patientId) {
        this.fullPatientId = patientId;
    }

    /**
     * <p>Getter for the field <code>fullPatientId</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getFullPatientId() {
        return fullPatientId;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link Integer} object.
     */
    public Integer getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link Integer} object.
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * <p>Setter for the field <code>identifierTypeCode</code>.</p>
     *
     * @param identifierTypeCode a {@link String} object.
     */
    public void setIdentifierTypeCode(String identifierTypeCode) {
        if ((identifierTypeCode != null) && identifierTypeCode.isEmpty()) {
            this.identifierTypeCode = null;
        }
        this.identifierTypeCode = identifierTypeCode;
    }

    /**
     * <p>Getter for the field <code>identifierTypeCode</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getIdentifierTypeCode() {
        return identifierTypeCode;
    }

    /**
     * <p>Setter for the field <code>domain</code>.</p>
     *
     * @param domain a {@link HierarchicDesignatorDB} object.
     */
    public void setDomain(HierarchicDesignatorDB domain) {
        this.domain = domain;
    }

    /**
     * <p>Getter for the field <code>domain</code>.</p>
     *
     * @return a {@link HierarchicDesignatorDB} object.
     */
    public HierarchicDesignatorDB getDomain() {
        return domain;
    }

    /**
     * <p>Setter for the field <code>idNumber</code>.</p>
     *
     * @param identifier a {@link String} object.
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * <p>Getter for the field <code>idNumber</code>.</p>
     *
     * @return a {@link String} object.
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * <p>remove element from the field <code>patients</code>.</p>
     *
     * @param patient a {@link PatientDB} object.
     */
    public void removePatient(PatientDB patient) {
        this.patients.remove(patient);
    }

    /**
     * <p>Add element for the field <code>patients</code>.</p>
     *
     * @param patient a {@link PatientDB} object.
     */
    public void addPatient(PatientDB patient) {
        this.patients.add(patient);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PatientIdentifierDB)) {
            return false;
        }

        PatientIdentifierDB that = (PatientIdentifierDB) o;

        if (fullPatientId != null ? !fullPatientId.equals(that.fullPatientId) : that.fullPatientId != null) {
            return false;
        }
        if (identifier != null ? !identifier.equals(that.identifier) : that.identifier != null) {
            return false;
        }
        return identifierTypeCode != null ? identifierTypeCode.equals(that.identifierTypeCode) : that.identifierTypeCode == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = fullPatientId != null ? fullPatientId.hashCode() : 0;
        result = 31 * result + (identifier != null ? identifier.hashCode() : 0);
        result = 31 * result + (identifierTypeCode != null ? identifierTypeCode.hashCode() : 0);
        return result;
    }
}
