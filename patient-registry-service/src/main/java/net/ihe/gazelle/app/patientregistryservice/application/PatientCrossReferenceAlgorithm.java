package net.ihe.gazelle.app.patientregistryservice.application;

import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.GenderCodeDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.references.CrossReferenceDB;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.CrossReferenceApplicationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.similarity.LevenshteinDistance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

public class PatientCrossReferenceAlgorithm {
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientCrossReferenceAlgorithm.class);
    public static final String PATIENT_DB_TO_COMPARE_IS_EMPTY_AND_CANNOT_BE_PROCESSED = "PatientDB toCompare is empty and cannot be processed";
    public static final String PATIENT_DB_TO_COMPARE_DOES_NOT_HAVE_THE_MINIMUM_REQUIRED_FIELDS_TO_BE_CROSSREFERENCED = "PatientDB to compare does " +
            "not have the minimum required fields to be crossreferenced";

    /**
     * Private constructor to override public implicit one
     */
    private PatientCrossReferenceAlgorithm() {
    }

    /**
     * method to compare one PatientDB to a collection of PatientDB
     *
     * @param comparedPatient : the PatientDB to create or update in the Database and so to check if any CrossReference for it already exists
     * @param patientsDB      : the collection of PatientDB to compare the similarities
     * @return the id CrossReferenceDB object containing the created/updated PatientDB.
     * @throws CrossReferenceApplicationException In case of missing mandatory parameters or fields
     */
    public static int crossReferencing(PatientDB comparedPatient, List<PatientDB> patientsDB) throws CrossReferenceApplicationException {
        LOGGER.info("Start XReference Algorithm");
        if (comparedPatient == null) {
            LOGGER.error(PATIENT_DB_TO_COMPARE_IS_EMPTY_AND_CANNOT_BE_PROCESSED);
            throw new IllegalArgumentException(PATIENT_DB_TO_COMPARE_IS_EMPTY_AND_CANNOT_BE_PROCESSED);
        }
        if (isPatientInvalidForXReferenceAlgorithm(comparedPatient)) {
            LOGGER.error(PATIENT_DB_TO_COMPARE_DOES_NOT_HAVE_THE_MINIMUM_REQUIRED_FIELDS_TO_BE_CROSSREFERENCED);
            throw new CrossReferenceApplicationException(PATIENT_DB_TO_COMPARE_DOES_NOT_HAVE_THE_MINIMUM_REQUIRED_FIELDS_TO_BE_CROSSREFERENCED);
        }
        if (patientsDB == null) {
            throw new IllegalArgumentException("List of PatientDB is null and cannot be processed.");
        }
        for (PatientDB patientDB : patientsDB) {
            if (isPatientInvalidForXReferenceAlgorithm(patientDB)) {
                LOGGER.error("One Patient is invalid xReference");
                throw new CrossReferenceApplicationException("PatientDB from Database does not have the minimum required fields to be " +
                        "crossreferenced");
            }
            CrossReferenceDB pixReference = patientDB.getPixReference();
            if (isMatchingSelection(comparedPatient, patientDB) && pixReference != null && pixReference.getId() != null) {
                LOGGER.info("A patient matches the new patient");
                return pixReference.getId();
            }
        }
        LOGGER.info("No matching patient");
        return 0;

    }

    /**
     * Private Method to convert to deprecate Date type used in Databases to LocalDate.
     *
     * @param toConvert Date to convert
     * @return a LocalDate object corresponding to the Date given in parameter
     */
    private static LocalDate convertDateToLocalDate(Date toConvert) {
        return toConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }


    /**
     * Private method to compare a single PatientDB with another one.
     * The comparison includes these fields with their respective criteria :
     * - The birthDate of the patients. Must be the same
     * - The Gender of the patients. Must be the same
     * - The familyName of the patients. Must have a levenshtein distance less than 3
     * - The givenName of the patients. Must have a levenshtein distance less than 3
     *
     * @param patient2 The PatientDB we are creating/updating
     * @param patient1 the PatientDB in the database
     * @return true if the PatientDB falls under the threshold of comparison, false if not
     */
    private static boolean isMatchingSelection(PatientDB patient1, PatientDB patient2) {

        Date comparedPatientBirthDate = patient1.getDateOfBirth();
        Date toComparePatientBirthDate = patient2.getDateOfBirth();


        GenderCodeDB gender = patient1.getGenderCode();
        GenderCodeDB gender2 = patient2.getGenderCode();
        String familyName1 = cleanString(patient1.getLastName());
        String familyName2 = cleanString(patient2.getLastName());
        String givenName1 = cleanString(patient1.getFirstName());
        String givenName2 = cleanString(patient2.getFirstName());


        return isEqualBirthDate(comparedPatientBirthDate, toComparePatientBirthDate) && isEqualGender(gender, gender2)
                && isLevenshteinComparisonAbove3(familyName1, familyName2) && isLevenshteinComparisonAbove3(givenName1, givenName2)
                && is80percentSimilar(familyName1, familyName2) && is80percentSimilar(givenName1, givenName2);
    }

    /**
     * Private method to check if two Date are the same.
     *
     * @param date1 : first date to compare
     * @param date2 : second date to compare
     * @return true if both date are the same, false otherwise.
     */
    private static boolean isEqualBirthDate(Date date1, Date date2) {
        if (date1 != null && date2 != null) {
            return convertDateToLocalDate(date1).equals(convertDateToLocalDate(date2));
        }
        return false;
    }

    /**
     * Private method to check if two genderCode are the same.
     *
     * @param genderCodeDB1 : the first genderCode to compare
     * @param genderCodeDB2 : the second genderCode to compare
     * @return true if both genderCode are the same, false otherwise.
     */
    private static boolean isEqualGender(GenderCodeDB genderCodeDB1, GenderCodeDB genderCodeDB2) {
        return genderCodeDB1.toGenderCode().equals(genderCodeDB2.toGenderCode());
    }

    /**
     * Private method to check if two string have a Levenshtein Distance of less than three
     * This is used when comparing names and accept the fact that they may have been misspelled
     *
     * @param string1 : the first string to compare
     * @param string2 : the second string to compare
     * @return true if both string are more thn 80% similar, false otherwise
     */
    private static boolean isLevenshteinComparisonAbove3(String string1, String string2) {
        if (string1 != null && !string1.isBlank() && string2 != null && !string2.isBlank()) {
            LevenshteinDistance stringCompare = new LevenshteinDistance();
            return (stringCompare.apply(string1, string2) < 3);
        }
        return false;
    }

    /**
     * Private method to check if two names have more than 80% of similarity.
     * This is used by combining a Levenshtein to the size of the smaller string used
     * In order to avoid having string of size two or three not interrupting the process with a hard coded 3 Levenshtein distance
     *
     * @param string1 : the first string to compare
     * @param string2 : the second string to compare
     * @return true if both string are more thn 80% similar, false otherwise
     */
    private static boolean is80percentSimilar(String string1, String string2) {
        if (!StringUtils.isEmpty(string1) && !StringUtils.isEmpty(string2) && string1.length() >= 4 && string2.length() >= 4) {
            int smallerSize = Math.min(string1.length(), string2.length());
            int minLevelOfLevenshtein = smallerSize * 0.2 > 1 ? (int) (smallerSize * 0.2) : 1;
            LevenshteinDistance stringCompare = new LevenshteinDistance();
            return (stringCompare.apply(string1, string2) <= minLevelOfLevenshtein);
        }
        return false;
    }

    /**
     * Private method to check if the minimum fields required for cross-referencing is present in the PatientDB.
     *
     * @param patientDB : the PatientDB to verify the fields.
     * @return true if the PatientDB is valid for cross-referencing, false otherwise.
     */
    public static boolean isPatientInvalidForXReferenceAlgorithm(PatientDB patientDB) {
        return patientDB.getDateOfBirth() == null || patientDB.getGenderCode() == null || StringUtils.isEmpty(patientDB.getLastName()) || StringUtils.isEmpty(patientDB.getFirstName());
    }

    /**
     * Private method used to clean strings used for First Names and Last Names
     * This method removes Special characters, accentuation, white-spaces and set the name to lower case before comparing.
     *
     * @param toClean : The string to strip of all non-alphanumeric characters
     * @return : the cleaned version of the string.
     */
    private static String cleanString(String toClean) {
        toClean = toClean.strip();
        toClean = StringUtils.stripAccents(toClean);
        toClean = toClean.replaceAll("[^A-Za-z0-9]", "");
        toClean = toClean.replace(" ", "");
        toClean = toClean.toLowerCase();
        return toClean;
    }


}
