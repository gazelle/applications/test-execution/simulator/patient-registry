package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service;

import net.ihe.gazelle.app.patientregistryservice.adapter.dao.EntityManagerProducer;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.DesignatorTypeDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.HierarchicDesignatorDB;
import net.ihe.gazelle.app.patientregistryservice.application.dao.DomainDAO;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.DomainSearchException;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import org.apache.commons.lang3.StringUtils;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.*;
import java.util.List;

/**
 * DAO for Domain Objects.
 */
@Named("domainDAOService")
public class DomainDAOImpl implements DomainDAO {
    private static final GazelleLogger log = GazelleLoggerFactory.getInstance().getLogger(DomainDAOImpl.class);
    public static final String DOMAIN_IDENTIFIER_CAN_NOT_BE_NULL_OR_EMPTY = "DomainIdentifier can not be null or empty";
    private EntityManager entityManager;

    /**
     * Private constructor to hide the implicit one.
     */
    private DomainDAOImpl() {
    }

    /**
     * Constructor for the class. Takes an EntityManager as argument. It will be used to perform actions on Database.
     *
     * @param entityManager {@link EntityManager} to perform actions on Database.
     */
    @Inject
    public DomainDAOImpl(@EntityManagerProducer.InjectEntityManager EntityManager entityManager) {
        if (entityManager == null) {
            throw new IllegalArgumentException("EntityManager cannot be null");
        }
        this.entityManager = entityManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createDomain(String domainIdentifier, String domainName) {
        if (domainIdentifier == null || domainIdentifier.isBlank()) {
            throw new IllegalArgumentException(DOMAIN_IDENTIFIER_CAN_NOT_BE_NULL_OR_EMPTY);
        }
        if (domainName == null || domainName.isBlank()) {
            domainName = String.format("DefaultSystemNameFor_systemIdentifier_%s", domainIdentifier);
        }
        if (!exist(domainIdentifier)) {
            try {
                HierarchicDesignatorDB domain = new HierarchicDesignatorDB(domainName, domainIdentifier, "ISO", DesignatorTypeDB.PATIENT_ID);
                domain.setUsage(DesignatorTypeDB.PATIENT_ID);
                entityManager.persist(domain);
                entityManager.flush();
                log.info(String.format("Domain : %s  created", domainIdentifier));
            } catch (PersistenceException exception) {
                log.error("Domain", exception);
            }
        } else {
            log.warn(String.format("Domain : %s  already saved", domainIdentifier));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void delete(String domainIdentifier) {
        if (domainIdentifier == null || domainIdentifier.isBlank()) {
            throw new IllegalArgumentException(DOMAIN_IDENTIFIER_CAN_NOT_BE_NULL_OR_EMPTY);
        }
        try {
            HierarchicDesignatorDB domain = retrieve(domainIdentifier);
            entityManager.remove(domain);
            entityManager.flush();
            log.info(String.format("Domain : %s  deleted", domainIdentifier));
        } catch (DomainSearchException exception) {
            log.warn(String.format("Domain : %s not found or already deleted", domainIdentifier), exception);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean exist(String domainIdentifier) {
        if (StringUtils.isBlank(domainIdentifier)) {
            throw new IllegalArgumentException(DOMAIN_IDENTIFIER_CAN_NOT_BE_NULL_OR_EMPTY);
        }
        TypedQuery<HierarchicDesignatorDB> query = entityManager.createNamedQuery("HierarchicDesignatorDB.findByUniversalID",
                HierarchicDesignatorDB.class);
        query.setParameter("universalID", domainIdentifier);
        query.setParameter("givenUsage", DesignatorTypeDB.PATIENT_ID);
        List<HierarchicDesignatorDB> hierarchicDesignatorDBList = query.getResultList();
        if (hierarchicDesignatorDBList.isEmpty()) {
            return false;
        } else {
            if (hierarchicDesignatorDBList.size() > 1) {
                log.warn("Duplicated Hierarchic Domain ");
                return true;
            }
            return true;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HierarchicDesignatorDB retrieve(String domainIdentifier) throws DomainSearchException {
        if (domainIdentifier == null || domainIdentifier.isBlank()) {
            throw new IllegalArgumentException(DOMAIN_IDENTIFIER_CAN_NOT_BE_NULL_OR_EMPTY);
        }
        try {
            TypedQuery<HierarchicDesignatorDB> query = entityManager.createNamedQuery("HierarchicDesignatorDB.findByUniversalID",
                    HierarchicDesignatorDB.class);
            query.setParameter("universalID", domainIdentifier);
            query.setParameter("givenUsage", DesignatorTypeDB.PATIENT_ID);
            HierarchicDesignatorDB domain = query.getSingleResult();
            log.info(String.format("Domain : %s  found", domainIdentifier));
            return domain;

        } catch (NoResultException exception) {
            throw new DomainSearchException("System does not exist", exception);
        } catch (NonUniqueResultException exception) {
            throw new DomainSearchException(String.format("Duplicated System for value : %s", domainIdentifier), exception);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer searchForHierarchicDesignator(String systemIdentifier) throws DomainSearchException {
        return retrieve(systemIdentifier).getId();
    }
}
