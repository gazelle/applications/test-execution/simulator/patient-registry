package net.ihe.gazelle.app.patientregistryservice.adapter.ws;

import com.gitb.ps.Void;
import com.gitb.ps.*;
import net.ihe.gazelle.app.patientregistryapi.adapter.ws.PatientProcessingWebserviceConstants;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebService;

/**
 * Patient Processing Service. Supports PatientFeed, PatientSearch and XRefSearch operations.
 */
@Stateless
@WebService(
        name = PatientProcessingWebserviceConstants.PATIENT_PROCESSING_RESOURCE_URL,
        serviceName = PatientProcessingWebserviceConstants.PATIENT_PROCESSING_SERVICE,
        portName = PatientProcessingWebserviceConstants.PATIENT_PROCESSING_SERVICE_PORT,
        targetNamespace = "http://www.gitb.com/ps/v1/",
        endpointInterface = "com.gitb.ps.ProcessingService")
public class PatientProcessingService implements ProcessingService {

    private static final String UNSUPPORTED_OPERATION = "Unsupported Operation";

    @Inject
    private PatientFeedProcessingService patientFeedProcessingService;

    @Inject
    private PatientSearchProcessingService patientSearchProcessingService;

    /**
     * Default constructor, used for injection.
     */
    public PatientProcessingService() {
    }

    /**
     * Constructor with dependencies.
     *
     * @param patientFeedService             application service to use to process PatientFeed requests.
     * @param patientSearchProcessingService application service to use to process PatientSearch requests.
     */
    public PatientProcessingService(PatientFeedProcessingService patientFeedService,
                                    PatientSearchProcessingService patientSearchProcessingService) {
        this.patientFeedProcessingService = patientFeedService;
        this.patientSearchProcessingService = patientSearchProcessingService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GetModuleDefinitionResponse getModuleDefinition(Void aVoid) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProcessResponse process(ProcessRequest processRequest) {
        if (processRequest != null) {
            if (PatientProcessingWebserviceConstants.PATIENT_CREATE_OPERATION.equals(processRequest.getOperation())) {
                return patientFeedProcessingService.processPatientCreate(processRequest);
            } else if (PatientProcessingWebserviceConstants.PATIENT_UPDATE_OPERATION.equals(processRequest.getOperation())) {
                return patientFeedProcessingService.processPatientUpdate(processRequest);
            } else if (PatientProcessingWebserviceConstants.PATIENT_SEARCH_OPERATION.equals(processRequest.getOperation())) {
                return patientSearchProcessingService.processPatientSearch(processRequest);
            } else if (PatientProcessingWebserviceConstants.PATIENT_PARAMETERS_SEARCH_OPERATION.equals(processRequest.getOperation())) {
                return patientSearchProcessingService.processPatientParametersSearch(processRequest);
            } else if (PatientProcessingWebserviceConstants.PATIENT_DELETE_OPERATION.equals(processRequest.getOperation())) {
                return patientFeedProcessingService.processPatientDelete(processRequest);
            } else if(PatientProcessingWebserviceConstants.PATIENT_IDENTIFIER_DELETE_OPERATION.equals(processRequest.getOperation())) {
                return patientFeedProcessingService.processPatientDeleteByIdentifier(processRequest);
            } else if (PatientProcessingWebserviceConstants.PATIENT_MERGE_OPERATION.equals(processRequest.getOperation())) {
                return patientFeedProcessingService.processPatientMerge(processRequest);
            } else {
                throw new UnsupportedOperationException(String.format("ProcessRequest.operation shall be %s, %s, %s, %s,%s,%s or %s!",
                        PatientProcessingWebserviceConstants.PATIENT_CREATE_OPERATION,
                        PatientProcessingWebserviceConstants.PATIENT_UPDATE_OPERATION,
                        PatientProcessingWebserviceConstants.PATIENT_DELETE_OPERATION,
                        PatientProcessingWebserviceConstants.PATIENT_IDENTIFIER_DELETE_OPERATION,
                        PatientProcessingWebserviceConstants.PATIENT_MERGE_OPERATION,
                        PatientProcessingWebserviceConstants.PATIENT_SEARCH_OPERATION,
                        PatientProcessingWebserviceConstants.PATIENT_PARAMETERS_SEARCH_OPERATION
                ));
            }
        } else {
            throw new IllegalArgumentException("ProcessRequest shall not be null !");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BeginTransactionResponse beginTransaction(BeginTransactionRequest beginTransactionRequest) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Void endTransaction(BasicRequest basicRequest) {
        throw new UnsupportedOperationException(UNSUPPORTED_OPERATION);
    }
}
