package net.ihe.gazelle.app.patientregistryservice.application.exceptions;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class PatientSearchExceptionTest {

    @Test
    public void generateException() {
        assertThrows(PatientSearchException.class, () -> {
            throw new PatientSearchException("Test");
        });
    }

    @Test
    public void generateException1() {
        assertThrows(PatientSearchException.class, () -> {
            throw new PatientSearchException("Test", new IllegalArgumentException("test"));
        });
    }

    @Test
    public void generateException2() {
        assertThrows(PatientSearchException.class, () -> {
            throw new PatientSearchException(new IllegalArgumentException("test"));
        });
    }

    @Test
    public void generateException3() {
        assertThrows(PatientSearchException.class, () -> {
            throw new PatientSearchException("Test", new IllegalArgumentException("test"), true, true);
        });
    }

    @Test
    public void generateException4() {
        assertThrows(PatientSearchException.class, () -> {
            throw new PatientSearchException();
        });
    }
}
