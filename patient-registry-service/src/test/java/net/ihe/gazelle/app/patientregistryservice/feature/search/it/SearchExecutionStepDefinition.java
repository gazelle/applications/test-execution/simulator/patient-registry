package net.ihe.gazelle.app.patientregistryservice.feature.search.it;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedException;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.GenderCode;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PatientSearchCriterionKey;
import net.ihe.gazelle.app.patientregistryfeedclient.adapter.PatientFeedClient;
import net.ihe.gazelle.app.patientregistrysearchclient.adapter.PatientSearchClient;
import net.ihe.gazelle.app.patientregistryservice.adapter.ws.PatientProcessingService;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteriaLogicalOperator;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.SearchCriterion;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.StringSearchCriterion;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.operator.StringSearchCriterionOperator;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Contains Step Definitions for the execution of Patient Search IT tests
 */
public class SearchExecutionStepDefinition {

    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    private PatientFeedClient patientFeedClient;
    private PatientSearchClient patientSearchClient;

    private List<Patient> existingPatients = new ArrayList<>();
    private Map<String, String> uuidOfFedPatients = new HashMap<>();

    private SearchCriteria searchCriteria;

    private List<Patient> searchedPatients;
    private List<Patient> checkedPatients;

    private Exception searchException;

    /**
     * Sets up in-memory Database and instantiate tested services.
     */
    @Before
    public void setUpInMemoryDBAndFeedClient() throws MalformedURLException {
        patientFeedClient = new PatientFeedClient(new URL("http://localhost:8580/patient-registry/PatientProcessingService/patient-processing-service?wsdl"));
        patientSearchClient = new PatientSearchClient(new URL("http://localhost:8580/patient-registry/PatientProcessingService/patient-processing-service?wsdl"));

        existingPatients = new ArrayList<>();

        searchedPatients = new ArrayList<>();
        checkedPatients = new ArrayList<>();
        searchCriteria = new SearchCriteria();
        searchException = null;
    }


    /**
     * Precondition that will store a new {@link Patient} as existing, so it can be used later on in tests.
     *
     * @param patientDataList list of information on the patient to create
     * @throws ParseException if dates cannot be parsed from feature.
     */
    @Given("the following patients exist")
    public void the_following_patients_exist(List<Map<String, String>> patientDataList) throws ParseException {
        for (Map<String, String> patientData : patientDataList) {
            Patient patient = new Patient();
                patient.setUuid(patientData.get("tmpuuid"));
            patient.setGender(GenderCode.valueOf(patientData.get("gender")));
            patient.setActive(Boolean.valueOf(patientData.get("active")));
            patient.setDateOfBirth(dateFormat.parse(patientData.get("dateOfBirth")));
            patient.setDateOfDeath(dateFormat.parse(patientData.get("dateOfDeath")));
            patient.setMultipleBirthOrder(Integer.valueOf(patientData.get("multipleBirthOrder")));
            existingPatients.add(patient);
        }
    }


    /**
     * Precondition that will add identifiers to existing patients.
     *
     * @param patientIdentifierDataList list on information on Identifiers to create
     */
    @Given("patients have the following identifiers")
    public void patients_have_the_following_identifiers(List<Map<String, String>> patientIdentifierDataList) {
        for (Map<String, String> identifierData : patientIdentifierDataList) {

            EntityIdentifier identifier = new EntityIdentifier(identifierData.get("systemIdentifier"),
                    identifierData.get("systemName"), identifierData.get("type"), identifierData.get("value"));

            Patient patient = getExistingPatientWithUUID(identifierData.get("tmpuuid"));
            if (patient == null) {
                throw new IllegalArgumentException(String.format("No patient corresponding to UUID %s !", identifierData.get("tmpuuid")));
            } else {
                patient.addIdentifier(identifier);
            }
        }
    }

    /**
     * Feed a patient using tested {@link PatientFeedClient} and {@link PatientProcessingService}
     *
     * @param uuid temporary UUID of the patient to feed.
     * @throws PatientFeedException if the feed cannot be performed correctly.
     */
    @Given("patient is fed with provisional uuid {string}")
    public void patient_is_fed_with_uuid(String uuid) throws PatientFeedException {
        if (System.getProperty("Fed") == null) {
            Patient patient = getExistingPatientWithUUID(uuid);
            if (patient != null) {
                String realUuid = patientFeedClient.createPatient(patient);
                uuidOfFedPatients.put(uuid, realUuid);
            } else {
                throw new IllegalArgumentException(String.format("No patient existing with UUID %s !", uuid));
            }
        }
    }

    /**
     * Add a {@link SearchCriterion} the Criteria that will be used during the Search Request.
     *
     * @param searchCriteriaName     name of the criterion
     * @param searchCriteriaValue    value to search
     * @param searchCriteriaVerbText operator to use
     */
    @Given("search criterion {string} {string} {string}")
    public void search_criterion(String searchCriteriaName, String searchCriteriaVerbText, String searchCriteriaValue) {
        if (this.searchCriteria == null) {
            this.searchCriteria = new SearchCriteria();
            searchCriteria.setLogicalOperator(SearchCriteriaLogicalOperator.AND);
        }
        addSearchCriterionToCriteria(searchCriteria, searchCriteriaName, searchCriteriaVerbText, searchCriteriaValue);
    }

    /**
     * Add a {@link SearchCriterion} the a specific {@link SearchCriteria}.
     *
     * @param searchCriteria         Search Criteria where to add Criterion
     * @param searchCriteriaName     name of the criterion
     * @param searchCriteriaValue    value to search
     * @param searchCriteriaVerbText operator to use
     */
    public void addSearchCriterionToCriteria(SearchCriteria searchCriteria, String searchCriteriaName, String searchCriteriaVerbText,
                                             String searchCriteriaValue) {
        if (searchCriteriaName != null) {
            if (searchCriteriaName.equals("tmpuuid")) {
                searchCriteria.addSearchCriterion(createStringSearchCriterion(PatientSearchCriterionKey.UUID,
                        getStringSearchCriterionOperator(searchCriteriaVerbText), uuidOfFedPatients.get(searchCriteriaValue)));
            } else if (searchCriteriaName.equals("uuid")) {
                searchCriteria.addSearchCriterion(createStringSearchCriterion(PatientSearchCriterionKey.UUID,
                        getStringSearchCriterionOperator(searchCriteriaVerbText), searchCriteriaValue));
            } else if (searchCriteriaName.equals("identifier.value")) {
                searchCriteria.addSearchCriterion(createStringSearchCriterion(PatientSearchCriterionKey.IDENTIFIER,
                        getStringSearchCriterionOperator(searchCriteriaVerbText), searchCriteriaValue));
            } else if (searchCriteriaName.equals("identifier.systemIdentifier")) {
                searchCriteria.addSearchCriterion(createStringSearchCriterion(PatientSearchCriterionKey.DOMAIN,
                        getStringSearchCriterionOperator(searchCriteriaVerbText), searchCriteriaValue));
            } else {
                fail(String.format("Unsupported criteria %s", searchCriteriaName));
            }
        } else {
            fail("Criterion name shall not be null !");
        }
    }

    @Given("search criteria for and conditions")
    public void searchCriteriaForAndCondition(List<Map<String, String>> patientDataList) {
        SearchCriteria searchCriteriaToComplete;
        if (this.searchCriteria == null) {
            this.searchCriteria = new SearchCriteria();
            searchCriteria.setLogicalOperator(SearchCriteriaLogicalOperator.AND);
            searchCriteriaToComplete = searchCriteria;
        } else {
            searchCriteriaToComplete = new SearchCriteria();
            searchCriteriaToComplete.setLogicalOperator(SearchCriteriaLogicalOperator.AND);
            searchCriteria.addSearchCriteria(searchCriteriaToComplete);
        }

        if (patientDataList != null) {
            for (Map<String, String> criterionMap : patientDataList) {
                String parameter = criterionMap.get("parameter");
                String verb = criterionMap.get("verb");
                String value = criterionMap.get("value");
                if (parameter == null || verb == null || value == null) {
                    fail(String.format("Criterion shall alwas have a parameter, verb and value : %s, %s, %s", parameter, verb, value));
                }
                addSearchCriterionToCriteria(searchCriteriaToComplete, parameter, verb, value);
            }
        }
    }

    @Given("search criteria for or conditions")
    public void searchCriteriaForOrCondition(List<Map<String, String>> patientDataList) {
        SearchCriteria searchCriteriaToComplete;
        if (this.searchCriteria == null) {
            this.searchCriteria = new SearchCriteria();
            searchCriteria.setLogicalOperator(SearchCriteriaLogicalOperator.OR);
            searchCriteriaToComplete = searchCriteria;
        } else {
            searchCriteriaToComplete = new SearchCriteria();
            searchCriteriaToComplete.setLogicalOperator(SearchCriteriaLogicalOperator.OR);
            searchCriteria.addSearchCriteria(searchCriteriaToComplete);
        }

        if (patientDataList != null) {
            for (Map<String, String> criterionMap : patientDataList) {
                String parameter = criterionMap.get("parameter");
                String verb = criterionMap.get("verb");
                String value = criterionMap.get("value");
                if (parameter == null || verb == null || value == null) {
                    fail(String.format("Criterion shall alwas have a parameter, verb and value : %s, %s, %s", parameter, verb, value));
                }
                addSearchCriterionToCriteria(searchCriteriaToComplete, parameter, verb, value);
            }
        }
    }

    @Given("root criteria is {string}")
    public void rootCriteriaIs(String operator) {
        if ("and".equals(operator)) {
            this.searchCriteria = new SearchCriteria();
            searchCriteria.setLogicalOperator(SearchCriteriaLogicalOperator.AND);
        } else if ("or".equals(operator)) {
            this.searchCriteria = new SearchCriteria();
            searchCriteria.setLogicalOperator(SearchCriteriaLogicalOperator.OR);
        } else {
            fail(String.format("Unexpected SearchCriteria Logical Operator : %s", operator));
        }
    }


    /**
     * Search is performed with previously initialized {@link SearchCriterion}
     */
    @When("search is done")
    public void search_is_done() {
        System.setProperty("Fed", "OK");
        try {
            searchedPatients = patientSearchClient.search(this.searchCriteria);
        } catch (Exception e) {
            searchException = e;
        }
    }

    /**
     * Checks Response status.
     *
     * @param responseStatus Excpected status of the response
     */
    @Then("response is {string}")
    public void response_is(String responseStatus) {
        if (responseStatus != null) {
            if (responseStatus.equals("OK")) {
                assertNull(searchException);
                assertNotNull(searchedPatients);
            } else if (responseStatus.equals("KO")) {
                assertNull(searchedPatients);
                assertNotNull(searchException);
            } else {
                fail(String.format("Unsupported Response status %s !", responseStatus));
            }
        } else {
            fail("Response status shall not be null !");
        }
    }

    /**
     * Check how many {@link Patient} were received.
     *
     * @param responseListSize expected number of {@link Patient}
     */
    @Then("received {int} patients")
    public void receivedResultSizePatients(int responseListSize) {
        assertEquals(responseListSize, searchedPatients.size());
    }

    /**
     * Checks all received {@link Patient} verify a specific search criterion.
     *
     * @param parameter name of the criterion
     * @param value     value to search
     * @param verb      operator to use
     */
    @Then("all received patients {string} {string} {string}")
    public void allReceivedPatientsVerify(String parameter, String verb, String value) {
        if (parameter != null) {
            if (parameter.equals("tmpuuid")) {
                searchedPatients.forEach((Patient patient) ->
                        assertTrue(checkStringValueWithOperator(verb, uuidOfFedPatients.get(value), patient.getUuid())));
            } else if (parameter.equals("uuid")) {
                searchedPatients.forEach((Patient patient) ->
                        assertTrue(checkStringValueWithOperator(verb, value, patient.getUuid())));
            } else if (parameter.equals("identifier.value")) {
                searchedPatients.forEach((Patient patient) -> {
                    List<String> identifierList = new ArrayList<>();
                    patient.getIdentifiers().forEach(identifier -> identifierList.add(identifier.getValue()));
                    assertTrue(checkStringListWithOperator(verb, value, identifierList));
                });
            } else if (parameter.equals("identifier.systemIdentifier")) {
                searchedPatients.forEach((Patient patient) -> {
                    List<String> identifierList = new ArrayList<>();
                    patient.getIdentifiers().forEach(identifier -> identifierList.add(identifier.getSystemIdentifier()));
                    assertTrue(checkStringListWithOperator(verb, value, identifierList));
                });
            } else {
                fail(String.format("Unsupported criteria %s", parameter));
            }
        } else {
            fail("Criterion name shall not be null !");
        }

    }

    /**
     * Checks all received {@link Patient} verify all search criterion from a list.
     *
     * @param patientDataList List of criterion that received patients shall verify
     */
    @Then("all received patients verify all")
    public void allReceivedPatientsVerifyAll(List<Map<String, String>> patientDataList) {
        if (patientDataList != null) {
            for (Map<String, String> criterionMap : patientDataList) {
                String parameter = criterionMap.get("parameter");
                String verb = criterionMap.get("verb");
                String value = criterionMap.get("value");
                if (parameter == null || verb == null || value == null) {
                    fail(String.format("Criterion shall alwas have a parameter, verb and value : %s, %s, %s", parameter, verb, value));
                }
                allReceivedPatientsVerify(parameter, verb, value);
            }
        }
    }

    /**
     * Checks all received {@link Patient} verify at least one search criterion in a list.
     *
     * @param patientDataList List of criterion that received patients may verify
     */
    @Then("all received patients verify one")
    public void allReceiverPatientsVerifyOne(List<Map<String, String>> patientDataList) {
        if (patientDataList != null) {
            boolean allReceiverPatientsVerifyOne = true;
            for (Patient patient : searchedPatients) {
                boolean patientVerifyAtLeastOne = false;
                for (Map<String, String> criterionMap : patientDataList) {
                    String parameter = criterionMap.get("parameter");
                    String verb = criterionMap.get("verb");
                    String value = criterionMap.get("value");
                    if (parameter == null || verb == null || value == null) {
                        fail(String.format("Criterion shall always have a parameter, verb and value : %s, %s, %s", parameter, verb, value));
                    }
                    patientVerifyAtLeastOne = patientVerifyAtLeastOne || patientVerify(patient, parameter, verb, value);
                }
                allReceiverPatientsVerifyOne = allReceiverPatientsVerifyOne &&  patientVerifyAtLeastOne;
            }
            assertTrue(allReceiverPatientsVerifyOne, "All patients should match at least one criterion");
        }
    }

    /**
     * Checks that some patient verify all Criterion in a list and store them in the checkerdPatients property.
     *
     * @param patientDataList List of criterion that received patients may verify
     */
    @Then("some patient verifies all")
    public void somePatientsVerifyAll(List<Map<String, String>> patientDataList) {
        if (patientDataList != null) {
            boolean somePatientsVerifyAll = false;
            for (Patient patient : searchedPatients) {
                for (Map<String, String> criterionMap : patientDataList) {
                    String parameter = criterionMap.get("parameter");
                    String verb = criterionMap.get("verb");
                    String value = criterionMap.get("value");
                    if (parameter == null || verb == null || value == null) {
                        fail(String.format("Criterion shall always have a parameter, verb and value : %s, %s, %s", parameter, verb, value));
                    }
                    somePatientsVerifyAll = somePatientsVerifyAll || patientVerify(patient, parameter, verb, value);
                }
            }
            assertTrue(somePatientsVerifyAll, "Some patients shall verify all criteria");
        }
    }

    /**
     * Checks that all retualways have a parameter, verb and value : always have a parameter, verb and value : rned patients have been checked correct.
     */
    @Then("all return patients match criteria")
    public void allReturnedPatientsMatchCriteria() {
        assertEquals(searchedPatients.size(), checkedPatients.size(), "All patients should verify criteria.");
    }


    /**
     * assert that a patient verify a criterion
     * @param patient the patient to check
     * @param parameter the parameter if the criterion
     * @param verb the verb of the criterion
     * @param value the value of the criterion
     * @return true is the patient verify the criterion
     */
    private boolean patientVerify(Patient patient , String parameter, String verb, String value) {
        if (parameter != null) {
            switch (parameter) {
                case "tmpuuid":
                    return checkStringValueWithOperator(verb, uuidOfFedPatients.get(value), patient.getUuid()) && !checkedPatients.contains(patient);
                case "uuid":
                    return checkStringValueWithOperator(verb, value, patient.getUuid()) && !checkedPatients.contains(patient);
                case "identifier.value": {
                    List<String> identifierList = new ArrayList<>();
                    patient.getIdentifiers().forEach(identifier -> identifierList.add(identifier.getValue()));
                    return checkStringListWithOperator(verb, value, identifierList);
                }
                case "identifier.systemIdentifier": {
                    List<String> identifierList = new ArrayList<>();
                    patient.getIdentifiers().forEach(identifier -> identifierList.add(identifier.getSystemIdentifier()));
                    return checkStringListWithOperator(verb, value, identifierList) && !checkedPatients.contains(patient);
                }
                default:
                    fail(String.format("Unsupported criteria %s", parameter));
                    break;
            }
            return false;
        } else {
            fail("Criterion name shall not be null !");
        }
        return false;

    }


    /**
     * Check a string value with a specific operator in a list
     *
     * @param operator name of the operator to use
     * @param expected expected value
     * @param actual   tested value
     * @return true if the list contain the expected value
     */
    private boolean checkStringListWithOperator(String operator, String expected, List<String> actual) {
        if (operator != null) {
            if (operator.equals("is")) {
                return actual.contains(expected);
            } else {
                fail(String.format("Unsupported Operator %s !", operator));
            }
        } else {
            fail("Operator shall not be null !");
        }
        return false;
    }

    /**
     * Check a string value with a specific operator
     *
     * @param operator name of the operator to use
     * @param expected expected value
     * @param actual   tested value
     * @return true if the string value is equal to the expected value
     */
    private boolean checkStringValueWithOperator(String operator, String expected, String actual) {
        if (operator != null) {
            if (operator.equals("is")) {
                return expected.equals(actual);
            } else {
                fail(String.format("Unsupported Operator %s !", operator));
            }
        } else {
            fail("Operator shall not be null !");
        }
        return false;
    }

    /**
     * Create a {@link StringSearchCriterion} object.
     *
     * @param key      key of the criterion
     * @param operator operator to use
     * @param value    value to search for
     * @return a {@link StringSearchCriterion} object.
     */
    private SearchCriterion createStringSearchCriterion(PatientSearchCriterionKey key, StringSearchCriterionOperator operator, String value) {
        StringSearchCriterion searchCriterion = new StringSearchCriterion(key);
        if (operator != StringSearchCriterionOperator.EXACT) {
            fail(String.format("Unsupported Operator %s !", operator));
        }
        searchCriterion.setValue(value);
        return searchCriterion;
    }

    /**
     * Get the {@link StringSearchCriterionOperator} corresponding to the .feature keyword.
     *
     * @param verb keyword of the operator
     * @return the corresponding {@link StringSearchCriterionOperator}
     */
    private StringSearchCriterionOperator getStringSearchCriterionOperator(String verb) {
        if (verb != null) {
            if (verb.equals("is")) {
                return StringSearchCriterionOperator.EXACT;
            } else {
                fail(String.format("Unsupported operator %s !", verb));
            }
        } else {
            fail("Operator shall not be null for criterion !");
        }
        return null;
    }

    /***
     * Get an existing patient by its temporary UUID.
     * @param uuid      UUID of the patient to get.
     * @return the patient with requested UUID
     */
    private Patient getExistingPatientWithUUID(String uuid) {
        if (uuid != null) {
            return existingPatients.stream()
                    .filter(patient -> uuid.equals(patient.getUuid()))
                    .findAny()
                    .orElse(null);
        } else {
            throw new IllegalArgumentException("UUID shall not be null !");
        }
    }
}
