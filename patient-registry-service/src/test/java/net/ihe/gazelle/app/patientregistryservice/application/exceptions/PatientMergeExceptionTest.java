package net.ihe.gazelle.app.patientregistryservice.application.exceptions;

import io.qameta.allure.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

@Feature("Coverage on exception")
@Story("PatientMergeException")
class PatientMergeExceptionTest {
    /**
     * Dummy Test for coverage
     *
     * @throws PatientMergeException testException
     */
    @Test
    @Description(" PatientMergeException")
    @Severity(SeverityLevel.MINOR)
    void generateException() {
        assertThrows(PatientMergeException.class, () -> {
            throw new PatientMergeException("Test");
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientMergeException testException
     */
    @Test
    @Description(" PatientMergeException")
    @Severity(SeverityLevel.MINOR)
    void generateException1() {
        assertThrows(PatientMergeException.class, () -> {
            throw new PatientMergeException("Test", new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientMergeException testException
     */
    @Test
    @Description(" PatientMergeException")
    @Severity(SeverityLevel.MINOR)
    void generateException2() {
        assertThrows(PatientMergeException.class, () -> {
            throw new PatientMergeException(new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientMergeException testException
     */
    @Test
    @Description(" PatientMergeException")
    @Severity(SeverityLevel.MINOR)
    void generateException4() {
        assertThrows(PatientMergeException.class, () -> {
            throw new PatientMergeException("Test", new IllegalArgumentException("test"), false, true);
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientMergeException testException
     */
    @Test
    @Description(" PatientMergeException")
    @Severity(SeverityLevel.MINOR)
    void generateException3() {
        assertThrows(PatientMergeException.class, () -> {
            throw new PatientMergeException();
        });
    }

}