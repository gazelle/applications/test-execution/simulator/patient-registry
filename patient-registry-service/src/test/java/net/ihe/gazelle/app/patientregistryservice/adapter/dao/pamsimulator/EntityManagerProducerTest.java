package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator;

import net.ihe.gazelle.app.patientregistryservice.adapter.dao.EntityManagerProducer;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Tests for {@link EntityManagerProducer}
 */
public class EntityManagerProducerTest {

    /**
     * Test constructor of class {@link EntityManagerProducer}
     */
    @Test
    public void instantiationTest() {
        EntityManagerProducer entityManagerProducer = new EntityManagerProducer();
        assertNotNull(entityManagerProducer, "EntityManagerProducer shall be instantiated and not null.");
    }
}
