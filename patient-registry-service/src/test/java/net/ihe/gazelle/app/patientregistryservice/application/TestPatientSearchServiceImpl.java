package net.ihe.gazelle.app.patientregistryservice.application;

import net.ihe.gazelle.app.patientregistryapi.application.PatientSearchService;
import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;

import java.util.ArrayList;
import java.util.List;

/**
 * Test implementation of PatientSearchService.
 */
public class TestPatientSearchServiceImpl implements PatientSearchService {

    private SearchResult searchResult;

    /**
     * Default constructor for the class. By default returns a valid response.
     */
    public TestPatientSearchServiceImpl() {
        this.searchResult = SearchResult.OK;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Patient> search(SearchCriteria searchCriteria) throws SearchException {

        switch (searchResult) {
            case OK:
                List<Patient> patients = new ArrayList<>();
                patients.add(new Patient());
                return patients;
            case KO:
                throw new SearchException("This is my exception");
            case NULL:
            default:
                return null;
        }
    }

    @Override
    public List<Patient> parametrizedSearch(List<SearchParameter> parameters) throws SearchException {
        switch (searchResult) {
            case OK:
                List<Patient> patients = new ArrayList<>();
                patients.add(new Patient());
                return patients;
            case KO:
                throw new SearchException("This is my exception");
            case NULL:
            default:
                return null;
        }
    }

    /**
     * Setter for the searchResult property
     *
     * @param searchResult value to set to the property
     */
    public void setSearchResult(SearchResult searchResult) {
        this.searchResult = searchResult;
    }

    /**
     * Enumeration of result that can be returned by this class.
     */
    public enum SearchResult {
        OK,
        KO,
        NULL
    }

}
