package net.ihe.gazelle.app.patientregistryservice.application;

import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search.ModifierProviderImpl;
import org.junit.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ModifierProviderImplTest {

    private ModifierProviderImpl modifierProviderImpl;

    @BeforeEach
    public void setUp() {
        modifierProviderImpl = new ModifierProviderImpl();
    }

    @Test
    public void testGetQueryOperator() {
        Assertions.assertEquals("LIKE", modifierProviderImpl.getQueryOperator("contains"));
        Assertions.assertEquals("!=", modifierProviderImpl.getQueryOperator("ne"));
        Assertions.assertEquals("<", modifierProviderImpl.getQueryOperator("lt"));
        Assertions.assertEquals("<", modifierProviderImpl.getQueryOperator("eb"));
        Assertions.assertEquals("<=", modifierProviderImpl.getQueryOperator("le"));
        Assertions.assertEquals(">", modifierProviderImpl.getQueryOperator("gt"));
        Assertions.assertEquals(">", modifierProviderImpl.getQueryOperator("sa"));
        Assertions.assertEquals(">=", modifierProviderImpl.getQueryOperator("ge"));
        Assertions.assertEquals(">=", modifierProviderImpl.getQueryOperator("ge"));
        Assertions.assertEquals("BETWEEN", modifierProviderImpl.getQueryOperator("ap"));
        Assertions.assertEquals("=", modifierProviderImpl.getQueryOperator("test"));
    }


}
