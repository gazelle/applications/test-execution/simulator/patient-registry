package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search;

import io.qameta.allure.Feature;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PatientSearchCriterionKey;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.DomainDAOImpl;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.IdentifierDAOImpl;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.PatientCrossReferenceDAOImpl;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.PatientDAOImpl;
import net.ihe.gazelle.app.patientregistryservice.application.dao.PatientDAO;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.DomainSearchException;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.PatientCreationException;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.PatientDeleteException;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteriaLogicalOperator;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.SearchCriterion;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.StringSearchCriterion;
import org.junit.jupiter.api.*;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Feature("PatientSearchDAO")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PatientSearchDAOTest {

    private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceSearchUnitTest";
    private static final GazelleLogger log = GazelleLoggerFactory.getInstance().getLogger(PatientSearchDAOTest.class);
    private EntityManager entityManager;
    private PatientSearchDAO patientSearchDAO;
    private DomainDAOImpl domainDAO;
    private IdentifierDAOImpl identifierDAO;
    private PatientDAO patientDAO;

    /**
     * Database init
     */
    @BeforeAll
    void initializeDatabase() {
        entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST).createEntityManager();
        entityManager.getTransaction().begin();

        patientSearchDAO = new PatientSearchDAO(entityManager, new PatientSearchCriterionJPAMappingService(entityManager),
                new PatientSearchResultJPAMappingService());

        domainDAO = new DomainDAOImpl(entityManager);
        identifierDAO = new IdentifierDAOImpl(entityManager, domainDAO);
        patientDAO = new PatientDAOImpl(entityManager,
                new PatientSearchDAO(entityManager, new PatientSearchCriterionJPAMappingService(entityManager), new PatientSearchResultJPAMappingService())
                , domainDAO, identifierDAO, new PatientCrossReferenceDAOImpl(entityManager), new SearchQueryBuilderImpl(entityManager, new PatientFieldsMatcherServiceImpl()));

    }

    /**
     * Database clean
     */
    @AfterAll
    void closeDatabase() {
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    /**
     * Test Patient search on UUID with a unique patient in DB.
     */
    @Test
    void patient_search_uuid_unique_patient() throws SearchException, PatientCreationException, PatientDeleteException {
        Patient patient = new Patient();
        String uuid = "patient_search_uuid_unique_patient";
        patient.setUuid(uuid);
        patientDAO.createPatient(patient);

        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.UUID);
        searchCriterion.setValue(uuid);
        searchCriteria.addSearchCriterion(searchCriterion);

        List<Patient> retrievedPatients = patientSearchDAO.search(searchCriteria);

        assertNotNull(retrievedPatients);
        assertEquals(1, retrievedPatients.size());
        assertEquals(patient, retrievedPatients.get(0), "Returned patient shall be equal to the one fed !");
        patientDAO.deletePatient(retrievedPatients.get(0));
    }

    /**
     * Test Patient search on UUID with multiple patients in DB.
     */
    @Test
    void patient_search_uuid_multiple_patient() throws SearchException, PatientCreationException, PatientDeleteException {
        Patient patient = new Patient();
        String uuid = "patient_search_uuid_multiple_patient_1";
        patient.setUuid(uuid);
        patientDAO.createPatient(patient);

        Patient patient1 = new Patient();
        String uuid1 = "patient_search_uuid_multiple_patient_2";
        patient1.setUuid(uuid1);
        patientDAO.createPatient(patient1);

        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.UUID);
        searchCriterion.setValue(uuid);
        searchCriteria.addSearchCriterion(searchCriterion);

        List<Patient> retrievedPatients = patientSearchDAO.search(searchCriteria);

        patientDAO.deletePatient(patient);
        patientDAO.deletePatient(patient1);

        assertNotNull(retrievedPatients);
        assertEquals(1, retrievedPatients.size());
        assertEquals(patient, retrievedPatients.get(0), "Returned patient shall be equal to the one fed !");
    }

    /**
     * Test Patient search on UUID with no match in DB.
     */
    @Test
    void patient_search_no_match() throws SearchException, PatientCreationException, PatientDeleteException {
        Patient patient = new Patient();
        String uuid = "patient_search_no_match-uuid0";
        patient.setUuid(uuid);
        patientDAO.createPatient(patient);

        Patient patient1 = new Patient();
        String uuid1 = "patient_search_no_match-uuid1";
        patient1.setUuid(uuid1);
        patientDAO.createPatient(patient1);

        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.UUID);
        searchCriterion.setValue("NoMatch");
        searchCriteria.addSearchCriterion(searchCriterion);

        List<Patient> retrievedPatients = patientSearchDAO.search(searchCriteria);

        patientDAO.deletePatient(patient);
        patientDAO.deletePatient(patient1);

        assertNotNull(retrievedPatients);
        assertEquals(0, retrievedPatients.size());
    }

    /**
     * Test Patient search on Identifier with a unique patient in DB.
     */
    @Test
    void patient_search_identifier_unique_patient() throws SearchException, PatientCreationException, PatientDeleteException {
        String systemIdentifier = "system-identifier";
        domainDAO.createDomain(systemIdentifier, "systemName");
        Patient patient = new Patient();
        String uuid = "patient_search_identifier_unique_patient-uuid0";
        String identifier = "test-identifier";
        patient.setUuid(uuid);
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier(systemIdentifier);
        entityIdentifier.setValue(identifier);
        patient.addIdentifier(entityIdentifier);
        patientDAO.createPatient(patient);


        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.IDENTIFIER);
        searchCriterion.setValue(identifier);
        searchCriteria.addSearchCriterion(searchCriterion);

        List<Patient> retrievedPatients = patientSearchDAO.search(searchCriteria);

        assertNotNull(retrievedPatients);
        assertEquals(1, retrievedPatients.size());
        assertEquals(patient.getUuid(), retrievedPatients.get(0).getUuid(), "Returned patient shall be equal to the one fed !");
        patientDAO.deletePatient(retrievedPatients.get(0));
    }

    /**
     * Test Patient search on Identifier with multiple patients in DB.
     */
    @Test
    void patient_search_identifier_multiple_patient() throws SearchException, PatientCreationException, PatientDeleteException {
        String systemIdentifier = "system-identifier";
        domainDAO.createDomain(systemIdentifier, "systemName");
        Patient patient = new Patient();
        String uuid = "test-uuid0";
        patient.setUuid(uuid);
        String identifier = "test-identifier";
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier(systemIdentifier);
        entityIdentifier.setValue(identifier);
        patient.addIdentifier(entityIdentifier);
        patientDAO.createPatient(patient);

        Patient patient1 = new Patient();
        String uuid1 = "test-uuid1";
        patient1.setUuid(uuid1);
        EntityIdentifier entityIdentifier1 = new EntityIdentifier();
        entityIdentifier1.setSystemIdentifier(systemIdentifier);
        entityIdentifier1.setValue("tutu");
        patient1.addIdentifier(entityIdentifier1);
        patientDAO.createPatient(patient1);

        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.IDENTIFIER);
        searchCriterion.setValue(identifier);
        searchCriteria.addSearchCriterion(searchCriterion);

        List<Patient> retrievedPatients = patientSearchDAO.search(searchCriteria);

        patientDAO.deletePatient(patient);
        patientDAO.deletePatient(patient1);

        assertNotNull(retrievedPatients);
        assertEquals(1, retrievedPatients.size());
        assertEquals(patient.getUuid(), retrievedPatients.get(0).getUuid(), "Returned patient shall be equal to the one fed !");
    }

    /**
     * Test Patient search on Domain with multiple patients in DB.
     */
    @Test
    void patientSearchDomainMultiplePatient() throws SearchException, PatientCreationException, PatientDeleteException, DomainSearchException {
        Patient patient = new Patient();
        String uuid = "test-uuid0°";
        patient.setUuid(uuid);
        String identifier = "test-identifier";
        String systemIdentifier = "system-identifier1";
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setValue(identifier);
        entityIdentifier.setSystemIdentifier(systemIdentifier);
        patient.addIdentifier(entityIdentifier);
        createIdentifiersDomainIfMissing(patient.getIdentifiers());
        patientDAO.createPatient(patient);

        Patient patient1 = new Patient();
        String uuid1 = "test-uuid10";
        patient1.setUuid(uuid1);
        EntityIdentifier entityIdentifier1 = new EntityIdentifier();
        entityIdentifier1.setValue("tutu");
        entityIdentifier1.setSystemIdentifier("toto");
        patient1.addIdentifier(entityIdentifier1);
        createIdentifiersDomainIfMissing(patient1.getIdentifiers());
        patientDAO.createPatient(patient1);

        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.DOMAIN);
        searchCriterion.setValue(systemIdentifier);
        searchCriteria.addSearchCriterion(searchCriterion);

        List<Patient> retrievedPatients = patientSearchDAO.search(searchCriteria);

        patientDAO.deletePatient(patient);
        patientDAO.deletePatient(patient1);
        domainDAO.delete(systemIdentifier);
        domainDAO.delete("toto");

        assertNotNull(retrievedPatients);
        assertEquals(1, retrievedPatients.size());
        Assertions.assertEquals(patient.getUuid(), retrievedPatients.get(0).getUuid(), "Returned patient shall be equal to the one fed !");
    }

    /**
     * Test Patient search on Identifier with no match in DB.
     */
    @Test
    void patient_search_no_match_identifier() throws SearchException, PatientCreationException, PatientDeleteException {
        Patient patient = new Patient();
        String uuid = "test-uuid0";
        String identifier = "test-identifier";
        patient.setUuid(uuid);
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("systeid");
        entityIdentifier.setValue(identifier);
        patient.addIdentifier(entityIdentifier);
        createIdentifiersDomainIfMissing(patient.getIdentifiers());
        patientDAO.createPatient(patient);

        Patient patient1 = new Patient();
        String uuid1 = "test-uuid1";
        patient1.setUuid(uuid1);
        patient1.addIdentifier(entityIdentifier);
        createIdentifiersDomainIfMissing(patient1.getIdentifiers());
        patientDAO.createPatient(patient1);

        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.IDENTIFIER);
        searchCriterion.setValue("Tralalilalere");
        searchCriteria.addSearchCriterion(searchCriterion);

        List<Patient> retrievedPatients = patientSearchDAO.search(searchCriteria);

        patientDAO.deletePatient(patient);
        patientDAO.deletePatient(patient1);

        assertNotNull(retrievedPatients);
        assertEquals(0, retrievedPatients.size());
    }

    /**
     * Test Patient search on Identifier AND UUID with multiple patients in DB.
     */
    @Test
    void patientSearchIdentifierAndUUIDMultiplePatient() throws SearchException, PatientCreationException, PatientDeleteException {
        String systemIdentifier = "system-identifier";
        domainDAO.createDomain(systemIdentifier, "systemName");
        String systemIdentifier2 = "system-identifier2";
        domainDAO.createDomain(systemIdentifier2, "systemName");


        Patient patient = new Patient();
        String uuid = "test-uuid0";
        String identifier = "test-identifier";

        patient.setUuid(uuid);
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier(systemIdentifier);
        entityIdentifier.setValue(identifier);
        patient.addIdentifier(entityIdentifier);
        createIdentifiersDomainIfMissing(patient.getIdentifiers());
        patientDAO.createPatient(patient);

        Patient patient1 = new Patient();
        String uuid1 = "test-uuid1";
        patient1.setUuid(uuid1);
        EntityIdentifier entityIdentifier1 = new EntityIdentifier();
        entityIdentifier1.setSystemIdentifier(systemIdentifier2);
        entityIdentifier1.setValue(identifier);
        patient1.addIdentifier(entityIdentifier1);
        patientDAO.createPatient(patient1);

        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.UUID);
        searchCriterion.setValue(uuid);
        SearchCriterion searchCriterion1 = new StringSearchCriterion(PatientSearchCriterionKey.IDENTIFIER);
        searchCriterion1.setValue(identifier);
        searchCriteria.addSearchCriterion(searchCriterion);
        searchCriteria.addSearchCriterion(searchCriterion1);
        searchCriteria.setLogicalOperator(SearchCriteriaLogicalOperator.AND);

        List<Patient> retrievedPatients = patientSearchDAO.search(searchCriteria);

        patientDAO.deletePatient(patient);
        patientDAO.deletePatient(patient1);

        assertNotNull(retrievedPatients);
        assertEquals(1, retrievedPatients.size());
        assertEquals(patient.getUuid(), retrievedPatients.get(0).getUuid(), "Returned patient shall be equal to the one fed !");
    }

    /**
     * Test Patient search on Identifier OR UUID with multiple patients in DB.
     */
    @Test
    void patientSearchIdentifierOrUUIDMultiplePatient() throws SearchException, PatientCreationException, PatientDeleteException {
        String systemIdentifier = "system-identifier";
        domainDAO.createDomain(systemIdentifier, "systemName");
        Patient patient = new Patient();
        String uuid = "test-uuid0";
        String identifier = "test-identifier";

        patient.setUuid(uuid);
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setValue(identifier);
        entityIdentifier.setSystemIdentifier(systemIdentifier);
        patient.addIdentifier(entityIdentifier);
        createIdentifiersDomainIfMissing(patient.getIdentifiers());
        patientDAO.createPatient(patient);

        Patient patient1 = new Patient();
        String uuid1 = "test-uuid1";
        patient1.setUuid(uuid1);
        EntityIdentifier entityIdentifier1 = new EntityIdentifier();
        entityIdentifier1.setSystemIdentifier(systemIdentifier);
        entityIdentifier1.setValue("tutu");
        patient1.addIdentifier(entityIdentifier1);
        createIdentifiersDomainIfMissing(patient1.getIdentifiers());
        patientDAO.createPatient(patient1);

        Patient patient2 = new Patient();
        String uuid2 = "test-uuid2";
        patient2.setUuid(uuid2);
        EntityIdentifier entityIdentifier2 = new EntityIdentifier();
        entityIdentifier2.setSystemIdentifier(systemIdentifier);
        entityIdentifier2.setValue("totu");
        patient2.addIdentifier(entityIdentifier2);
        createIdentifiersDomainIfMissing(patient2.getIdentifiers());
        patientDAO.createPatient(patient2);

        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.UUID);
        searchCriterion.setValue(uuid1);
        SearchCriterion searchCriterion1 = new StringSearchCriterion(PatientSearchCriterionKey.IDENTIFIER);
        searchCriterion1.setValue(identifier);
        searchCriteria.addSearchCriterion(searchCriterion);
        searchCriteria.addSearchCriterion(searchCriterion1);
        searchCriteria.setLogicalOperator(SearchCriteriaLogicalOperator.OR);

        List<Patient> retrievedPatients = patientSearchDAO.search(searchCriteria);

        patientDAO.deletePatient(patient);
        patientDAO.deletePatient(patient1);
        patientDAO.deletePatient(patient2);

        assertNotNull(retrievedPatients);
        assertEquals(2, retrievedPatients.size());
    }


    /**
     * Test Patient search on Identifier AND Domain with multiple patients in DB.
     */
    @Test
    void patientSearchIdentifierAndDomainMultiplePatient() throws SearchException, PatientCreationException, PatientDeleteException {
        String systemIdentifier = "system-identifier";
        domainDAO.createDomain(systemIdentifier, "systemName");
        Patient patient = new Patient();
        String uuid = "test-uuid345";
        String identifier = "test-identifier";

        patient.setUuid(uuid);
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setValue(identifier);
        entityIdentifier.setSystemIdentifier(systemIdentifier);
        patient.addIdentifier(entityIdentifier);
        createIdentifiersDomainIfMissing(patient.getIdentifiers());
        patientDAO.createPatient(patient);

        Patient patient1 = new Patient();
        String uuid1 = "test-uuid134";
        patient1.setUuid(uuid1);
        EntityIdentifier entityIdentifier1 = new EntityIdentifier();
        entityIdentifier1.setValue(identifier);
        entityIdentifier1.setSystemIdentifier("Tarte");
        patient1.addIdentifier(entityIdentifier1);
        createIdentifiersDomainIfMissing(patient1.getIdentifiers());
        patientDAO.createPatient(patient1);

        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.DOMAIN);
        searchCriterion.setValue(systemIdentifier);
        SearchCriterion searchCriterion1 = new StringSearchCriterion(PatientSearchCriterionKey.IDENTIFIER);
        searchCriterion1.setValue(identifier);
        searchCriteria.addSearchCriterion(searchCriterion);
        searchCriteria.addSearchCriterion(searchCriterion1);
        searchCriteria.setLogicalOperator(SearchCriteriaLogicalOperator.AND);

        List<Patient> retrievedPatients = patientSearchDAO.search(searchCriteria);

        patientDAO.deletePatient(patient);
        patientDAO.deletePatient(patient1);
        domainDAO.delete(systemIdentifier);
        domainDAO.delete("Tarte");

        assertNotNull(retrievedPatients);
        assertEquals(1, retrievedPatients.size());
        assertEquals(patient.getUuid(), retrievedPatients.get(0).getUuid(), "Returned patient shall be equal to the one fed !");
    }

    /**
     * Creates domains that does not exist in Database.
     *
     * @param identifiers list of identifier with identifiers to create.
     */
    private void createIdentifiersDomainIfMissing(List<EntityIdentifier> identifiers) {
        identifiers.forEach(identifier -> {
            if (identifier.getSystemIdentifier() != null && !domainDAO.exist(identifier.getSystemIdentifier())) {
                domainDAO.createDomain(identifier.getSystemIdentifier(), identifier.getSystemName());
            }
        });
    }
}
