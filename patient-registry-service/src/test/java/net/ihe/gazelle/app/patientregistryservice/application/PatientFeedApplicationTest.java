package net.ihe.gazelle.app.patientregistryservice.application;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedException;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFieldsMatcherService;
import net.ihe.gazelle.app.patientregistryapi.application.PatientRetrieveException;
import net.ihe.gazelle.app.patientregistryapi.business.*;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.DomainDAOImpl;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.IdentifierDAOImpl;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.PatientCrossReferenceDAOImpl;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.PatientDAOImpl;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.patient.search.*;
import net.ihe.gazelle.app.patientregistryservice.application.dao.PatientCrossReferenceDAO;
import net.ihe.gazelle.app.patientregistryservice.application.dao.PatientDAO;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.PatientDeleteException;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.PatientNotFoundException;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.PatientReadException;
import org.junit.Assert;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for {@link PatientFeedApplication}
 */
@Feature("PatientFeedServices")
class PatientFeedApplicationTest {

    private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitTest";

    private PatientDAO patientDAO;
    private DomainDAOImpl domainDAO;
    private IdentifierDAOImpl identifierDAO;
    private PatientCrossReferenceDAO patientCrossReferenceDAO;

    private EntityManager entityManager;

    private PatientFeedApplication feedApplication;
    private PatientRetrieveApplication retrieveApplication;
    private PatientFieldsMatcherService patientFieldsMatcherService;

    /**
     * database init
     */
    @BeforeEach
    void initializeDatabase() {
        entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST).createEntityManager();
        entityManager.getTransaction().begin();
        domainDAO = new DomainDAOImpl(entityManager);
        identifierDAO = new IdentifierDAOImpl(entityManager, domainDAO);
        patientCrossReferenceDAO = new PatientCrossReferenceDAOImpl(entityManager);
        patientFieldsMatcherService = new PatientFieldsMatcherServiceImpl();
        patientDAO = new PatientDAOImpl(entityManager,
                new PatientSearchDAO(entityManager, new PatientSearchCriterionJPAMappingService(entityManager),
                        new PatientSearchResultJPAMappingService())
                , domainDAO, identifierDAO, patientCrossReferenceDAO, new SearchQueryBuilderImpl(entityManager, patientFieldsMatcherService));

        feedApplication = new PatientFeedApplication(patientDAO, domainDAO, patientCrossReferenceDAO);
        retrieveApplication = new PatientRetrieveApplication(patientDAO);
    }

    /**
     * database clean
     */
    @AfterEach
    void closeDatabase() {
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    /**
     * test create + retrieve one patient
     */
    @Test
    void patientCreationTest() throws PatientFeedException, PatientRetrieveException, PatientDeleteException {
        Patient patient = new Patient();
        String uuid = feedApplication.createPatient(patient);

        Patient retrievedPatient = retrieveApplication.retrievePatient(uuid);

        assertNotNull(retrievedPatient);
        feedApplication.deletePatient(uuid);
    }

    /**
     * test create + retrieve multiple patients
     */
    @Test
    void multiplePatientsCreationTest() throws PatientFeedException, PatientRetrieveException, PatientDeleteException {
        Patient patient = new Patient();
        String uuid = feedApplication.createPatient(patient);
        String uuid2 = feedApplication.createPatient(patient);

        Patient retrievedPatient = retrieveApplication.retrievePatient(uuid);
        Patient retrievedPatient2 = retrieveApplication.retrievePatient(uuid2);

        assertNotNull(retrievedPatient);
        assertNotNull(retrievedPatient2);

        feedApplication.deletePatient(uuid);
        ;
        feedApplication.deletePatient(uuid2);
        ;
    }


    /**
     * test create + retrieve with complete patient
     */
    @Test
    void completePatientCreationTest() throws PatientFeedException, PatientRetrieveException, PatientDeleteException {
        Patient patient = new Patient();
        patient.setActive(true);
        Date dod = new Date(2020, 02, 23);
        patient.setDateOfDeath(dod);
        Date dob = new Date(1991, 03, 10);
        patient.setDateOfBirth(dob);
        patient.setGender(GenderCode.FEMALE);

        EntityIdentifier identifier = new EntityIdentifier();
        String identifierValue = "identifierValue3";
        String systemName = "systemName";
        String systemID = "systemId";
        identifier.setSystemName(systemName);
        identifier.setSystemIdentifier(systemID);
        identifier.setValue(identifierValue);
        patient.addIdentifier(identifier);

        Address address = new Address();
        String country = "country";
        address.setCountryIso3(country);
        patient.addAddress(address);

        PersonName name = new PersonName();
        String family = "family";
        name.setFamily(family);
        patient.addName(name);

        String uuid = feedApplication.createPatient(patient);

        Patient retrievedPatient = retrieveApplication.retrievePatient(uuid);

        assertTrue(retrievedPatient.isActive());
        assertEquals(dod, retrievedPatient.getDateOfDeath());
        assertEquals(dob, retrievedPatient.getDateOfBirth());
        assertEquals(GenderCode.FEMALE, retrievedPatient.getGender());
        assertEquals(1, retrievedPatient.getIdentifiers().size());
        assertEquals(identifierValue, retrievedPatient.getIdentifiers().get(0).getValue());
        assertEquals(1, retrievedPatient.getAddresses().size());
        assertEquals(country, retrievedPatient.getAddresses().get(0).getCountryIso3());
        assertEquals(1, retrievedPatient.getNames().size());
        assertEquals(family, retrievedPatient.getNames().get(0).getFamily());

        feedApplication.deletePatient(uuid);
        domainDAO.delete(systemID);
    }

    /**
     * test create + retrieve one patient with existing domain
     */
    @Test
    void patientCreationExistingDomainTest() throws PatientFeedException, PatientRetrieveException, PatientDeleteException {
        Patient patient = new Patient();
        String domainIdentifier = "domainIdentifierTest1";
        String domainName = "domainName";
        domainDAO.createDomain(domainIdentifier, domainName);

        EntityIdentifier identifier = new EntityIdentifier();
        String identifierValue = "identifierValue";
        identifier.setValue(identifierValue);
        identifier.setSystemIdentifier(domainIdentifier);
        identifier.setSystemName(domainName);
        patient.addIdentifier(identifier);

        String uuid = feedApplication.createPatient(patient);

        Patient retrievedPatient = retrieveApplication.retrievePatient(uuid);

        assertNotNull(retrievedPatient);

        feedApplication.deletePatient(uuid);
        domainDAO.delete(domainIdentifier);
    }


    @Description("test create + retrieve one patient with non-existing domain")
    @Test
    void patientCreationNonExistingDomainTest() throws PatientFeedException, PatientRetrieveException, PatientDeleteException {
        Patient patient = new Patient();
        String domainIdentifier = "domainIdentifier1";
        String domainName = "domainName";

        EntityIdentifier identifier = new EntityIdentifier();
        String identifierValue = "identifierValue1";
        identifier.setValue(identifierValue);
        identifier.setSystemIdentifier(domainIdentifier);
        identifier.setSystemName(domainName);
        patient.addIdentifier(identifier);

        String uuid = feedApplication.createPatient(patient);

        Patient retrievedPatient = retrieveApplication.retrievePatient(uuid);

        assertNotNull(retrievedPatient);

        feedApplication.deletePatient(uuid);
        domainDAO.delete(domainIdentifier);
    }

    @Test
    @Description("Create a Patient with complete field for XCrossRef")
    void test() throws PatientFeedException, PatientReadException, PatientNotFoundException, PatientDeleteException {
        Patient patient = new Patient();
        patient.setUuid("PatientPIXmFeed");

        EntityIdentifier identifier = new EntityIdentifier();
        String domainIdentifier = "domainIdentifierPIXm1";
        String domainName = "domainNamePIXm";
        String identifierValue = "identifierValue1";

        identifier.setValue(identifierValue);
        identifier.setSystemIdentifier(domainIdentifier);
        identifier.setSystemName(domainName);
        identifier.setType("ISO");
        patient.addIdentifier(identifier);

        patient.setDateOfBirth(new Date());
        patient.setGender(GenderCode.MALE);
        PersonName name = new PersonName();
        name.addGiven("testFirstName");
        name.setFamily("testLastName");
        patient.addName(name);

        String uuid = feedApplication.createPatient(patient);
        PatientDB patientDB = patientDAO.readPatientDB(uuid);
        Assert.assertNotEquals("Uuid shall have been updated", "PatientPIXmFeed", patientDB.getUuid());
        Assert.assertNotNull("PixReference shall be not null", patientDB.getPixReference());
        feedApplication.deletePatient(patientDB.getUuid());
        domainDAO.delete(domainIdentifier);
    }

    @Test
    @Description("Create tow Patients with complete fields for XCrossRef they shall be Xreferenced")
    void test2Xref() throws PatientFeedException, PatientReadException, PatientNotFoundException, PatientDeleteException {
        Patient patient = new Patient();
        patient.setUuid("PatientPIXmFeed");

        EntityIdentifier identifier = new EntityIdentifier();
        String domainIdentifier = "domainIdentifierPIXm2";
        String domainName = "domainNamePIXm";
        String identifierValue = "identifierValue1";

        identifier.setValue(identifierValue);
        identifier.setSystemIdentifier(domainIdentifier);
        identifier.setSystemName(domainName);
        identifier.setType("ISO");
        patient.addIdentifier(identifier);

        patient.setDateOfBirth(new Date());
        patient.setGender(GenderCode.MALE);
        PersonName name = new PersonName();
        name.addGiven("testFirstName");
        name.setFamily("testLastName");
        patient.addName(name);
        Patient patient2 = new Patient();
        patient2.setUuid("PatientPIXmFeed");

        EntityIdentifier identifier2 = new EntityIdentifier();
        String domainIdentifier2 = "domainIdentifierPIXm";
        String domainName2 = "domainNamePIXm";
        String identifierValue2 = "identifierValue2";

        identifier2.setValue(identifierValue2);
        identifier2.setSystemIdentifier(domainIdentifier2);
        identifier2.setSystemName(domainName2);
        identifier2.setType("ISO");
        patient2.addIdentifier(identifier2);

        patient2.setDateOfBirth(patient.getDateOfBirth());
        patient2.setGender(GenderCode.MALE);
        PersonName name2 = new PersonName();
        name2.addGiven("testFirstName2");
        name2.setFamily("testLastName2");
        patient2.addName(name2);

        String uuid = feedApplication.createPatient(patient);
        String uuid2 = feedApplication.createPatient(patient2);

        PatientDB patientDB = patientDAO.readPatientDB(uuid);
        PatientDB patientDB2 = patientDAO.readPatientDB(uuid2);
        Assert.assertNotEquals("Uuid shall have been updated", "PatientPIXmFeed", patientDB.getUuid());
        Assert.assertNotEquals("Uuid shall have been updated", "PatientPIXmFeed", patientDB2.getUuid());
        Assert.assertNotNull("PixReference shall be not null", patientDB.getPixReference());
        Assert.assertNotNull("PixReference shall be not null", patientDB2.getPixReference());
        Assert.assertEquals("It shall be the same XRef", patientDB.getPixReference().getId(), patientDB2.getPixReference().getId());
        feedApplication.deletePatient(patientDB.getUuid());
        feedApplication.deletePatient(patientDB2.getUuid());
        domainDAO.delete(domainIdentifier);
        domainDAO.delete(domainIdentifier2);
    }

    @Test
    @Description("Create tow Patients with complete fields for XCrossRef they shall be Xreferenced in different Xref")
    void test2XrefNo() throws PatientFeedException, PatientReadException, PatientNotFoundException, PatientDeleteException {
        Patient patient = new Patient();
        patient.setUuid("PatientPIXmFeed");

        EntityIdentifier identifier = new EntityIdentifier();
        String domainIdentifier = "domainIdentifierPIXm";
        String domainName = "domainNamePIXm2";
        String identifierValue = "identifierValue1";

        identifier.setValue(identifierValue);
        identifier.setSystemIdentifier(domainIdentifier);
        identifier.setSystemName(domainName);
        identifier.setType("ISO");
        patient.addIdentifier(identifier);

        patient.setDateOfBirth(new Date());
        patient.setGender(GenderCode.MALE);
        PersonName name = new PersonName();
        name.addGiven("testFirstName");
        name.setFamily("testLastName");
        patient.addName(name);

        Patient patient2 = new Patient();
        patient2.setUuid("PatientPIXmFeed");

        EntityIdentifier identifier2 = new EntityIdentifier();
        String domainIdentifier2 = "domainIdentifierPIXm";
        String domainName2 = "domainNamePIXm2";
        String identifierValue2 = "identifierValue2";

        identifier2.setValue(identifierValue2);
        identifier2.setSystemIdentifier(domainIdentifier2);
        identifier2.setSystemName(domainName2);
        identifier2.setType("ISO");
        patient2.addIdentifier(identifier2);

        patient2.setDateOfBirth(patient.getDateOfBirth());
        patient2.setGender(GenderCode.MALE);
        PersonName name2 = new PersonName();
        name2.addGiven("Albert");
        name2.setFamily("testLastName2");
        patient2.addName(name2);

        String uuid = feedApplication.createPatient(patient);
        String uuid2 = feedApplication.createPatient(patient2);

        PatientDB patientDB = patientDAO.readPatientDB(uuid);
        PatientDB patientDB2 = patientDAO.readPatientDB(uuid2);
        Assert.assertNotEquals("Uuid shall have been updated", "PatientPIXmFeed", patientDB.getUuid());
        Assert.assertNotEquals("Uuid shall have been updated", "PatientPIXmFeed", patientDB2.getUuid());
        Assert.assertNotNull("PixReference shall be not null", patientDB.getPixReference());
        Assert.assertNotNull("PixReference shall be not null", patientDB2.getPixReference());
        Assert.assertNotEquals("It shall be the same XRef", patientDB.getPixReference().getId(), patientDB2.getPixReference().getId());
        feedApplication.deletePatient(patientDB.getUuid());
        feedApplication.deletePatient(patientDB2.getUuid());
        domainDAO.delete(domainIdentifier);
    }

    @Test
    @Description("Delete a Patient with complete field for XCrossRef")
    void testDelete() throws PatientFeedException, PatientReadException, PatientNotFoundException, PatientDeleteException {
        Patient patient = new Patient();
        patient.setUuid("PatientPIXmFeed");

        EntityIdentifier identifier = new EntityIdentifier();
        String domainIdentifier = "domainIdentifierPIXm4";
        String domainName = "domainNamePIXm";
        String identifierValue = "identifierValue1";

        identifier.setValue(identifierValue);
        identifier.setSystemIdentifier(domainIdentifier);
        identifier.setSystemName(domainName);
        identifier.setType("ISO");
        patient.addIdentifier(identifier);

        patient.setDateOfBirth(new Date());
        patient.setGender(GenderCode.MALE);
        PersonName name = new PersonName();
        name.addGiven("testFirstName");
        name.setFamily("testLastName");
        patient.addName(name);

        String uuid = feedApplication.createPatient(patient);
        PatientDB patientDB = patientDAO.readPatientDB(uuid);
        Assert.assertNotEquals("Uuid shall have been updated", "PatientPIXmFeed", patientDB.getUuid());
        Assert.assertNotNull("PixReference shall be not null", patientDB.getPixReference());

        Assert.assertTrue(feedApplication.deletePatient(uuid));
        assertThrows(PatientNotFoundException.class, () -> patientDAO.readPatient(uuid));
        domainDAO.delete(domainIdentifier);
    }

    @Test
    @Description("Delete a Patient with error")
    void testDelete2() throws PatientFeedException {

        assertThrows(PatientFeedException.class, () -> feedApplication.deletePatient(""));
        assertThrows(PatientFeedException.class, () -> feedApplication.deletePatient((String) null));

    }

    @Test
    @Description("Delete a Patient already deleted")
    void testDelete3() throws PatientFeedException {
        Assert.assertFalse(feedApplication.deletePatient("1234"));
    }


    @Test
    @Description("Delete 2 patients using the same Patient Identifier")
    void testdeleteIdreused() throws PatientFeedException, PatientReadException, PatientNotFoundException, PatientDeleteException {
        Patient patient = new Patient();
        patient.setUuid("PatientPIXmFeed");

        EntityIdentifier identifier = new EntityIdentifier();
        String domainIdentifier = "domainIdentifierPIXm";
        String domainName = "domainNamePIXm";
        String identifierValue = "identifierValue1";

        identifier.setValue(identifierValue);
        identifier.setSystemIdentifier(domainIdentifier);
        identifier.setSystemName(domainName);
        identifier.setType("ISO");
        patient.addIdentifier(identifier);

        patient.setDateOfBirth(new Date());
        patient.setGender(GenderCode.MALE);
        PersonName name = new PersonName();
        name.addGiven("testFirstName");
        name.setFamily("testLastName");
        patient.addName(name);
        Patient patient2 = new Patient();
        patient2.setUuid("PatientPIXmFeed");

        EntityIdentifier identifier2 = new EntityIdentifier();
        identifier2.setValue(identifierValue);
        identifier2.setSystemIdentifier(domainIdentifier);
        identifier2.setSystemName(domainName);
        identifier2.setType("ISO");
        patient2.addIdentifier(identifier2);

        patient2.setDateOfBirth(patient.getDateOfBirth());
        patient2.setGender(GenderCode.MALE);
        PersonName name2 = new PersonName();
        name2.addGiven("testFirstName2");
        name2.setFamily("testLastName2");
        patient2.addName(name2);

        String uuid = feedApplication.createPatient(patient);
        String uuid2 = feedApplication.createPatient(patient2);

        PatientDB patientDB = patientDAO.readPatientDB(uuid);
        PatientDB patientDB2 = patientDAO.readPatientDB(uuid2);
        Assert.assertNotEquals("Uuid shall have been updated", "PatientPIXmFeed", patientDB.getUuid());
        Assert.assertNotEquals("Uuid shall have been updated", "PatientPIXmFeed", patientDB2.getUuid());
        Assert.assertNotNull("PixReference shall be not null", patientDB.getPixReference());
        Assert.assertNotNull("PixReference shall be not null", patientDB2.getPixReference());
        Assert.assertEquals("It shall be the same XRef", patientDB.getPixReference().getId(), patientDB2.getPixReference().getId());
        feedApplication.deletePatient(patientDB.getUuid());
        feedApplication.deletePatient(patientDB2.getUuid());
        domainDAO.delete(domainIdentifier);

    }
}
