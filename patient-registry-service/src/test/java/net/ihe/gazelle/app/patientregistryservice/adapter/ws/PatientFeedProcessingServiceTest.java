package net.ihe.gazelle.app.patientregistryservice.adapter.ws;

import com.gitb.core.AnyContent;
import com.gitb.core.ValueEmbeddingEnumeration;
import com.gitb.ps.ProcessRequest;
import com.gitb.ps.ProcessResponse;
import com.gitb.tr.BAR;
import com.gitb.tr.TestResultType;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import net.ihe.gazelle.app.patientregistryapi.adapter.ws.PatientProcessingWebserviceConstants;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PersonName;
import net.ihe.gazelle.app.patientregistryservice.application.TestPatientFeedServiceImpl;
import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperObjectToAnyContent;
import net.ihe.gazelle.lib.gitbutils.adapter.MappingException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test Feed processing service
 */
@Feature("GITB Feed Webservices")
public class PatientFeedProcessingServiceTest {

    private PatientFeedProcessingService patientFeedProcessingService;
    private TestPatientFeedServiceImpl testPatientFeedServiceImpl;

    /**
     * Initialize implementations to be tested before each test.
     */
    @BeforeEach
    public void setUp(){
        this.testPatientFeedServiceImpl = new TestPatientFeedServiceImpl();
        this.patientFeedProcessingService = new PatientFeedProcessingService(testPatientFeedServiceImpl);
    }

    /**
     * Test Processing a patient create, nominal case.
     */
    @Test
    @Covers(requirements = {"PATREG-30","PATREG-36","PATREG-37","PATREG-39"})
    @Description("Creation of a patient in Nominal Case")
    public void processCreate() {
        testPatientFeedServiceImpl.setFeedResult(TestPatientFeedServiceImpl.FeedResult.OK);
        Patient patient = new Patient();
        PersonName personName = new PersonName();
        personName.setFamily("Bars");
        patient.addName(personName);

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_CREATE_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_INPUT_NAME,
                    patient));
        } catch (MappingException e){
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        ProcessResponse processResponse = patientFeedProcessingService.processPatientCreate(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
        assertEquals(1, processResponse.getOutput().size(), "Response shall contain a unique output which is the value of the" +
                " UUID assigned to the fed patient.");
        assertEquals("string", processResponse.getOutput().get(0).getType(), "Returned UUID shall be stored in a string GITB AnyContent");
        assertEquals(PatientProcessingWebserviceConstants.UUID_OUTPUT_NAME, processResponse.getOutput().get(0).getName(), "Name of returned GITB AnyContent shall be 'uuid'");
        assertEquals(ValueEmbeddingEnumeration.STRING, processResponse.getOutput().get(0).getEmbeddingMethod(), "Embedding method of returned " +
                "GITB AnyContent shall be 'STRING'");

        assertNotNull(processResponse.getReport(), "ProcessResponse.report shall not be null !");
        assertEquals(TestResultType.SUCCESS, processResponse.getReport().getResult(), "Result of the processing shall be success !");
    }

    /**
     * Test Processing a patient feed, service throwing an exception.
     */
    @Test
    @Covers(requirements = {"PATREG-35","PATREG-38","PATREG-39"})
    @Description("Creation of a patient with an PatientFeedException")
    public void processCreate_with_PatientFeedException_return() {
        testPatientFeedServiceImpl.setFeedResult(TestPatientFeedServiceImpl.FeedResult.KO);
        Patient patient = new Patient();

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_CREATE_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_INPUT_NAME,
                    patient));
        } catch (MappingException e){
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        ProcessResponse processResponse = patientFeedProcessingService.processPatientCreate(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
        assertNotNull(processResponse.getReport(), "ProcessResponse.report shall not be null !");
        assertEquals(TestResultType.FAILURE, processResponse.getReport().getResult(), "Result of the processing shall be success !");
        assertNotNull(processResponse.getReport().getReports(), "Report shall contain error");
        assertNotNull(processResponse.getReport().getReports().getInfoOrWarningOrError(), "Report shall contain error");
        assertEquals(1, processResponse.getReport().getReports().getInfoOrWarningOrError().size(), "Report shall contain a single error");
        assertNotNull(processResponse.getReport().getReports().getInfoOrWarningOrError().get(0), "Report shall contain error");
        assertNotNull(((BAR) processResponse.getReport().getReports().getInfoOrWarningOrError().get(0).getValue()).getDescription(),
                "Report shall contain error with valued description !");
    }

    /**
     * Test Processing a patient feed, service returning null.
     */
    @Test
    @Covers(requirements = {"PATREG-31"})
    @Description("Creation of a patient with an null return")
    public void process_with_null_return() {
        testPatientFeedServiceImpl.setFeedResult(TestPatientFeedServiceImpl.FeedResult.NULL);
        Patient patient = new Patient();

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_CREATE_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_INPUT_NAME,
                    patient));
        } catch (MappingException e){
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }
        assertThrows(IllegalArgumentException.class, () -> patientFeedProcessingService.processPatientCreate(processRequest));
    }

    /**
     * Test Processing a patient feed, error case using and AnyContent input that is not convertible to a Patient.
     */
    @Test
    @Covers(requirements = {"PATREG-32","PATREG-33"})
    @Description("Creation of a patient with a STRING object instead of Patient object")
    public void process_Incorrect_Patient() {
        AnyContent anyContent = new AnyContent();
        anyContent.setName("Test");
        anyContent.setValue("TarteAuxPoiresDuVergerDeMonAdolescence");
        anyContent.setEmbeddingMethod(ValueEmbeddingEnumeration.STRING);
        anyContent.setType("string");
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_CREATE_OPERATION);
        processRequest.getInput().add(anyContent);
        assertThrows(IllegalArgumentException.class, () -> patientFeedProcessingService.processPatientCreate(processRequest));
    }

    /**
     * Test Processing a patient feed, error case using no input in ProcessRequest
     */
    @Test
    @Covers(requirements = {"PATREG-33","PATREG-34"})
    @Description("Creation of a patient with no input on the start of the process")
    public void process_no_input() {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_CREATE_OPERATION);
        assertThrows(IllegalArgumentException.class, () -> patientFeedProcessingService.processPatientCreate(processRequest));
    }

/*--------------------------Tests Suite w/ UPDATE methtods-------------------------------------------*/
    /**
     * Test Processing a patient update, nominal case.
     */
    @Test
    @Description("Updating Patient in Nominal/OK Case")
        public void processUpdate() {
            testPatientFeedServiceImpl.setFeedResult(TestPatientFeedServiceImpl.FeedResult.OK);
            Patient patient = new Patient();
            PersonName personName = new PersonName();
            personName.setFamily("Joestar");
            patient.addName(personName);

            ProcessRequest processRequest = new ProcessRequest();
            processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_UPDATE_OPERATION);
            try {
                processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_INPUT_NAME,
                        patient));
                processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_INPUT_NAME,
                        new EntityIdentifier()));
            } catch (MappingException e){
                fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
            }

            ProcessResponse processResponse = patientFeedProcessingService.processPatientUpdate(processRequest);
            assertNotNull(processResponse, "ProcessResponse shall not be null !");
            assertEquals(1, processResponse.getOutput().size(), "Response shall contain a unique output which is all the values " +
                    " assigned to the updated patient.");
            assertEquals("object", processResponse.getOutput().get(0).getType(), "Returned Patient shall be stored in a Patient GITB AnyContent");
            assertEquals(PatientProcessingWebserviceConstants.PATIENT_OUTPUT_NAME, processResponse.getOutput().get(0).getName(), "Name of returned GITB AnyContent shall be 'Patient'");
            assertEquals(ValueEmbeddingEnumeration.BASE_64, processResponse.getOutput().get(0).getEmbeddingMethod(), "Embedding method of returned " +
                    "GITB AnyContent shall be 'BASE_64'");
            assertNotNull(processResponse.getReport(), "ProcessResponse.report shall not be null !");
            assertEquals(TestResultType.SUCCESS, processResponse.getReport().getResult(), "Result of the processing shall be success !");
        }

    /**
     * Test Processing a patient feed, service throwing an exception.
     */
    @Test
    @Description("Updating Patient in KO Case with a PatientFeedException return")
    public void processUpdate_with_PatientFeedException_return() {
        testPatientFeedServiceImpl.setFeedResult(TestPatientFeedServiceImpl.FeedResult.KO);
        Patient patient = new Patient();

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_UPDATE_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_INPUT_NAME,
                    patient));
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_INPUT_NAME,
                    new EntityIdentifier()));
        } catch (MappingException e){
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        ProcessResponse processResponse = patientFeedProcessingService.processPatientUpdate(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
        assertNotNull(processResponse.getReport(), "ProcessResponse.report shall not be null !");
        assertEquals(TestResultType.FAILURE, processResponse.getReport().getResult(), "Result of the processing shall be success !");
        assertNotNull(processResponse.getReport().getReports(), "Report shall contain error");
        assertNotNull(processResponse.getReport().getReports().getInfoOrWarningOrError(), "Report shall contain error");
        assertEquals(1, processResponse.getReport().getReports().getInfoOrWarningOrError().size(), "Report shall contain a single error");
        assertNotNull(processResponse.getReport().getReports().getInfoOrWarningOrError().get(0), "Report shall contain error");
        assertNotNull(((BAR) processResponse.getReport().getReports().getInfoOrWarningOrError().get(0).getValue()).getDescription(),
                "Report shall contain error with valued description !");
    }
    /**
     * Test Processing a patient feed, service returning null.
     */
    @Test
    @Description("Updating Patient in Null Case with a null return")
    public void processUpdate_with_null_return() {
        testPatientFeedServiceImpl.setFeedResult(TestPatientFeedServiceImpl.FeedResult.NULL);
        Patient patient = new Patient();
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_UPDATE_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_INPUT_NAME,
                    patient));
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_INPUT_NAME,
                    new EntityIdentifier()));
        } catch (MappingException e){
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }
        assertThrows(IllegalArgumentException.class, () -> patientFeedProcessingService.processPatientUpdate(processRequest));
    }

    /**
     * Test Processing a patient UPDATE, error case using and AnyContent input that is not convertible to a Patient.
     */
    @Test
    @Description("Updating Patient w/ a String object as input (incorrect Type of object)")
    public void process_Incorrect_UpdatedPatient() {
        AnyContent anyContent = new AnyContent();
        anyContent.setName("Test");
        anyContent.setValue("GateauxAuxPatatesDoucesDeMaMaman");
        anyContent.setEmbeddingMethod(ValueEmbeddingEnumeration.STRING);
        anyContent.setType("string");
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_UPDATE_OPERATION);
        processRequest.getInput().add(anyContent);
        assertThrows(IllegalArgumentException.class, () -> patientFeedProcessingService.processPatientUpdate(processRequest));
    }

    /**
     * Test Processing a patient Update, error case using no input in ProcessRequest
     */
    @Test
    @Description("Updating Patient w/ no inputs")
    public void processUpdate_no_input() {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_UPDATE_OPERATION);
        assertThrows(IllegalArgumentException.class, () -> patientFeedProcessingService.processPatientUpdate(processRequest));
    }

    /*--------------------------Tests Suite w/ MERGE methtods-------------------------------------------*/

    /**
     * Test Processing a patient to merge, nominal case.
     */
    @Test
    @Description("Merging 2 patients in Nominal/OK Case")
    public void processMerge() {
        testPatientFeedServiceImpl.setFeedResult(TestPatientFeedServiceImpl.FeedResult.OK);
        String originalPatientUUID = "123";
        String duplicatedPatientUUID="456";

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_MERGE_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.UUID_OF_ORIGINAL_PATIENT_INPUT_NAME,
                    originalPatientUUID));
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.UUID_OF_DUPLICATED_PATIENT_INPUT_NAME,
                    duplicatedPatientUUID));
        } catch (MappingException e){
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        ProcessResponse processResponse = patientFeedProcessingService.processPatientMerge(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
        assertEquals(1, processResponse.getOutput().size(), "Response shall contain 1 output which is the value of the" +
                " UUID assigned to the original patient.");
        assertEquals("string", processResponse.getOutput().get(0).getType(), "Returned Patient shall be stored in a String GITB AnyContent");
        assertEquals(PatientProcessingWebserviceConstants.UUID_OUTPUT_NAME, processResponse.getOutput().get(0).getName(), "Name of returned GITB AnyContent shall be 'uuid'");
        assertEquals(ValueEmbeddingEnumeration.STRING, processResponse.getOutput().get(0).getEmbeddingMethod(), "Embedding method of returned " +
                "GITB AnyContent shall be 'STRING'");

        assertNotNull(processResponse.getReport(), "ProcessResponse.report shall not be null !");
        assertEquals(TestResultType.SUCCESS, processResponse.getReport().getResult(), "Result of the processing shall be success !");
    }

    /**
     * Test Processing a patient Merge, service throwing an exception.
     */
    @Test
    @Description("Merging 2 patients in KO Case w/ a PatientFeedException")
    public void processMerge_with_PatientFeedException() {
        testPatientFeedServiceImpl.setFeedResult(TestPatientFeedServiceImpl.FeedResult.KO);
        String originalPatientUUID = "123";
        String duplicatedPatientUUID="456";

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_MERGE_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.UUID_OF_ORIGINAL_PATIENT_INPUT_NAME,
                    originalPatientUUID));
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.UUID_OF_DUPLICATED_PATIENT_INPUT_NAME,
                    duplicatedPatientUUID));
        } catch (MappingException e){
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }
        ProcessResponse processResponse = patientFeedProcessingService.processPatientMerge(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
        assertNotNull(processResponse.getReport(), "ProcessResponse.report shall not be null !");
        assertEquals(TestResultType.FAILURE, processResponse.getReport().getResult(), "Result of the processing shall be Failure !");
        assertNotNull(processResponse.getReport().getReports(), "Report shall contain error");
        assertNotNull(processResponse.getReport().getReports().getInfoOrWarningOrError(), "Report shall contain error");
        assertEquals(1, processResponse.getReport().getReports().getInfoOrWarningOrError().size(), "Report shall contain a single error");
        assertNotNull(processResponse.getReport().getReports().getInfoOrWarningOrError().get(0), "Report shall contain error");
        assertNotNull(((BAR) processResponse.getReport().getReports().getInfoOrWarningOrError().get(0).getValue()).getDescription(),
                "Report shall contain error with valued description !");
    }

    /**
     * Test Processing a patient Merge, service throwing an exception.
     */
    @Test
    @Description("Merging 2 patients  w/ a wrong Content Name")
    public void processMerge_with_wrongContentName() {
        testPatientFeedServiceImpl.setFeedResult(TestPatientFeedServiceImpl.FeedResult.OK);
        String originalPatientUUID = "123";
        String duplicatedPatientUUID="456";

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_MERGE_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.UUID_OF_DUPLICATED_PATIENT_INPUT_NAME,
                    originalPatientUUID));
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.UUID_OF_ORIGINAL_PATIENT_INPUT_NAME,
                    duplicatedPatientUUID));
        } catch (MappingException e){
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }
        assertThrows(IllegalArgumentException.class, ()-> patientFeedProcessingService.processPatientMerge(processRequest));
    }
    /**
     * Test Processing a patient merge, service returning null.
     */
    @Test
    @Description("Merging 2 patients in Null Case w/ a null return")
    public void processMerge_with_null_return() {
        testPatientFeedServiceImpl.setFeedResult(TestPatientFeedServiceImpl.FeedResult.NULL);
        String originalPatientUUID = "123";
        String duplicatedPatientUUID="456";

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_MERGE_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.UUID_OF_ORIGINAL_PATIENT_INPUT_NAME,
                    originalPatientUUID));
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.UUID_OF_DUPLICATED_PATIENT_INPUT_NAME,
                    duplicatedPatientUUID));
        } catch (MappingException e){
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }
        assertThrows(IllegalArgumentException.class, () -> patientFeedProcessingService.processPatientMerge(processRequest));
    }

    /**
     * Test Processing a patient MERGE, error case using and AnyContent input that is not convertible to a Patient.
     */
    @Test
    @Description("Merging 2 patients w/ a patient object as input (incorrect Type of object)")
    public void process_Incorrect_MergedPatient() {
        AnyContent anyContent = new AnyContent();
        anyContent.setName("Test");
        anyContent.setValue("GateauxAuxPatatesDoucesDeMaMaman");
        anyContent.setEmbeddingMethod(ValueEmbeddingEnumeration.BASE_64);
        anyContent.setType("patient");
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_MERGE_OPERATION);
        processRequest.getInput().add(anyContent);
        assertThrows(IllegalArgumentException.class, () -> patientFeedProcessingService.processPatientMerge(processRequest));
    }
    /**
     * Test Processing a patient merge, error case using no input in ProcessRequest
     */
    @Test
    @Description("Merging 2 patients w/ no inputs")
    public void processMerge_no_input() {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_MERGE_OPERATION);
        assertThrows(IllegalArgumentException.class, () -> patientFeedProcessingService.processPatientMerge(processRequest));
    }


    /*--------------------------Tests Suite w/ DELETE methtods-------------------------------------------*/

    /**
     * Test Processing a patient to delete, nominal case.
     */
    @Test
    @Description("Deleting 1 patient in Nominal/OK Case")
    public void processDelete() {
        String uuidPatient="123";

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_DELETE_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.UUID_PATIENT_TO_DELETE_INPUT_NAME,
                    uuidPatient));
        } catch (MappingException e){
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        ProcessResponse processResponse = patientFeedProcessingService.processPatientDelete(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
        assertEquals(1, processResponse.getOutput().size(), "Response shall contain 1 output which is the value of the" +
                " UUID assigned to the deleted patient.");
        assertEquals("string", processResponse.getOutput().get(0).getType(), "Returned Patient shall be stored in a String GITB AnyContent");
        assertEquals(PatientProcessingWebserviceConstants.DELETE_STATUS_OUTPUT_NAME, processResponse.getOutput().get(0).getName(), "Name of returned GITB AnyContent shall be 'UUID'");
        assertEquals(PatientProcessingWebserviceConstants.DELETE_STATUS_DONE,processResponse.getOutput().get(0).getValue(), "Name of Returned should be 'Done'");
        assertEquals(ValueEmbeddingEnumeration.STRING, processResponse.getOutput().get(0).getEmbeddingMethod(), "Embedding method of returned " +
                "GITB AnyContent shall be 'STRING'");

        assertNotNull(processResponse.getReport(), "ProcessResponse.report shall not be null !");
        assertEquals(TestResultType.SUCCESS, processResponse.getReport().getResult(), "Result of the processing shall be success !");
    }

    /**
     * Test Processing a patient merge, with a false return.
     */
    @Test
    @Description("Deleting 1 patient in Nominal/OK Case w/ FALSE return")
    public void processDelete_with_false_return() {
        String uuidPatient="789";

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_DELETE_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.UUID_PATIENT_TO_DELETE_INPUT_NAME,
                    uuidPatient));
        } catch (MappingException e){
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        ProcessResponse processResponse = patientFeedProcessingService.processPatientDelete(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
        assertEquals(1, processResponse.getOutput().size(), "Response shall contain 1 output which is the value of the" +
                " Delete status.");
        assertEquals("string", processResponse.getOutput().get(0).getType(), "Returned Patient shall be stored in a String GITB AnyContent");
        assertEquals(PatientProcessingWebserviceConstants.DELETE_STATUS_OUTPUT_NAME, processResponse.getOutput().get(0).getName(), "Name of returned GITB AnyContent shall be 'UUID'");
        assertEquals(ValueEmbeddingEnumeration.STRING, processResponse.getOutput().get(0).getEmbeddingMethod(), "Embedding method of returned " +
                "GITB AnyContent shall be 'STRING'");
        assertEquals(PatientProcessingWebserviceConstants.DELETE_STATUS_GONE,processResponse.getOutput().get(0).getValue(), "Name of Returned should be 'Gone'");
        assertNotNull(processResponse.getReport(), "ProcessResponse.report shall not be null !");
        assertEquals(TestResultType.SUCCESS, processResponse.getReport().getResult(), "Result of the processing shall be success !");
    }

    /**
     * Test Processing a patient Delete, service throwing an exception.
     */
    @Test
    @Description("Deleting 1 patient in KO Case w/ a PatientFeedException return")
    public void processDelete_with_PatientFeedException() {
        String uuidPatient="456";

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_DELETE_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.UUID_PATIENT_TO_DELETE_INPUT_NAME,
                    uuidPatient));
        } catch (MappingException e){
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        ProcessResponse processResponse = patientFeedProcessingService.processPatientDelete(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
        assertNotNull(processResponse.getReport(), "ProcessResponse.report shall not be null !");
        assertEquals(TestResultType.FAILURE, processResponse.getReport().getResult(), "Result of the processing shall be success !");
        assertNotNull(processResponse.getReport().getReports(), "Report shall contain error");
        assertNotNull(processResponse.getReport().getReports().getInfoOrWarningOrError(), "Report shall contain error");
        assertEquals(1, processResponse.getReport().getReports().getInfoOrWarningOrError().size(), "Report shall contain a single error");
        assertNotNull(processResponse.getReport().getReports().getInfoOrWarningOrError().get(0), "Report shall contain error");
        assertNotNull(((BAR) processResponse.getReport().getReports().getInfoOrWarningOrError().get(0).getValue()).getDescription(),
                "Report shall contain error with valued description !");
    }

    /**
     * Test Processing a patient DELETE, error case using and AnyContent input that is not convertible to a Patient.
     */
    @Test
    @Description("Deleting 1 patient w/ a Patient object as input (incorrect Type of object)")
    public void process_Incorrect_DeletedPatient() {
        AnyContent anyContent = new AnyContent();
        anyContent.setName("Test");
        anyContent.setValue("GateauxAuxPatatesDoucesDeMaMaman");
        anyContent.setEmbeddingMethod(ValueEmbeddingEnumeration.BASE_64);
        anyContent.setType("Patient");
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_DELETE_OPERATION);
        processRequest.getInput().add(anyContent);
        assertThrows(IllegalArgumentException.class, () -> patientFeedProcessingService.processPatientDelete(processRequest));
    }
    /**
     * Test Processing a patient Delete, error case using no input in ProcessRequest
     */
    @Test
    @Description("Deleting 1 patient w/ no inputs")
    public void processDelete_no_input() {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_DELETE_OPERATION);
        assertThrows(IllegalArgumentException.class, () -> patientFeedProcessingService.processPatientDelete(processRequest));
    }
}

