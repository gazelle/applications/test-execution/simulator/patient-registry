package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import net.ihe.gazelle.app.patientregistryapi.application.SearchCrossReferenceException;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.*;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.references.CrossReferenceDB;
import net.ihe.gazelle.app.patientregistryservice.application.dao.PatientCrossReferenceDAO;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.CreationCrossReferenceException;
import org.junit.Assert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@Feature("PatientCrossReferenceDAO")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PatientCrossReferenceDAOImplTest {

    private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitCrossRefTest";
    private PatientCrossReferenceDAO patientCrossReferenceDAO;

    private EntityManager entityManager;

    /**
     * database init
     */
    @BeforeAll
    public void initializeDatabase() throws ParseException {
        entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST).createEntityManager();
        patientCrossReferenceDAO = new PatientCrossReferenceDAOImpl(entityManager);
        entityManager.getTransaction().begin();
        addTestEntitiesToDB(entityManager);
    }

    private void addTestEntitiesToDB(EntityManager entityManager) throws ParseException {
        initializeDb(entityManager);
    }

    /**
     * Close the connection to the database after all tests are executed.
     */
    @AfterAll
    public void closeDatabase() {
        entityManager.clear();
        entityManager.close();
    }

    @Test
    void testNullEntityManager() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> new PatientCrossReferenceDAOImpl(null));
        assertEquals("EntityManager cannot be null", exception.getMessage());
    }

    @Test
    void testEmptyCrossReference() throws SearchCrossReferenceException {
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("3333");
        entityIdentifier.setValue("7777");
        PatientDB patientDB = patientCrossReferenceDAO.searchForPatientDBWithSourceIdentifier(entityIdentifier.getSystemIdentifier(),
                entityIdentifier.getValue());
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceDAO.searchForPatientAliasesWithPatientDB(10));
        assertEquals("Search error : No unique X-ref for given Patient", exception.getMessage());
    }

    @Test
    void testAllValid() throws SearchCrossReferenceException {
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("1111");
        entityIdentifier.setValue("1235");
        PatientDB patientDB = patientCrossReferenceDAO.searchForPatientDBWithSourceIdentifier(entityIdentifier.getSystemIdentifier(),
                entityIdentifier.getValue());
        assertEquals("Dupont", patientDB.getLastName());
       CrossReferenceDB crossReferenceDB =  patientCrossReferenceDAO.searchForPatientAliasesWithPatientDB(patientDB.getPixReference().getId());
        assertEquals(2,crossReferenceDB.getPatients().size());
    }

    @Test
    void testNullEntityIdentifier() {
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceDAO.searchForPatientDBWithSourceIdentifier(null, "132"));
        assertEquals("The system identifier from entityIdentifier cannot be null or empty", exception.getMessage());
    }

    @Test
    void testNoSystemIdentifierInEntityIdentifier() {
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceDAO.searchForPatientDBWithSourceIdentifier(entityIdentifier.getSystemIdentifier(),
                        entityIdentifier.getValue()));
        assertEquals("The system identifier from entityIdentifier cannot be null or empty", exception.getMessage());
    }

    @Test
    void testBlankSystemIdentifierInEntityIdentifier() {
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("");
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceDAO.searchForPatientDBWithSourceIdentifier(entityIdentifier.getSystemIdentifier(),
                        entityIdentifier.getValue()));
        assertEquals("The system identifier from entityIdentifier cannot be null or empty", exception.getMessage());
    }

    @Test
    void testNoValueInEntityIdentifier() {
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("toto");
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceDAO.searchForPatientDBWithSourceIdentifier(entityIdentifier.getSystemIdentifier(),
                        entityIdentifier.getValue()));
        assertEquals("The value from entityIdentifier cannot be null or empty", exception.getMessage());
    }

    @Test
    void testBlankValueInEntityIdentifier() {
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("toto");
        entityIdentifier.setValue("");
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceDAO.searchForPatientDBWithSourceIdentifier(entityIdentifier.getSystemIdentifier(),
                        entityIdentifier.getValue()));
        assertEquals("The value from entityIdentifier cannot be null or empty", exception.getMessage());
    }

    @Test
    void testNoHierarchicDesignatorDB() {
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("unknown-hierarchicDesignator");
        entityIdentifier.setValue("test");
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceDAO.searchForPatientDBWithSourceIdentifier(entityIdentifier.getSystemIdentifier(),
                        entityIdentifier.getValue()));
        assertEquals("Error in the sourceIdentifier : System does not exist", exception.getMessage());
    }

    @Test
    void testNoPatientIdentifierDB() {
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("1111");
        entityIdentifier.setValue("unknown");
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceDAO.searchForPatientDBWithSourceIdentifier(entityIdentifier.getSystemIdentifier(),
                        entityIdentifier.getValue()));
        assertEquals("Error in the sourceIdentifier : it does not match any identity", exception.getMessage());
    }

    @Test
    void testNoPatientDB() {
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("4444");
        entityIdentifier.setValue("8888");
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceDAO.searchForPatientDBWithSourceIdentifier(entityIdentifier.getSystemIdentifier(),
                        entityIdentifier.getValue()));
        assertEquals("Error in the sourceIdentifier : it does not match any Patient", exception.getMessage());
    }


    @Test
    @Description("Test method create a X Reference")
    void createXRefEmpty() throws CreationCrossReferenceException, SearchCrossReferenceException {
        CrossReferenceDB crossReferenceDB = new CrossReferenceDB();
        crossReferenceDB.setLastModifier("fde");
        PatientDB patientDB = new PatientDB();
        patientDB.setUuid("test");

        List<PatientDB> patientDBS = new ArrayList<>();
        patientDBS.add(patientDB);
        patientDB.setPixReference(crossReferenceDB);
        entityManager.persist(patientDB);
        crossReferenceDB.setPatients(patientDBS);

        crossReferenceDB = patientCrossReferenceDAO.createCrossReference(crossReferenceDB);
        CrossReferenceDB referenceDB = patientCrossReferenceDAO.searchForPatientAliasesWithPatientDB(crossReferenceDB.getId());
        Assert.assertNotNull("List of XRef shall not be null", referenceDB);
    }


    private void initializeDb(EntityManager entityManager) {
        HierarchicDesignatorDB hierarchicDesignatorDB = new HierarchicDesignatorDB();
        hierarchicDesignatorDB.setUniversalID("1111");
        hierarchicDesignatorDB.setUniversalIDType("ISO");
        hierarchicDesignatorDB.setUsage(DesignatorTypeDB.PATIENT_ID);
        HierarchicDesignatorDB hierarchicDesignatorDB2 = entityManager.merge(hierarchicDesignatorDB);

        HierarchicDesignatorDB hierarchicDesignatorDB3 = new HierarchicDesignatorDB();
        hierarchicDesignatorDB3.setUniversalID("2222");
        hierarchicDesignatorDB3.setUniversalIDType("ISO");
        hierarchicDesignatorDB3.setUsage(DesignatorTypeDB.PATIENT_ID);
        HierarchicDesignatorDB hierarchicDesignatorDB4 = entityManager.merge(hierarchicDesignatorDB3);

        HierarchicDesignatorDB hierarchicDesignatorDB5 = new HierarchicDesignatorDB();
        hierarchicDesignatorDB5.setUniversalID("3333");
        hierarchicDesignatorDB5.setUniversalIDType("ISO");
        hierarchicDesignatorDB5.setUsage(DesignatorTypeDB.PATIENT_ID);
        HierarchicDesignatorDB hierarchicDesignatorDB6 = entityManager.merge(hierarchicDesignatorDB5);

        HierarchicDesignatorDB hierarchicDesignatorDB7 = new HierarchicDesignatorDB();
        hierarchicDesignatorDB7.setUniversalID("4444");
        hierarchicDesignatorDB7.setUniversalIDType("ISO");
        hierarchicDesignatorDB7.setUsage(DesignatorTypeDB.PATIENT_ID);
        HierarchicDesignatorDB hierarchicDesignatorDB8 = entityManager.merge(hierarchicDesignatorDB7);

        PatientIdentifierDB patientIdentifierDB = new PatientIdentifierDB();
        patientIdentifierDB.setDomain(hierarchicDesignatorDB2);
        patientIdentifierDB.setIdentifier("1235");
        PatientIdentifierDB patientIdentifierDBmerged = entityManager.merge(patientIdentifierDB);

        PatientIdentifierDB patientIdentifierDB2 = new PatientIdentifierDB();
        patientIdentifierDB2.setDomain(hierarchicDesignatorDB4);
        patientIdentifierDB2.setIdentifier("1236");
        PatientIdentifierDB patientIdentifierDB2merged = entityManager.merge(patientIdentifierDB2);

        PatientIdentifierDB patientIdentifierDB3 = new PatientIdentifierDB();
        patientIdentifierDB3.setDomain(hierarchicDesignatorDB6);
        patientIdentifierDB3.setIdentifier("7777");
        PatientIdentifierDB patientIdentifierDB3merged = entityManager.merge(patientIdentifierDB3);

        PatientIdentifierDB patientIdentifierDB4 = new PatientIdentifierDB();
        patientIdentifierDB4.setDomain(hierarchicDesignatorDB8);
        patientIdentifierDB4.setIdentifier("8888");
        PatientIdentifierDB patientIdentifierDB4merged = entityManager.merge(patientIdentifierDB4);

        PatientDB patient1 = new PatientDB();
        patient1.setUuid("1235");
        patient1.setStillActive(true);
        patient1.setGenderCode(GenderCodeDB.F);
        patient1.setLastName("Dupont");
        List<PatientIdentifierDB> patientIdentifierDBS = new ArrayList<>();
        patientIdentifierDBS.add(patientIdentifierDBmerged);
        patient1.setPatientIdentifiers(patientIdentifierDBS);
        PatientDB patient1m = entityManager.merge(patient1);

        PatientDB patient2 = new PatientDB();
        patient2.setUuid("9999");
        patient2.setStillActive(false);
        patient2.setGenderCode(GenderCodeDB.M);
        patient2.setLastName("Martin");
        List<PatientIdentifierDB> patientIdentifierDBS2 = new ArrayList<>();
        patientIdentifierDBS2.add(patientIdentifierDB2merged);
        patient2.setPatientIdentifiers(patientIdentifierDBS2);
        PatientDB patient2m = entityManager.merge(patient2);

        PatientDB patient3 = new PatientDB();
        patient3.setUuid("7777");
        patient3.setStillActive(false);
        patient3.setGenderCode(GenderCodeDB.M);
        patient3.setLastName("Noah");
        List<PatientIdentifierDB> patientIdentifierDBS3 = new ArrayList<>();
        patientIdentifierDBS3.add(patientIdentifierDB3merged);
        patient3.setPatientIdentifiers(patientIdentifierDBS3);

        PatientDB patient3m = entityManager.merge(patient3);

        List<PatientDB> patientDBS = new ArrayList<>();
        patientDBS.add(patient1m);
        patientDBS.add(patient2m);

        CrossReferenceDB crossReferenceDB = new CrossReferenceDB();
        crossReferenceDB.setLastModifier("fde");
        CrossReferenceDB crossReferenceDBMerged = entityManager.merge(crossReferenceDB);

        patient1m.setPixReference(crossReferenceDBMerged);
        entityManager.merge(patient1m);

        patient2m.setPixReference(crossReferenceDBMerged);
        entityManager.merge(patient2m);

        CrossReferenceDB crossReferenceDB2 = new CrossReferenceDB();
        crossReferenceDB2.setLastModifier("mbr");
        entityManager.persist(crossReferenceDB2);
        entityManager.merge(crossReferenceDB2);

        CrossReferenceDB crossReferenceDB3 = new CrossReferenceDB();
        crossReferenceDB3.setLastModifier("mbr2");
        crossReferenceDB3.setPatients(new ArrayList<>());
        patient3m.setPixReference(crossReferenceDB3);
        entityManager.merge(patient3m);
    }

    @Test
    @Description("Try use methods without parameter")
    void expectedExceptionThrows() {
        assertThrows(IllegalArgumentException.class, () -> patientCrossReferenceDAO.createCrossReference(null));
        assertThrows(IllegalArgumentException.class, () -> patientCrossReferenceDAO.searchForPatientAliasesWithPatientDB(null));

    }
}