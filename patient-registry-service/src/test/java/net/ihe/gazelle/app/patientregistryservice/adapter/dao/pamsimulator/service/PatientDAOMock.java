package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service;

import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.app.patientregistryservice.application.dao.PatientDAO;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.*;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;

import java.util.ArrayList;
import java.util.List;

/**
 * Test implementation of Patient DAO.
 */
public class PatientDAOMock implements PatientDAO {

    private SearchResult searchResult;
    private RetrieveResult retrieveResult;

    /**
     * Default constructor for the class.
     */
    public PatientDAOMock() {
        retrieveResult = RetrieveResult.OK;
        searchResult = SearchResult.OK;
    }

    /**
     * Setter for the retrieveResult property
     *
     * @param retrieveResult value to set
     */
    public void setRetrieveResult(RetrieveResult retrieveResult) {
        this.retrieveResult = retrieveResult;
    }

    /**
     * Setter for the searchResult property
     *
     * @param searchResult value to set
     */
    public void setSearchResult(SearchResult searchResult) {
        this.searchResult = searchResult;
    }

    /***
     * List of patient returned by default by this class.
     * @return a default list of patients
     */
    public List<Patient> getReturnedPatientList() {
        List<Patient> patients = new ArrayList<>();
        Patient patient1 = new Patient();
        patient1.setUuid("uuid1");
        Patient patient2 = new Patient();
        patient1.setUuid("uuid2");
        patients.add(patient1);
        patients.add(patient2);
        return patients;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPatientExisting(String uuid) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createPatient(Patient patient) {
    }

    @Override
    public void createPatientDB(PatientDB patientDB) throws PatientCreationException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Patient> readPatient(SearchCriteria searchCriteria) throws PatientReadException {
        if (SearchResult.OK.equals(searchResult)) {
            return getReturnedPatientList();
        } else {
            throw new PatientReadException();
        }
    }

    @Override
    public List<Patient> searchPatients(List<SearchParameter> parameters) {
            return getReturnedPatientList();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Patient readPatient(String uuid) throws PatientReadException, PatientNotFoundException {
        switch (retrieveResult) {
            case OK:
                return new Patient();
            case PATIENT_READ_EXCEPTION:
                throw new PatientReadException();
            case PATIENT_NOT_FOUND_EXCEPTION:
                throw new PatientNotFoundException();
            default:
                return null;
        }
    }

    @Override
    public PatientDB readPatientDB(String uuid) throws PatientReadException, PatientNotFoundException {
        return null;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void deletePatient(Patient patient) {

    }

    @Override
    public void deletePatient(String patientUuid) throws PatientDeleteException {

    }

    @Override
    public void deletePatientDB(PatientDB patientDB) throws PatientDeleteException {

    }

    @Override
    public PatientDB updatePatientDB(PatientDB patientDB) throws PatientUpdateException {
        return null;
    }

    @Override
    public List<PatientDB> retrieveMatchingPatients(PatientDB patient) throws PatientNotFoundException {
        return null;
    }

    @Override
    public PatientDB readFirstPatientByIdentifier(EntityIdentifier identifier) throws PatientNotFoundException {
        return null;
    }

    @Override
    public List<PatientDB> readAllPatientsByIdentifier(EntityIdentifier identifier) throws PatientNotFoundException {
        return null;
    }


    /**
     * Enumeration of retrieve result that can be returned by this class.
     */
    public enum RetrieveResult {
        OK,
        PATIENT_READ_EXCEPTION,
        PATIENT_NOT_FOUND_EXCEPTION
    }

    /**
     * Enumeration of search result that can be returned by this class.
     */
    public enum SearchResult {
        OK,
        PATIENT_READ_EXCEPTION,
    }
}
