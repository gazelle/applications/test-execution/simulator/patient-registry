package net.ihe.gazelle.app.patientregistryservice.application;

import net.ihe.gazelle.app.patientregistryapi.application.PatientRetrieveException;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.service.PatientDAOMock;
import net.ihe.gazelle.app.patientregistryservice.application.exceptions.PatientNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Tests for {@link PatientRetrieveApplication}
 */
public class PatientRetrieveApplicationTest {

    private PatientDAOMock patientDAO;
    private PatientRetrieveApplication patientRetrieveApplication;

    /**
     * Initialize implementations to be tested before each test.
     */
    @BeforeEach
    public void init(){
        patientDAO = new PatientDAOMock();
        patientRetrieveApplication = new PatientRetrieveApplication(patientDAO);
    }

    /**
     * Check the retrieve in nominal case
     *
     * @throws PatientRetrieveException if an exception is thrown during the retrieve operation.
     */
    @Test
    public void retrieve_ok() throws PatientRetrieveException {
        assertNotNull(patientRetrieveApplication.retrievePatient("uuid"));
    }

    /**
     * Checsk the retrieve when a {@link PatientRetrieveException} is thrown.
     */
    @Test
    public void retrieve_PatientReadException() {
        patientDAO.setRetrieveResult(PatientDAOMock.RetrieveResult.PATIENT_READ_EXCEPTION);

        assertThrows(PatientRetrieveException.class, () -> patientRetrieveApplication.retrievePatient("uuid"));
    }

    /**
     * Checsk the retrieve when a {@link PatientNotFoundException} is thrown.
     */
    @Test
    public void retrieve_PatientNotFound() {
        patientDAO.setRetrieveResult(PatientDAOMock.RetrieveResult.PATIENT_NOT_FOUND_EXCEPTION);

        assertThrows(PatientRetrieveException.class, () -> patientRetrieveApplication.retrievePatient("uuid"));
    }
}
