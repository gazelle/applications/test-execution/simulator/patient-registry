package net.ihe.gazelle.app.patientregistryservice.adapter.dao;

import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 * Test implementation of a {@link Produces} annotated class. Used to inject {@link EntityManager} corresponding to test Datasource.
 */
public class TestEntityManagerProducer {

    private static final String PERSISTENCE_UNIT_NAME_CONST = "PersistenceUnitTest";

    @Produces
    @EntityManagerProducer.InjectEntityManager
    private EntityManager entityManager = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME_CONST).createEntityManager();
}
