package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.converter;

import net.ihe.gazelle.app.patientregistryapi.business.ContactPoint;
import net.ihe.gazelle.app.patientregistryapi.business.ContactPointUse;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientPhoneNumberDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PhoneNumberTypeDB;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Test conversion ContactPoint from Business model to Database model or the opposite.
 */
class ContactPointConverterTest {

    /**
     * Map contact point value
     */
    @Test
    public void mapContactPointValueTest() {
        ContactPoint contact = new ContactPoint();
        String value = "value";
        contact.setValue(value);

        PatientPhoneNumberDB contactDB = ContactPointConverter.toContactPointDB(contact,null);

        assertEquals(value, contactDB.getValue());
    }

    /**
     * Map contact point Patient
     */
    @Test
    public void mapContactPointPatientTest() {
        ContactPoint contact = new ContactPoint();
        PatientDB patient = new PatientDB();

        PatientPhoneNumberDB contactDB = ContactPointConverter.toContactPointDB(contact,patient);

        assertEquals(patient, contactDB.getPatient());
    }

    /**
     * Map contact point type (MOBILE)
     */
    @Test
    public void mapContactPointTypePhoneTest() {
        ContactPoint contact = new ContactPoint();
        contact.setUse(ContactPointUse.MOBILE);

        PatientPhoneNumberDB contactDB = ContactPointConverter.toContactPointDB(contact,null);

        assertEquals(PhoneNumberTypeDB.MOBILE, contactDB.getType());
    }

    /**
     * Map contact point value (BEEPER)
     */
    @Test
    public void mapContactPointTypeBeeperTest() {
        ContactPoint contact = new ContactPoint();
        contact.setUse(ContactPointUse.BEEPER);

        PatientPhoneNumberDB contactDB = ContactPointConverter.toContactPointDB(contact,null);

        assertEquals(PhoneNumberTypeDB.BEEPER, contactDB.getType());
    }

    /**
     * Map contact point value (OTHER)
     */
    @Test
    public void mapContactPointTypeOtherTest() {
        ContactPoint contact = new ContactPoint();
        contact.setUse(ContactPointUse.OTHER);

        PatientPhoneNumberDB contactDB = ContactPointConverter.toContactPointDB(contact,null);

        assertEquals(PhoneNumberTypeDB.OTHER, contactDB.getType());
    }

    /**
     * Map contact point value (EMERGENCY)
     */
    @Test
    public void mapContactPointUseEmergencyTest() {
        ContactPoint contact = new ContactPoint();
        contact.setUse(ContactPointUse.EMERGENCY);

        PatientPhoneNumberDB contactDB = ContactPointConverter.toContactPointDB(contact,null);

        assertEquals(PhoneNumberTypeDB.EMERGENCY, contactDB.getType());
    }

    /**
     * Map contact point value (TEMPORARY)
     */
    @Test
    public void mapContactPointUseTemporaryTest() {
        ContactPoint contact = new ContactPoint();
        contact.setUse(ContactPointUse.TEMPORARY);

        PatientPhoneNumberDB contactDB = ContactPointConverter.toContactPointDB(contact,null);

        assertEquals(PhoneNumberTypeDB.VACATION, contactDB.getType());
    }

    /**
     * Map contact point value (WORK)
     */
    @Test
    public void mapContactPointUseWorkTest() {
        ContactPoint contact = new ContactPoint();
        contact.setUse(ContactPointUse.WORK);

        PatientPhoneNumberDB contactDB = ContactPointConverter.toContactPointDB(contact,null);

        assertEquals(PhoneNumberTypeDB.WORK, contactDB.getType());
    }

    /**
     * Map contact point value (HOME)
     */
    @Test
    public void mapContactPointUseHomeTest() {
        ContactPoint contact = new ContactPoint();
        contact.setUse(ContactPointUse.HOME);

        PatientPhoneNumberDB contactDB = ContactPointConverter.toContactPointDB(contact,null);

        assertEquals(PhoneNumberTypeDB.HOME, contactDB.getType());
    }

    /**
     * Map mobile ContactPoint from DB model to business model
     */
    @Test
    public void mapMobileToContactPoint(){
        PatientPhoneNumberDB patientPhoneNumberDB = new PatientPhoneNumberDB();
        patientPhoneNumberDB.setType(PhoneNumberTypeDB.MOBILE);
        patientPhoneNumberDB.setValue("MyValue");

        ContactPoint contactPoint = ContactPointConverter.toContactPoint(patientPhoneNumberDB);

        assertNotNull(contactPoint);
        assertEquals(null, contactPoint.getType());
        assertEquals(ContactPointUse.MOBILE, contactPoint.getUse());
        assertEquals("MyValue", contactPoint.getValue());
    }

    /**
     * Map home ContactPoint from DB model to business model
     */
    @Test
    public void mapHomeToContactPoint(){
        PatientPhoneNumberDB patientPhoneNumberDB = new PatientPhoneNumberDB();
        patientPhoneNumberDB.setType(PhoneNumberTypeDB.HOME);
        patientPhoneNumberDB.setValue("MyValue");

        ContactPoint contactPoint = ContactPointConverter.toContactPoint(patientPhoneNumberDB);

        assertNotNull(contactPoint);
        assertEquals(null, contactPoint.getType());
        assertEquals(ContactPointUse.HOME, contactPoint.getUse());
        assertEquals("MyValue", contactPoint.getValue());
    }

    /**
     * Map beeper ContactPoint from DB model to business model
     */
    @Test
    public void mapBeeperToContactPoint(){
        PatientPhoneNumberDB patientPhoneNumberDB = new PatientPhoneNumberDB();
        patientPhoneNumberDB.setType(PhoneNumberTypeDB.BEEPER);
        patientPhoneNumberDB.setValue("MyValue");

        ContactPoint contactPoint = ContactPointConverter.toContactPoint(patientPhoneNumberDB);

        assertNotNull(contactPoint);
        assertEquals(null, contactPoint.getType());
        assertEquals(ContactPointUse.BEEPER, contactPoint.getUse());
        assertEquals("MyValue", contactPoint.getValue());
    }

    /**
     * Map other ContactPoint from DB model to business model
     */
    @Test
    public void mapOtherToContactPoint(){
        PatientPhoneNumberDB patientPhoneNumberDB = new PatientPhoneNumberDB();
        patientPhoneNumberDB.setType(PhoneNumberTypeDB.OTHER);
        patientPhoneNumberDB.setValue("MyValue");

        ContactPoint contactPoint = ContactPointConverter.toContactPoint(patientPhoneNumberDB);

        assertNotNull(contactPoint);
        assertEquals(null, contactPoint.getType());
        assertEquals(ContactPointUse.OTHER, contactPoint.getUse());
        assertEquals("MyValue", contactPoint.getValue());
    }

    /**
     * Map emergency ContactPoint from DB model to business model
     */
    @Test
    public void mapEmergencyToContactPoint(){
        PatientPhoneNumberDB patientPhoneNumberDB = new PatientPhoneNumberDB();
        patientPhoneNumberDB.setType(PhoneNumberTypeDB.EMERGENCY);
        patientPhoneNumberDB.setValue("MyValue");

        ContactPoint contactPoint = ContactPointConverter.toContactPoint(patientPhoneNumberDB);

        assertNotNull(contactPoint);
        assertEquals(null, contactPoint.getType());
        assertEquals(ContactPointUse.EMERGENCY, contactPoint.getUse());
        assertEquals("MyValue", contactPoint.getValue());
    }

    /**
     * Map vacancy ContactPoint from DB model to business model
     */
    @Test
    public void mapVacancyToContactPoint(){
        PatientPhoneNumberDB patientPhoneNumberDB = new PatientPhoneNumberDB();
        patientPhoneNumberDB.setType(PhoneNumberTypeDB.VACATION);
        patientPhoneNumberDB.setValue("MyValue");

        ContactPoint contactPoint = ContactPointConverter.toContactPoint(patientPhoneNumberDB);

        assertNotNull(contactPoint);
        assertEquals(null, contactPoint.getType());
        assertEquals(ContactPointUse.TEMPORARY, contactPoint.getUse());
        assertEquals("MyValue", contactPoint.getValue());
    }

    /**
     * Map work ContactPoint from DB model to business model
     */
    @Test
    public void mapWorkToContactPoint(){
        PatientPhoneNumberDB patientPhoneNumberDB = new PatientPhoneNumberDB();
        patientPhoneNumberDB.setType(PhoneNumberTypeDB.WORK);
        patientPhoneNumberDB.setValue("MyValue");

        ContactPoint contactPoint = ContactPointConverter.toContactPoint(patientPhoneNumberDB);

        assertNotNull(contactPoint);
        assertEquals(null, contactPoint.getType());
        assertEquals(ContactPointUse.WORK, contactPoint.getUse());
        assertEquals("MyValue", contactPoint.getValue());
    }

    /**
     * Map no type ContactPoint from DB model to business model
     */
    @Test
    public void mapNoTypeToContactPoint(){
        PatientPhoneNumberDB patientPhoneNumberDB = new PatientPhoneNumberDB();
        patientPhoneNumberDB.setType(null);
        patientPhoneNumberDB.setValue("MyValue");

        ContactPoint contactPoint = ContactPointConverter.toContactPoint(patientPhoneNumberDB);

        assertNotNull(contactPoint);
        assertEquals(null, contactPoint.getType());
        assertEquals(null, contactPoint.getUse());
        assertEquals("MyValue", contactPoint.getValue());
    }
}