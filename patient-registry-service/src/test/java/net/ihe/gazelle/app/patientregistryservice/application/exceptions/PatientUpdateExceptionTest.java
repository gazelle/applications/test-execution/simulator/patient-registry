package net.ihe.gazelle.app.patientregistryservice.application.exceptions;

import io.qameta.allure.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

@Feature("Coverage on exception")
@Story("PatientUpdateException")
class PatientUpdateExceptionTest {
    /**
     * Dummy Test for coverage
     *
     * @throws PatientUpdateException testException
     */
    @Test
    @Description("PatientUpdateException")
    @Severity(SeverityLevel.MINOR)
    void generateException() {
        assertThrows(PatientUpdateException.class, () -> {
            throw new PatientUpdateException("Test");
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientUpdateException testException
     */
    @Test
    @Description("PatientUpdateException")
    @Severity(SeverityLevel.MINOR)
    void generateException1() {
        assertThrows(PatientUpdateException.class, () -> {
            throw new PatientUpdateException("Test", new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientUpdateException testException
     */
    @Test
    @Description("PatientUpdateException")
    @Severity(SeverityLevel.MINOR)
    void generateException2() {
        assertThrows(PatientUpdateException.class, () -> {
            throw new PatientUpdateException(new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientUpdateException testException
     */
    @Test
    @Description("PatientUpdateException")
    @Severity(SeverityLevel.MINOR)
    void generateException4() {
        assertThrows(PatientUpdateException.class, () -> {
            throw new PatientUpdateException("Test", new IllegalArgumentException("test"), false, true);
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientUpdateException testException
     */
    @Test
    @Description("PatientUpdateException")
    @Severity(SeverityLevel.MINOR)
    void generateException3() {
        assertThrows(PatientUpdateException.class, () -> {
            throw new PatientUpdateException();
        });
    }
}