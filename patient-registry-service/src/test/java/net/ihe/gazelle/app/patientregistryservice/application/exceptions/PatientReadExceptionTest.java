package net.ihe.gazelle.app.patientregistryservice.application.exceptions;

import io.qameta.allure.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

@Feature("Coverage on exception")
@Story("PatientReadException")
class PatientReadExceptionTest {
    /**
     * Dummy Test for coverage
     *
     * @throws PatientReadException testException
     */
    @Test
    @Description(" PatientReadException")
    @Severity(SeverityLevel.MINOR)
    void generateException() {
        assertThrows(PatientReadException.class, () -> {
            throw new PatientReadException("Test");
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientReadException testException
     */
    @Test
    @Description(" PatientReadException")
    @Severity(SeverityLevel.MINOR)
    void generateException1() {
        assertThrows(PatientReadException.class, () -> {
            throw new PatientReadException("Test", new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientReadException testException
     */
    @Test
    @Description(" PatientReadException")
    @Severity(SeverityLevel.MINOR)
    void generateException2() {
        assertThrows(PatientReadException.class, () -> {
            throw new PatientReadException(new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientReadException testException
     */
    @Test
    @Description(" PatientReadException")
    @Severity(SeverityLevel.MINOR)
    void generateException4() {
        assertThrows(PatientReadException.class, () -> {
            throw new PatientReadException("Test", new IllegalArgumentException("test"), false, true);
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientReadException testException
     */
    @Test
    @Description(" PatientReadException")
    @Severity(SeverityLevel.MINOR)
    void generateException3() {
        assertThrows(PatientReadException.class, () -> {
            throw new PatientReadException();
        });
    }

}