package net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.converter;

import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientDB;
import net.ihe.gazelle.app.patientregistryservice.adapter.dao.pamsimulator.model.patient.PatientIdentifierDB;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * test identifier mapping
 */
class IdentifierConverterTest {

    /**
     * test patient link
     */
    @Test
    public void mapIdentifierPatientTest() {
        EntityIdentifier identifier = new EntityIdentifier();
        PatientDB patient = new PatientDB();

        PatientIdentifierDB identifierDB = IdentifierConverter.toPatientIdentifierDB(identifier,patient);

        assertTrue(identifierDB.getPatients().contains(patient), "patient should be linked");
    }

    /**
     * test value mapping
     */
    @Test
    public void mapIdentifierValueTest() {
        EntityIdentifier identifier = new EntityIdentifier();
        String value = "value";
        identifier.setValue(value);

        PatientIdentifierDB identifierDB = IdentifierConverter.toPatientIdentifierDB(identifier,null);

        assertEquals(value,identifierDB.getIdentifier(), "value should be mapped to Id number");
    }

    /**
     * test type mapping
     */
    @Test
    public void mapIdentifierTypeTest() {
        EntityIdentifier identifier = new EntityIdentifier();
        String type = "type";
        identifier.setType(type);

        PatientIdentifierDB identifierDB = IdentifierConverter.toPatientIdentifierDB(identifier,null);

        assertEquals(type,identifierDB.getIdentifierTypeCode(), "type should be mapped to type code");
    }

    /**
     * test domain name mapping
     */
    @Test
    public void mapIdentifierDomainNameTest() {
        EntityIdentifier identifier = new EntityIdentifier();
        String id = "id";
        identifier.setSystemIdentifier(id);
        String name = "name";
        identifier.setSystemName(name);

        PatientIdentifierDB identifierDB = IdentifierConverter.toPatientIdentifierDB(identifier,null);

        assertEquals(name,identifierDB.getDomain().getNamespaceID(), "name should be mapped to namespace ID");
    }


    /**
     * test domain id mapping
     */
    @Test
    public void mapIdentifierDomainIdTest() {
        EntityIdentifier identifier = new EntityIdentifier();
        String id = "id";
        identifier.setSystemIdentifier(id);

        PatientIdentifierDB identifierDB = IdentifierConverter.toPatientIdentifierDB(identifier,null);

        assertEquals(id,identifierDB.getDomain().getUniversalID(), "doamin id should be mapped to universal ID");
    }

    /**
     * test domain id Type mapping
     */
    @Test
    public void mapIdentifierDomainIdTypeTest() {
        EntityIdentifier identifier = new EntityIdentifier();
        String id = "id";
        identifier.setSystemIdentifier(id);
        String iso = "ISO";

        PatientIdentifierDB identifierDB = IdentifierConverter.toPatientIdentifierDB(identifier,null);

        assertEquals(iso,identifierDB.getDomain().getUniversalIDType(), "universal id type should always be iso");
    }

}