package net.ihe.gazelle.app.patientregistryservice.application.exceptions;

import io.qameta.allure.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

@Feature("Coverage on exception")
@Story("PatientIdentifierException")
class PatientIdentifierExceptionTest {
    /**
     * Dummy Test for coverage
     *
     * @throws PatientIdentifierException testException
     */
    @Test
    @Description(" PatientIdentifierException")
    @Severity(SeverityLevel.MINOR)
    void generateException() {
        assertThrows(PatientIdentifierException.class, () -> {
            throw new PatientIdentifierException("Test");
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientIdentifierException testException
     */
    @Test
    @Description(" PatientIdentifierException")
    @Severity(SeverityLevel.MINOR)
    void generateException1() {
        assertThrows(PatientIdentifierException.class, () -> {
            throw new PatientIdentifierException("Test", new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientIdentifierException testException
     */
    @Test
    @Description(" PatientIdentifierException")
    @Severity(SeverityLevel.MINOR)
    void generateException2() {
        assertThrows(PatientIdentifierException.class, () -> {
            throw new PatientIdentifierException(new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientIdentifierException testException
     */
    @Test
    @Description(" PatientIdentifierException")
    @Severity(SeverityLevel.MINOR)
    void generateException4() {
        assertThrows(PatientIdentifierException.class, () -> {
            throw new PatientIdentifierException("Test", new IllegalArgumentException("test"), false, true);
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientIdentifierException testException
     */
    @Test
    @Description(" PatientIdentifierException")
    @Severity(SeverityLevel.MINOR)
    void generateException3() {
        assertThrows(PatientIdentifierException.class, () -> {
            throw new PatientIdentifierException();
        });
    }
}