package net.ihe.gazelle.app.patientregistryservice.application.exceptions;

import io.qameta.allure.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

@Feature("Coverage on exception")
@Story("PatientDeleteException")
class PatientDeleteExceptionTest {
    /**
     * Dummy Test for coverage
     *
     * @throws PatientDeleteException testException
     */
    @Test
    @Description("PatientDeleteException")
    @Severity(SeverityLevel.MINOR)
    void generateException() {
        assertThrows(PatientDeleteException.class, () -> {
            throw new PatientDeleteException("Test");
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientDeleteException testException
     */
    @Test
    @Description("PatientDeleteException")
    @Severity(SeverityLevel.MINOR)
    void generateException1() {
        assertThrows(PatientDeleteException.class, () -> {
            throw new PatientDeleteException("Test", new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientDeleteException testException
     */
    @Test
    @Description("PatientDeleteException")
    @Severity(SeverityLevel.MINOR)
    void generateException2() {
        assertThrows(PatientDeleteException.class, () -> {
            throw new PatientDeleteException(new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientDeleteException testException
     */
    @Test
    @Description("PatientDeleteException")

    @Severity(SeverityLevel.MINOR)
    void generateException4() {
        assertThrows(PatientDeleteException.class, () -> {
            throw new PatientDeleteException("Test", new IllegalArgumentException("test"), false, true);
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws PatientDeleteException testException
     */
    @Test
    @Description("PatientDeleteException")

    @Severity(SeverityLevel.MINOR)
    void generateException3() {
        assertThrows(PatientDeleteException.class, () -> {
            throw new PatientDeleteException();
        });
    }
}