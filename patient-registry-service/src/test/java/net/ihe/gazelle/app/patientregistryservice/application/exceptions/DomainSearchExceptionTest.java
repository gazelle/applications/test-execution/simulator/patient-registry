package net.ihe.gazelle.app.patientregistryservice.application.exceptions;

import io.qameta.allure.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

@Feature("Coverage on exception")
@Story("DomainSearchException")
class DomainSearchExceptionTest {
    /**
     * Dummy Test for coverage
     *
     * @throws DomainSearchException testException
     */
    @Test
    @Description(" DomainSearchException")
    @Severity(SeverityLevel.MINOR)
    void generateException() {
        assertThrows(DomainSearchException.class, () -> {
            throw new DomainSearchException("Test");
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws DomainSearchException testException
     */
    @Test
    @Description(" DomainSearchException")
    @Severity(SeverityLevel.MINOR)
    void generateException1() {
        assertThrows(DomainSearchException.class, () -> {
            throw new DomainSearchException("Test", new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws DomainSearchException testException
     */
    @Test
    @Description(" DomainSearchException")
    @Severity(SeverityLevel.MINOR)
    void generateException2() {
        assertThrows(DomainSearchException.class, () -> {
            throw new DomainSearchException(new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws DomainSearchException testException
     */
    @Test
    @Description(" DomainSearchException")
    @Severity(SeverityLevel.MINOR)
    void generateException4() {
        assertThrows(DomainSearchException.class, () -> {
            throw new DomainSearchException("Test", new IllegalArgumentException("test"), false, true);
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws DomainSearchException testException
     */
    @Test
    @Description(" DomainSearchException")
    @Severity(SeverityLevel.MINOR)
    void generateException3() {
        assertThrows(DomainSearchException.class, () -> {
            throw new DomainSearchException();
        });
    }

}