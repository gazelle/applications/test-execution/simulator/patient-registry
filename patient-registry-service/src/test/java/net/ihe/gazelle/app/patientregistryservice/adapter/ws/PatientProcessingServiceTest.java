package net.ihe.gazelle.app.patientregistryservice.adapter.ws;

import com.gitb.ps.Void;
import com.gitb.ps.*;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import net.ihe.gazelle.app.patientregistryapi.adapter.ws.PatientProcessingWebserviceConstants;
import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PersonName;
import net.ihe.gazelle.app.patientregistryservice.application.TestPatientFeedServiceImpl;
import net.ihe.gazelle.app.patientregistryservice.application.TestPatientSearchServiceImpl;
import net.ihe.gazelle.lib.annotations.Covers;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperObjectToAnyContent;
import net.ihe.gazelle.lib.gitbutils.adapter.MappingException;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Tests for class {@link PatientProcessingService}
 */
@Feature("GITB Feed Webservice")
@Covers(requirements = {"SIMUSRV-001", "SIMUSRV-002"})
public class PatientProcessingServiceTest {

    private PatientProcessingService patientProcessingService;

    /**
     * Initialize implementations to be tested before each test.
     */
    @BeforeEach
    public void setUp() {
        this.patientProcessingService = new PatientProcessingService(new PatientFeedProcessingService(new TestPatientFeedServiceImpl()),
                new PatientSearchProcessingService(new TestPatientSearchServiceImpl()));
    }

    /**
     * Test Unsupported Operation getModuleDefinition
     */
    @Test
    @Description("Test the method getModuleDefinition")
    public void getModuleDefinition() {
        assertThrows(UnsupportedOperationException.class, () -> patientProcessingService.getModuleDefinition(new Void()));
    }

    /**
     * Test Unsupported Operation beginTransaction
     */
    @Test
    public void beginTransaction() {
        assertThrows(UnsupportedOperationException.class, () -> patientProcessingService.beginTransaction(new BeginTransactionRequest()));
    }

    /**
     * Test Unsupported Operation endTransaction
     */
    @Test
    public void endTransaction() {
        assertThrows(UnsupportedOperationException.class, () -> patientProcessingService.endTransaction(new BasicRequest()));
    }


    /**
     * Test Processing a patient operation, error case using null ProcessRequest
     */
    @Test
    public void process_no_ProcessingRequest() {
        assertThrows(IllegalArgumentException.class, () -> patientProcessingService.process(null));
    }

    /**
     * Test Processing a patient operation, error case using wrong operation in ProcessRequest.
     */
    @Test
    public void process_Wrong_Operation() {
        Patient patient = new Patient();
        PersonName personName = new PersonName();
        personName.setFamily("Bars");
        patient.addName(personName);

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation("TarteALaCremeCremeuse");
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent("MyPatient", patient));
        } catch (MappingException e) {
            fail("No exception expected when mapping test Patient", e);
        }
        assertThrows(UnsupportedOperationException.class, () -> patientProcessingService.process(processRequest));
    }

    /**
     * Test Processing a patient create.
     */
    @Test
    @Description("Test the process of Create Patient")
    public void process_Patient_Create() {
        Patient patient = new Patient();
        PersonName personName = new PersonName();
        personName.setFamily("Bars");
        patient.addName(personName);

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_CREATE_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_INPUT_NAME
                    , patient));
        } catch (MappingException e) {
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        ProcessResponse processResponse = patientProcessingService.process(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
    }

    /**
     * Test Processing a patient update.
     */
    @Test
    @Description("Test the process of Update Patient")
    public void process_Patient_Update() {
        Patient patient = new Patient();
        PersonName personName = new PersonName();
        personName.setFamily("Bars");
        patient.addName(personName);

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_UPDATE_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_INPUT_NAME,
                    patient));
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_INPUT_NAME,
                    new EntityIdentifier()));
        } catch (MappingException e) {
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        ProcessResponse processResponse = patientProcessingService.process(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
    }

    /**
     * Test Processing a patient Search.
     */
    @Test
    @Description("Test the process of Search Patient")
    public void process_Patient_Search() {

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_SEARCH_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_SEARCH_CRITERIA_INPUT_NAME
                    , new SearchCriteria()));
        } catch (MappingException e) {
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        ProcessResponse processResponse = patientProcessingService.process(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
    }

    @Test
    @Description("Test the process of Search Patient")
    public void process_Patient_Search_With_Parameters() {

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_PARAMETERS_SEARCH_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.PATIENT_PARAMETERS_SEARCH_INPUT_NAME
                    , new SearchParameter()));
        } catch (MappingException e) {
            throw new RuntimeException(e);
        }

        ProcessResponse processResponse = patientProcessingService.process(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
    }

    /**
     * Test Processing a patient Search.
     */
    @Test
    @Description("Test the process of Delete Patient")
    public void process_Patient_Delete() {

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_DELETE_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.UUID_PATIENT_TO_DELETE_INPUT_NAME
                    , "uuid"));
        } catch (MappingException e) {
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        ProcessResponse processResponse = patientProcessingService.process(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
    }

    /**
     * Test Processing a patient Merge.
     */
    @Test
    @Description("Test the process of Merge Patient")
    public void process_Patient_Merge() {

        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(PatientProcessingWebserviceConstants.PATIENT_MERGE_OPERATION);
        try {
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.UUID_OF_ORIGINAL_PATIENT_INPUT_NAME
                    , "uuid"));
            processRequest.getInput().add(new MapperObjectToAnyContent().getAnyContent(PatientProcessingWebserviceConstants.UUID_OF_DUPLICATED_PATIENT_INPUT_NAME
                    , "uuid"));
        } catch (MappingException e) {
            fail("No exception is supposed to be thrown when transforming test patient to AnyContent !", e);
        }

        ProcessResponse processResponse = patientProcessingService.process(processRequest);
        assertNotNull(processResponse, "ProcessResponse shall not be null !");
    }


}
