package net.ihe.gazelle.app.patientregistryservice.application;

import net.ihe.gazelle.app.patientregistryapi.application.PatientCrossReferenceSearch;
import net.ihe.gazelle.app.patientregistryapi.application.SearchCrossReferenceException;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PatientAliases;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;


 class PatientCrossReferenceSearchImplTest {
    private PatientCrossReferenceSearch patientCrossReferenceSearch = new PatientCrossReferenceSearchImpl(new PatientCrossReferenceDAOMock(),
            new DomainServiceMock());

    /**
     * Test to search with a null parameter
     */
    @Test
     void searchTestWithNullSourceIdentifier() {

        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class, () -> patientCrossReferenceSearch.search(null,
                null));
        assertEquals("The source identifier cannot be null", exception.getMessage());
    }

    @Test
     void searchTestWithNullSystemIdentifier() {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        List<String> targetDomains = new ArrayList<>();
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceSearch.search(sourceIdentifier, targetDomains));

        assertEquals("The system identifier from sourceIdentifier cannot be null or empty", exception.getMessage());
    }

    @Test
     void searchTestWithBlankSystemIdentifier() {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("");
        List<String> targetDomains = new ArrayList<>();
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceSearch.search(sourceIdentifier, targetDomains));

        assertEquals("The system identifier from sourceIdentifier cannot be null or empty", exception.getMessage());
    }

    @Test
     void searchTestWithNullValue() {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("toto");
        List<String> targetDomains = new ArrayList<>();
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceSearch.search(sourceIdentifier, targetDomains));

        assertEquals("The value from sourceIdentifier cannot be null or empty", exception.getMessage());
    }

    @Test
     void searchTestWithBlankValue() {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("toto");
        sourceIdentifier.setValue("");
        List<String> targetDomains = new ArrayList<>();
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceSearch.search(sourceIdentifier, targetDomains));
        assertEquals("The value from sourceIdentifier cannot be null or empty", exception.getMessage());
    }

    @Test
     void searchTestWithBlankValueTarget() {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("toto");
        sourceIdentifier.setValue("test");
        List<String> targetDomains = new ArrayList<>();
        targetDomains.add("");
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceSearch.search(sourceIdentifier, targetDomains));
        assertEquals("One of the target domain does not exist", exception.getMessage());
    }

    @Test
    void searchWithNoPixRef() throws SearchCrossReferenceException {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("Test");
        sourceIdentifier.setValue("NoCrossRef");
        List<String> targetDomains = new ArrayList<>();
        targetDomains.add("valid");
        PatientAliases patientAliases = patientCrossReferenceSearch.search(sourceIdentifier, targetDomains);
        assertEquals("It must contains only one patient", 1, patientAliases.getMembers().size());
    }
    @Test
    void searchWithNouuid() throws SearchCrossReferenceException {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("Test");
        sourceIdentifier.setValue("No uuid");
        List<String> targetDomains = new ArrayList<>();
        targetDomains.add("valid");
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceSearch.search(sourceIdentifier, targetDomains));
        assertEquals("An error occurred during CrossReference Search", exception.getMessage());
    }

    @Test
     void searchTestWithNonExistingSystem() {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("toto");
        sourceIdentifier.setValue("SystemNotFound");
        List<String> targetDomains = new ArrayList<>();
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceSearch.search(sourceIdentifier, targetDomains));

        assertEquals("Error in the sourceIdentifier : System does not exit", exception.getMessage());
    }

    @Test
     void searchTestWithUnexpectedException() {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("toto");
        sourceIdentifier.setValue("UnexpectedException");
        List<String> targetDomains = new ArrayList<>();
        targetDomains.add("valid");
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceSearch.search(sourceIdentifier, targetDomains));

        assertEquals("An error occurred during CrossReference Search", exception.getMessage());
    }

    @Test
     void searchTestNoPatientIdentifierFoundException() {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("toto");
        sourceIdentifier.setValue("PatientIdentifiernotfound");
        List<String> targetDomains = new ArrayList<>();
        targetDomains.add("valid");
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceSearch.search(sourceIdentifier, targetDomains));

        assertEquals("Error in the sourceIdentifier : it does not match any identity", exception.getMessage());
    }

    @Test
     void searchTestNoPatientFoundException() {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("toto");
        sourceIdentifier.setValue("PatientNotFound");
        List<String> targetDomains = new ArrayList<>();
        targetDomains.add("valid");
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceSearch.search(sourceIdentifier, targetDomains));

        assertEquals("Error in the sourceIdentifier : it does not match any Patient", exception.getMessage());
    }


    @Test
     void searchTestNoXRefFoundException() throws SearchCrossReferenceException {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("toto");
        sourceIdentifier.setValue("NoXCrossrefFound");
        List<String> targetDomains = new ArrayList<>();
        targetDomains.add("valid");
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceSearch.search(sourceIdentifier, targetDomains));
        assertEquals("Search error : No unique X-ref for given Patient", exception.getMessage());
    }

    @Test
     void searchTestXUniqueCrossRefFound() throws SearchCrossReferenceException {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("toto");
        sourceIdentifier.setValue("1CrossFound");
        List<String> targetDomains = new ArrayList<>();
        targetDomains.add("valid");
        PatientAliases patientAliases = patientCrossReferenceSearch.search(sourceIdentifier, targetDomains);
        for (Patient patient : patientAliases.getMembers()) {
            if (patient.getUuid().equals("original")) {
                fail();
            }
        }
        Assert.assertEquals("The X-ref shall contain the patients matching with the sourceIdentifier, no cross ref has been found ", 0,
                patientAliases.getMembers().size());

    }

    @Test
     void searchTestMultipleCrossRefFound() throws SearchCrossReferenceException {
        EntityIdentifier sourceIdentifier = new EntityIdentifier();
        sourceIdentifier.setSystemIdentifier("toto");
        sourceIdentifier.setValue("3CrossFound");
        List<String> targetDomains = new ArrayList<>();
        targetDomains.add("targetDomain");
        PatientAliases patientAliases = patientCrossReferenceSearch.search(sourceIdentifier, targetDomains);
        for (Patient patient : patientAliases.getMembers()) {
            if (patient.getUuid().equals("original3")) {
                fail();
            }
        }
        Assert.assertEquals("The X-ref shall contain the patients matching with the sourceIdentifier, no cross ref has been found ", 3,
                patientAliases.getMembers().size());
    }

    @Test
     void searchWithZeroTargetDomainMatching() throws SearchCrossReferenceException {
        EntityIdentifier sourceIdentifier1 = new EntityIdentifier();
        sourceIdentifier1.setSystemIdentifier("valid");
        sourceIdentifier1.setValue("searchTargetDomainFound");

        List<String> targetDomains = new ArrayList<>();
        targetDomains.add("valid");

        PatientAliases patientAliases = patientCrossReferenceSearch.search(sourceIdentifier1, targetDomains);
        Assert.assertEquals(0,patientAliases.getMembers().size() );
    }

    @Test
    void testWithUnkwownTargetDomain() throws SearchCrossReferenceException {
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("1111");
        entityIdentifier.setValue("searchTargetDomainFound");

        EntityIdentifier entityIdentifier2 = new EntityIdentifier();
        entityIdentifier2.setSystemIdentifier("2222");
        entityIdentifier2.setValue("2345");

        EntityIdentifier entityIdentifier3 = new EntityIdentifier();
        entityIdentifier3.setSystemIdentifier("3333");
        entityIdentifier3.setValue("3456");

        List<String> targetDomains = new ArrayList<>();
        targetDomains.add("2222");
        targetDomains.add("3333");
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class,
                () -> patientCrossReferenceSearch.search(entityIdentifier, targetDomains));
        assertEquals("One of the target domain does not exist", exception.getMessage());
    }

}