Feature: Feed Patient information

  Background:
    Given the following patients exist
      | tmpuuid  | gender | active | dateOfBirth                  | dateOfDeath                  | multipleBirthOrder |
      | tmpuuid0 | MALE   | true   | 1984-03-12T17:25:04.017+0100 | 2020-03-12T17:25:04.017+0100 | 134                |
    And patients have the following identifiers
      | tmpuuid  | systemIdentifier | value        | systemName | type |
      | tmpuuid0 | urn:oid:1.2.3.5  | identifier42 | system0    | PI   |
      | tmpuuid0 | urn:oid:5.6.7.9  | identifier42 | system2    | PI   |
    And patients have the following names
      | tmpuuid  | family | given1 | given2 | given3 |
      | tmpuuid0 | Bars   | Alain  | Hilary | Didier |
    And patients have the following addresses
      | tmpuuid  | line1                | city               | countryIso3 | postalCode | state | use   |
      | tmpuuid0 | 4 Rue Hélène-Boucher | Thorigné-Fouillard | FRA         | 35325      | test  | HOME  |
      | tmpuuid0 | 1 Rue des vignes     | Thorigné-Fouillard | FRA         | 35325      | etat  | LEGAL |
    And patients have the following contact points
      | tmpuuid  | use   | value           |
      | tmpuuid0 | HOME  | 0167200001      |
      | tmpuuid0 | OTHER | ftg@kereval.com |
    And patients have the following observations
      | tmpuuid  | type        | value |
      | tmpuuid0 | BLOOD_GROUP | O+    |
      | tmpuuid0 | WEIGHT      | 62    |

  Scenario:
    When patient is fed with provisional uuid "tmpuuid0"
    Then fed patient has returned new assigned uuid
    And fed patient is equals to existing patient with provisional uuid "tmpuuid0"