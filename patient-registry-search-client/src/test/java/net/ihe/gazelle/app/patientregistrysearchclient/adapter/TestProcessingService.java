package net.ihe.gazelle.app.patientregistrysearchclient.adapter;

import com.gitb.core.AnyContent;
import com.gitb.ps.Void;
import com.gitb.ps.*;
import com.gitb.tr.ObjectFactory;
import com.gitb.tr.*;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PersonName;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperObjectToAnyContent;
import net.ihe.gazelle.lib.gitbutils.adapter.MappingException;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static net.ihe.gazelle.app.patientregistryapi.adapter.ws.PatientProcessingWebserviceConstants.PATIENT_INPUT_NAME;
import static net.ihe.gazelle.app.patientregistryapi.adapter.ws.PatientProcessingWebserviceConstants.PATIENT_LIST_OUTPUT_NAME;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Test implementation of processing service used to mock responses received by PatientFeedClient
 */
public class TestProcessingService implements ProcessingService {

    private static final String UUID = "TEST-uuid1";
    private ResponseTypes responseToReturn;

    /**
     * Default constructor for the class. By default, the service returns a valid success answer.
     */
    public TestProcessingService(){
        responseToReturn = ResponseTypes.CORRECT;
    }

    /**
     * Setter to the responseToReturn property
     * @param responseToReturn the response to return
     */
    public void setResponseToReturn(ResponseTypes responseToReturn){
        this.responseToReturn = responseToReturn;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GetModuleDefinitionResponse getModuleDefinition(Void aVoid) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProcessResponse process(ProcessRequest processRequest) {
        switch(responseToReturn){
            case CORRECT:
                return getCorrectProcessResponse();
            case MISSING_REPORT:
                return getResponseMissingReport();
            case FAILED_REPORT:
                return getResponseWithFailedReport("ErrorDescription");
            case MULTIPLE_OUTPUT:
                return getResponseWithMultipleOutputs();
            case INVALID_OUTPUT:
                return getResponseWithInvalidOutput();
            case NULL_RESPONSE:
                return null;
            case UNEXPECTED_STATUS:
                return getResponseWithUnexpectedReportStatus();
            case NO_REPORTS_IN_REPORT:
                return getResponseWithoutReports();
            case NO_ERROR_IN_FAILURE_REPORT:
                return getResponseWithoutErrorInFailureReport();
            case NO_ERROR_DESCRIPTION:
                return getResponseWithFailedReport(null);
            case ILLEGAL_ARGUMENT_EXCEPTION:
                throw new IllegalArgumentException();
            case UNSUPPORTED_OPERATION_EXCEPTION:
                throw new UnsupportedOperationException();
            default:
                return null;
        }
    }

    /**
     * Return a correct {@link ProcessResponse} to a feed Process Request
     * @return a correct {@link ProcessResponse}
     */
    private ProcessResponse getCorrectProcessResponse(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.SUCCESS);
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(PATIENT_INPUT_NAME, getPatients()));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Return a {@link ProcessResponse} with no Report
     * @return a {@link ProcessResponse} with no report
     */
    private ProcessResponse getResponseMissingReport(){
        ProcessResponse processResponse = new ProcessResponse();
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(PATIENT_INPUT_NAME, getPatients()));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Return a valid {@link ProcessResponse} for a failed feed request.
     * @param errorDescription   Value of the Error's description for the report
     * @return the failed {@link ProcessResponse}
     */
    private ProcessResponse getResponseWithFailedReport(String errorDescription){
        com.gitb.tr.ObjectFactory objectFactory = new ObjectFactory();
        ProcessResponse processResponse = new ProcessResponse();

        TAR report = new TAR();
        report.setResult(TestResultType.FAILURE);
        report.setReports(new TestAssertionGroupReportsType());

        BAR itemContent = new BAR();
        itemContent.setDescription(errorDescription);
        report.getReports().getInfoOrWarningOrError().add(objectFactory.createTestAssertionGroupReportsTypeError(itemContent));

        report.setCounters(new ValidationCounters());
        report.getCounters().setNrOfAssertions(BigInteger.valueOf(0));
        report.getCounters().setNrOfWarnings(BigInteger.valueOf(0));
        report.getCounters().setNrOfErrors(BigInteger.valueOf(1));

        report.setContext(new AnyContent());
        report.getContext().getItem().add(new AnyContent());
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(PATIENT_INPUT_NAME, getPatients()));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} with multiple outputs
     * @return a {@link ProcessResponse} with multiple outputs
     */
    private ProcessResponse getResponseWithMultipleOutputs(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.SUCCESS);
        processResponse.setReport(report);
        try {
            AnyContent anyContent = new MapperObjectToAnyContent().getAnyContent(PATIENT_INPUT_NAME, getPatients());
            processResponse.getOutput().add(anyContent);
            processResponse.getOutput().add(anyContent);
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} with an output not valid for a Patient Feed response.
     * @return a {@link ProcessResponse} with invalid output
     */
    private ProcessResponse getResponseWithInvalidOutput(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.SUCCESS);
        processResponse.setReport(report);
        PersonName personName = new PersonName();
        personName.setFamily("Bars");
        try {
            AnyContent anyContent = new MapperObjectToAnyContent().getAnyContent(PATIENT_INPUT_NAME, personName);
            processResponse.getOutput().add(anyContent);
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} with unexpected Report Status, id not SUCCESS or FAILURE.
     * @return a {@link ProcessResponse} with unexpected Report Status
     */
    private ProcessResponse getResponseWithUnexpectedReportStatus(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.WARNING);
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(PATIENT_INPUT_NAME, getPatients()));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} without Error reports in the TAR report.
     * @return a {@link ProcessResponse} without Error Reports
     */
    private ProcessResponse getResponseWithoutReports(){
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.FAILURE);
        report.setContext(new AnyContent());
        report.getContext().getItem().add(new AnyContent());
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(PATIENT_INPUT_NAME, getPatients()));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Returns a {@link ProcessResponse} without any Error thought with status FAILURE.
     * @return ProcessResponse
     */
    private ProcessResponse getResponseWithoutErrorInFailureReport(){
        ProcessResponse processResponse = new ProcessResponse();

        TAR report = new TAR();
        report.setResult(TestResultType.FAILURE);
        report.setReports(new TestAssertionGroupReportsType());

        report.setCounters(new ValidationCounters());
        report.getCounters().setNrOfAssertions(BigInteger.valueOf(0));
        report.getCounters().setNrOfWarnings(BigInteger.valueOf(0));
        report.getCounters().setNrOfErrors(BigInteger.valueOf(0));

        report.setContext(new AnyContent());
        report.getContext().getItem().add(new AnyContent());
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(PATIENT_LIST_OUTPUT_NAME, getPatients()));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BeginTransactionResponse beginTransaction(BeginTransactionRequest beginTransactionRequest) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Void endTransaction(BasicRequest basicRequest) {
        return null;
    }

    public static List<Patient> getPatients(){
        List<Patient> patients = new ArrayList<>();
        Patient patient = new Patient();
        patient.setUuid(UUID);
        patients.add(patient);
        return patients;
    }

    /**
     * Enumeration of all types of response this fake service can return
     */
    public enum  ResponseTypes{
        CORRECT,
        MISSING_REPORT,
        FAILED_REPORT,
        MULTIPLE_OUTPUT,
        INVALID_OUTPUT,
        NULL_RESPONSE,
        UNEXPECTED_STATUS,
        NO_REPORTS_IN_REPORT,
        NO_ERROR_IN_FAILURE_REPORT,
        NO_ERROR_DESCRIPTION,
        ILLEGAL_ARGUMENT_EXCEPTION,
        UNSUPPORTED_OPERATION_EXCEPTION;
    }
}
