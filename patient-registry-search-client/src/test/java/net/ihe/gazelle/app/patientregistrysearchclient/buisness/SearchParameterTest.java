package net.ihe.gazelle.app.patientregistrysearchclient.buisness;

import net.ihe.gazelle.app.patientregistryapi.application.SearchParameter;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SearchParameterTest {

    @Test
     void testSearchParameter() {
        SearchParameter searchParameter = new SearchParameter();
        searchParameter.setKey("key");
        searchParameter.setValue("value");
        searchParameter.setModifier("modifier");
        searchParameter.setClassType("classType");

        assertEquals("key", searchParameter.getKey());
        assertEquals("value", searchParameter.getValue());
        assertEquals("modifier", searchParameter.getModifier());
        assertEquals("classType", searchParameter.getClassType());
    }
}
