package net.ihe.gazelle.app.patientregistryapi.application;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertThrows;

class SearchCrossReferenceExceptionTest {
    /**
     * Dummy Test for coverage
     *
     * @throws SearchCrossReferenceException testException
     */
    @Test
    void generateException() {
        assertThrows(SearchCrossReferenceException.class, () -> {
            throw new SearchCrossReferenceException("Test");
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws SearchCrossReferenceException testException
     */
    @Test
    void generateException1() {
        assertThrows(SearchCrossReferenceException.class, () -> {
            throw new SearchCrossReferenceException("Test", new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws SearchCrossReferenceException testException
     */
    @Test
    void generateException2() {
        assertThrows(SearchCrossReferenceException.class, () -> {
            throw new SearchCrossReferenceException(new IllegalArgumentException("test"));
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws SearchCrossReferenceException testException
     */
    @Test
    void generateException4() {
        assertThrows(SearchCrossReferenceException.class, () -> {
            throw new SearchCrossReferenceException("Test", new IllegalArgumentException("test"), false, true);
        });
    }

    /**
     * Dummy Test for coverage
     *
     * @throws SearchCrossReferenceException testException
     */
    @Test
    void generateException3() {
        assertThrows(SearchCrossReferenceException.class, () -> {
            throw new SearchCrossReferenceException();
        });
    }

}