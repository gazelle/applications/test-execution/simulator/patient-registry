package net.ihe.gazelle.app.patientregistryapi.business;

import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

/**
 * tests for Patient class
 */
class PatientAliasesTest {

    /**
     * test default constructor
     */
    @Test
    void patientAliasesDefaultConstructorTest() {
        PatientAliases patientAliases = new PatientAliases();

        assertTrue(patientAliases.getMembers().isEmpty());
    }

    /**
     * test complete constructor
     */
    @Test
    void patientAliasesCompleteConstructorTest() {
        String uuid = "3b19b902-cfe8-11ea-87d0-0242ac130003";
        Date lastModifierDateTime = new Date();
        String lastModifier = "toto";

        PatientAliases patientAliases = new PatientAliases(uuid, lastModifier, lastModifierDateTime);

        assertTrue(patientAliases.getMembers().isEmpty());
        assertEquals(uuid, patientAliases.getUuid());
        assertEquals(lastModifier, patientAliases.getLastModifier());
        assertEquals(lastModifierDateTime, patientAliases.getLastModifierDateTime());
    }


    /**
     * test member list add patient
     */
    @Test
    void addMembersTest() {
        PatientAliases patientAliases = new PatientAliases();
        patientAliases.addMember(new Patient());

        assertFalse(patientAliases.getMembers().isEmpty());
    }


    /**
     * test members list remove member
     */
    @Test
    void removeMemberToIndexTest() {
        PatientAliases patientAliases = new PatientAliases();
        for (int i = 0; i < 5; i++) {
            patientAliases.addMember(new Patient());
        }
        int index = 3;
        Patient patientToAddAndRemove = new Patient();
        patientToAddAndRemove.setUuid("75a59e02-cdb2-11ea-87d0-0242ac130003");
        patientAliases.addMember(patientToAddAndRemove, index);
        patientAliases.removeMember(patientToAddAndRemove);

        assertFalse(patientAliases.getMembers().contains(patientToAddAndRemove));
    }


    /**
     * test Members list add member
     */
    @Test
    void addMemberTest() {
        PatientAliases patientAliases = new PatientAliases();
        patientAliases.addMember(new Patient());

        assertFalse(patientAliases.getMembers().isEmpty());
    }

    /**
     * test member list add patient to index
     */
    @Test
    void addMemberToIndexTest() {
        PatientAliases patientAliases = new PatientAliases();
        for (int i = 0; i < 5; i++) {
            patientAliases.addMember(new Patient());
        }
        int index = 3;
        Patient patient = new Patient();
        patientAliases.addMember(patient, index);

        assertEquals(patient, patientAliases.getMembers().get(index));
    }

    /**
     * test member list remove patient
     */
    @Test
    void removePatientToIndexTest() {
        PatientAliases patientAliases = new PatientAliases();
        for (int i = 0; i < 5; i++) {
            patientAliases.addMember(new Patient());
        }
        int index = 3;
        Patient patientUUIDToAddAndRemove = new Patient();
        patientUUIDToAddAndRemove.setUuid("44380a84-cfe8-11ea-87d0-0242ac130003");

        patientAliases.addMember(patientUUIDToAddAndRemove, index);
        patientAliases.removeMember(patientUUIDToAddAndRemove);

        assertFalse(patientAliases.getMembers().contains(patientUUIDToAddAndRemove));
    }


    /**
     * Test check date time last modifier
     */
 /* This Test is working 1/2 times

 @Test
    void checkPatientAliasesDateTime() {
        PatientAliases patientAliases = new PatientAliases();
        Patient patient = new Patient();
        patientAliases.addMember(patient);

        String date1 = patientAliases.getLastModifierDateTime().toString();
        patientAliases.addMember(patient);
        String lastDateTime1 = patientAliases.getLastModifierDateTime().toString();
        assertFalse(lastDateTime1.contains(date1));

    }*/


    /**
     * Test check last modifier
     */
    @Test
    void checkPatientAliasesLastModifier() {
        PatientAliases patientAliases = new PatientAliases();
        Patient patient = new Patient();
        patient.setUuid("4e7fc004-cfe8-11ea-87d0-0242ac130003");
        patientAliases.addMember(patient);
        patientAliases.setLastModifier("toto");
        assertTrue(patientAliases.getLastModifier().contains("toto"));
    }


    /**
     * Test patientAliases on member and Null
     */
    @Test
    void equals_patientAliases_null() {
        PatientAliases patientAliases = new PatientAliases();
        assertNotNull(patientAliases, "PatientAliases shall not be equal to null!");
    }


    /**
     * Test equals on same patientAliases
     */
    @Test
    void equals_patientAliases_same() {
        PatientAliases patientAliases = new PatientAliases();
        assertEquals(patientAliases, patientAliases, "PatientAliases shall be equal!");
    }

    /**
     * Test equals on Patient and another class
     */
    @Test
    void equals_patientAliases_other_class() {
        PatientAliases patientAliases = new PatientAliases();
        assertNotEquals("TEST", patientAliases, "PatientAliases shall not be equal to string !");
    }


}