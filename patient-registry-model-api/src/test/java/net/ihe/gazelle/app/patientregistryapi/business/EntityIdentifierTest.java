package net.ihe.gazelle.app.patientregistryapi.business;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * test EntityIdentifier class
 */
class EntityIdentifierTest {

    /**
     * test default constructor
     */
    @Test
    public void entityIdentifierDefaultConstructorTest() {
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        assertNotNull(entityIdentifier);
    }

    /**
     * test complete constructor
     */
    @Test
    public void entityIdentifierCompleteConstructorTest() {
        String systemIdentifier = "systemIdentifier";
        String systemName = "systemName";
        String type = "type";
        String value = "value";

        EntityIdentifier entityIdentifier = new EntityIdentifier(systemIdentifier, systemName, type, value);

        assertEquals(systemIdentifier, entityIdentifier.getSystemIdentifier());
        assertEquals(systemName, entityIdentifier.getSystemName());
        assertEquals(type, entityIdentifier.getType());
        assertEquals(value, entityIdentifier.getValue());
    }

    /**
     * Test equals on Identifiers and null
     */
    @Test
    public void equals_EntityIdentifier_null(){
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        assertFalse(entityIdentifier.equals(null),"EntityIdentifier shall not be equal to null!");
    }

    /**
     * Test equals on same Identifiers
     */
    @Test
    public void equals_EntityIdentifier_same(){
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        assertTrue(entityIdentifier.equals(entityIdentifier),"EntityIdentifieres shall be equal!");
    }

    /**
     * Test equals on Identifiers and another class
     */
    @Test
    public void equals_EntityIdentifier_other_class(){
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        assertFalse(entityIdentifier.equals(43),"EntityIdentifier shall not be equal to city string !");
    }

    /**
     * Test equals on Identifiers value equals
     */
    @Test
    public void equals_EntityIdentifier_value_true(){
        String value= "1.2.3.4";
        EntityIdentifier contactPoint = new EntityIdentifier();
        contactPoint.setValue(value);
        EntityIdentifier contactPoint1 = new EntityIdentifier();
        contactPoint1.setValue(value);
        assertTrue(contactPoint.equals(contactPoint1),"EntityIdentifier shall be equal !");
    }

    /**
     * Test equals on Identifiers value not equals
     */
    @Test
    public void equals_EntityIdentifier_value_false(){
        String value= "1.2.3.4";
        String value1= "5.6.7.8";
        EntityIdentifier contactPoint = new EntityIdentifier();
        contactPoint.setValue(value);
        EntityIdentifier contactPoint1 = new EntityIdentifier();
        contactPoint1.setValue(value1);
        assertFalse(contactPoint.equals(contactPoint1),"EntityIdentifier shall not be equal !");
    }

    /**
     * Test equals on Identifiers type equals
     */
    @Test
    public void equals_EntityIdentifier_type_true(){
        String type= "1.2.3.4";
        EntityIdentifier contactPoint = new EntityIdentifier();
        contactPoint.setType(type);
        EntityIdentifier contactPoint1 = new EntityIdentifier();
        contactPoint1.setType(type);
        assertTrue(contactPoint.equals(contactPoint1),"EntityIdentifier shall be equal !");
    }

    /**
     * Test equals on Identifiers type not equals
     */
    @Test
    public void equals_EntityIdentifier_type_false(){
        String type= "1.2.3.4";
        String type1= "5.6.7.8";
        EntityIdentifier contactPoint = new EntityIdentifier();
        contactPoint.setType(type);
        EntityIdentifier contactPoint1 = new EntityIdentifier();
        contactPoint1.setType(type1);
        assertFalse(contactPoint.equals(contactPoint1),"EntityIdentifier shall not be equal !");
    }

    /**
     * Test equals on Identifiers systemName equals
     */
    @Test
    public void equals_EntityIdentifier_systemName_true(){
        String systemName= "1.2.3.4";
        EntityIdentifier contactPoint = new EntityIdentifier();
        contactPoint.setSystemName(systemName);
        EntityIdentifier contactPoint1 = new EntityIdentifier();
        contactPoint1.setSystemName(systemName);
        assertTrue(contactPoint.equals(contactPoint1),"EntityIdentifier shall be equal !");
    }

    /**
     * Test equals on Identifiers systemName not equals
     */
    @Test
    public void equals_EntityIdentifier_systemName_false(){
        String systemName= "1.2.3.4";
        String systemName1= "5.6.7.8";
        EntityIdentifier contactPoint = new EntityIdentifier();
        contactPoint.setSystemName(systemName);
        EntityIdentifier contactPoint1 = new EntityIdentifier();
        contactPoint1.setSystemName(systemName1);
        assertFalse(contactPoint.equals(contactPoint1),"EntityIdentifier shall not be equal !");
    }

    /**
     * Test equals on Identifiers systemIdentifier equals
     */
    @Test
    public void equals_EntityIdentifier_systemIdentifier_true(){
        String systemIdentifier= "1.2.3.4";
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier(systemIdentifier);
        EntityIdentifier entityIdentifier1 = new EntityIdentifier();
        entityIdentifier1.setSystemIdentifier(systemIdentifier);
        assertTrue(entityIdentifier.equals(entityIdentifier1),"EntityIdentifier shall be equal !");
    }

    /**
     * Test equals on Identifiers systemIdentifier not equals
     */
    @Test
    public void equals_EntityIdentifier_systemIdentifier_false(){
        String systemIdentifier= "1.2.3.4";
        String systemIdentifier1= "5.6.7.8";
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier(systemIdentifier);
        EntityIdentifier entityIdentifier1 = new EntityIdentifier();
        entityIdentifier1.setSystemIdentifier(systemIdentifier1);
        assertFalse(entityIdentifier.equals(entityIdentifier1),"EntityIdentifier shall not be equal !");
    }

}