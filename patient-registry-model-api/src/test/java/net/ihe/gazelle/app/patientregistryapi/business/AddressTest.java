package net.ihe.gazelle.app.patientregistryapi.business;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * test Adress class
 */
class AddressTest {

    /**
     * test default constructor
     */
    @Test
    public void addressDefaultConstructorTest() {
        Address address = new Address();
        assertTrue(address.getLines().isEmpty());
    }

    /**
     * test complete constructor
     */
    @Test
    public void addressCompleteConstructorTest() {
        String city = "city";
        String countryIso3 = "countryIso3";
        String postalCode = "postalCode";
        String state = "state";
        AddressUse use = AddressUse.BAD;

        Address address = new Address(city, countryIso3, postalCode, state, use);

        assertTrue(address.getLines().isEmpty());
        assertEquals(city, address.getCity());
        assertEquals(countryIso3, address.getCountryIso3());
        assertEquals(postalCode, address.getPostalCode());
        assertEquals(state, address.getState());
        assertEquals(use, address.getUse());
        assertEquals(use.value(), address.getUse().value());
    }

    /**
     * test add line to list
     */
    @Test
    public void addressListsTest() {
        String line = "line";

        Address address = new Address();

        address.addLine(line);

        assertTrue(address.getLines().contains(line));
    }

    /**
     * test Line list add line name to index
     */
    @Test
    public void addLineToIndexTest() {
        Address address = new Address();
        for(int i=0; i<5; i++) {
            address.addLine("");
        }
        int index = 3;
        String lineToAdd = "lineToAdd";
        address.addLine(lineToAdd,index);

        assertEquals(lineToAdd,address.getLines().get(index));
    }

    /**
     * test address remove line
     */
    @Test
    public void removeLineTest() {
        Address address = new Address();
        for(int i=0; i<5; i++) {
            address.addLine("");
        }
        int index = 3;
        String lineToAddAndRemove = "lineToAddAndRemove";
        address.addLine(lineToAddAndRemove,index);
        address.removeLine(lineToAddAndRemove);

        assertFalse(address.getLines().contains(lineToAddAndRemove));
    }

    /**
     * Test equals on address and null
     */
    @Test
    public void equals_Address_null(){
        String city= "Rennes";
        Address address = new Address();
        address.setCity(city);
        assertFalse(address.equals(null),"Address shall not be equal to null!");
    }

    /**
     * Test equals on same address
     */
    @Test
    public void equals_Address_same(){
        String city= "Rennes";
        Address address = new Address();
        address.setCity(city);
        assertTrue(address.equals(address),"Addresses shall be equal!");
    }

    /**
     * Test equals on address and another class
     */
    @Test
    public void equals_Address_other_class(){
        String city= "Rennes";
        Address address = new Address();
        address.setCity(city);
        assertFalse(address.equals(city),"Address shall not be equal to city string !");
    }

    /**
     * Test equals on address city equals
     */
    @Test
    public void equals_Address_City_true(){
        String city= "Rennes";
        Address address = new Address();
        address.setCity(city);
        Address address1 = new Address();
        address1.setCity(city);
        assertTrue(address.equals(address1),"Addresses shall be equal !");
    }

    /**
     * Test equals on address city not equals
     */
    @Test
    public void equals_Address_City_false(){
        String city= "Rennes";
        String city1= "Tourcoing";
        Address address = new Address();
        address.setCity(city);
        Address address1 = new Address();
        address1.setCity(city1);
        assertFalse(address.equals(address1),"Addresses shall not be equal !");
    }

    /**
     * Test equals on address countryIso3 equals
     */
    @Test
    public void equals_Address_countryIso3_true(){
        String countryIso3= "FRA";
        Address address = new Address();
        address.setCountryIso3(countryIso3);
        Address address1 = new Address();
        address1.setCountryIso3(countryIso3);
        assertTrue(address.equals(address1),"Addresses shall be equal !");
    }

    /**
     * Test equals on address countryIso3 not equals
     */
    @Test
    public void equals_Address_countryIso3_false(){
        String countryIso3= "FRA";
        String countryIso31= "GBR";
        Address address = new Address();
        address.setCountryIso3(countryIso3);
        Address address1 = new Address();
        address1.setCountryIso3(countryIso31);
        assertFalse(address.equals(address1),"Addresses shall not be equal !");
    }

    /**
     * Test equals on address postalCode equals
     */
    @Test
    public void equals_Address_postalCode_true(){
        String postalCode= "FRA";
        Address address = new Address();
        address.setPostalCode(postalCode);
        Address address1 = new Address();
        address1.setPostalCode(postalCode);
        assertTrue(address.equals(address1),"Addresses shall be equal !");
    }

    /**
     * Test equals on address countryIso3 not equals
     */
    @Test
    public void equals_Address_postalCode_false(){
        String postalCode= "FRA";
        String postalCode1= "GBR";
        Address address = new Address();
        address.setPostalCode(postalCode);
        Address address1 = new Address();
        address1.setPostalCode(postalCode1);
        assertFalse(address.equals(address1),"Addresses shall not be equal !");
    }

    /**
     * Test equals on address state equals
     */
    @Test
    public void equals_Address_state_true(){
        String state= "FRA";
        Address address = new Address();
        address.setState(state);
        Address address1 = new Address();
        address1.setState(state);
        assertTrue(address.equals(address1),"Addresses shall be equal !");
    }

    /**
     * Test equals on address state not equals
     */
    @Test
    public void equals_Address_state_false(){
        String state= "FRA";
        String state1= "GBR";
        Address address = new Address();
        address.setState(state);
        Address address1 = new Address();
        address1.setState(state1);
        assertFalse(address.equals(address1),"Addresses shall not be equal !");
    }

    /**
     * Test equals on address use equals
     */
    @Test
    public void equals_Address_use_true(){
        AddressUse use = AddressUse.HOME;
        Address address = new Address();
        address.setUse(use);
        Address address1 = new Address();
        address1.setUse(use);
        assertTrue(address.equals(address1),"Addresses shall be equal !");
    }

    /**
     * Test equals on address use not equals
     */
    @Test
    public void equals_Address_use_false(){
        AddressUse use = AddressUse.HOME;
        AddressUse use1= AddressUse.BIRTH_PLACE;
        Address address = new Address();
        address.setUse(use);
        Address address1 = new Address();
        address1.setUse(use1);
        assertFalse(address.equals(address1),"Addresses shall not be equal !");
    }

    /**
     * Test equals on address lines equals
     */
    @Test
    public void equals_Address_lines_true(){
        String line= "10 Rue Helene Boucher";
        Address address = new Address();
        address.addLine(line);
        Address address1 = new Address();
        address1.addLine(line);
        assertTrue(address.equals(address1),"Addresses shall be equal !");
    }

    /**
     * Test equals on address lines not equals
     */
    @Test
    public void equals_Address_lines_false(){
        String line= "10 Rue Helene Boucher";
        String line1= "20 Rue de Paris";
        Address address = new Address();
        address.addLine(line);
        Address address1 = new Address();
        address1.addLine(line1);
        assertFalse(address.equals(address1),"Addresses shall not be equal !");
    }

    /**
     * Test equals on address lines of different size
     */
    @Test
    public void equals_Address_lines_different_size(){
        String line= "FRA";
        Address address = new Address();
        address.addLine(line);
        Address address1 = new Address();
        assertFalse(address.equals(address1),"Addresses shall not be equal !");
    }

    /**
     * Test equals on address lines in different order
     */
    @Test
    public void equals_Address_lines_different_order(){
        String line= "FRA";
        String line1= "20 Rue de Paris";
        Address address = new Address();
        address.addLine(line);
        address.addLine(line1);
        Address address1 = new Address();
        address1.addLine(line1);
        address1.addLine(line);
        assertFalse(address.equals(address1),"Addresses shall not be equal !");
    }

}