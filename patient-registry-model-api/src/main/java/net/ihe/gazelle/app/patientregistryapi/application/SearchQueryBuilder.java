package net.ihe.gazelle.app.patientregistryapi.application;

import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;

import javax.persistence.TypedQuery;
import java.util.List;

public interface SearchQueryBuilder<T> {

    TypedQuery<T> build(List<SearchParameter> searchParameters) throws SearchException;
}
