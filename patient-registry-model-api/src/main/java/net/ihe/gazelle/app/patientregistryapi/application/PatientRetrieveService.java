package net.ihe.gazelle.app.patientregistryapi.application;

import net.ihe.gazelle.app.patientregistryapi.business.Patient;

/**
 * Patient Retrieve Service API. Used to retrieve a patient by its UUID.
 */
public interface PatientRetrieveService {

    /**
     * Retrieve a {@link Patient} by its UUID.
     * @param uuid    Literal value of the UUID of the {@link Patient} to find
     * @return the {@link Patient} that correspond to the requested UUID.
     */
    Patient retrievePatient(String uuid) throws PatientRetrieveException ;
}
