
package net.ihe.gazelle.app.patientregistryapi.business;


/**
 * Diverse observations type to add to the patient
 */
public enum ObservationType {

    WEIGHT,
    HEIGHT,
    RELIGION,
    RACE,
    MOTHERS_MAINDEN_NAME,
    ACCOUNT_NUMBER,
    BLOOD_GROUP,
    BIRTHPLACE_NAME,
    SOCIO_PROFESSIONAL_CATEGORY,
    WEEKS_OF_GESTATION,
    IS_PREGNANT,
    LUNAR_DATE_YEAR,
    LUNAR_DATE_MONTH,
    SMS_CONSENT,
    SOCIO_PROFESSIONAL_OCCUPATION,
    LUNAR_DATE_WEEK,
    VIP_INDICATOR,
    MARITAL_STATUS,
    IDENTITY_RELIABILITY_CODE;

    /**
     * get enum member name
     * @return enum member name
     */
    public String value() {
        return name();
    }

}
