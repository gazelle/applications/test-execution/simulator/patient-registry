package net.ihe.gazelle.app.patientregistryapi.business;

import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.key.SearchCriterionKey;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.operator.SearchCriterionOperator;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.operator.StringSearchCriterionOperator;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.option.SearchCriterionOptions;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.option.StringSearchCriterionOptions;

/**
 * Patient Search Criterion Key to be used to perfom searches on Patient Demographics.
 */
public enum PatientSearchCriterionKey implements SearchCriterionKey {

    UUID (String.class, StringSearchCriterionOperator.EXACT, new StringSearchCriterionOptions(true)),
    IDENTIFIER (String.class, StringSearchCriterionOperator.EXACT, new StringSearchCriterionOptions(true)),
    DOMAIN (String.class, StringSearchCriterionOperator.EXACT, new StringSearchCriterionOptions(true));

    private SearchCriterionOptions defaultOptions;
    private SearchCriterionOperator defaultOperator;
    private Class valueClass;

    /**
     * Default constructor for the class.
     * @param clazz         class of the criterion's value
     * @param operator      operator to use by search engine to match this criterion
     * @param options
     */
    PatientSearchCriterionKey(Class clazz, SearchCriterionOperator operator, SearchCriterionOptions options){
        this.valueClass = clazz;
        this.defaultOperator = operator;
        this.defaultOptions = options;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchCriterionOptions getDefaultOptions() {
        return this.defaultOptions;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchCriterionOperator getDefaultOperator() {
        return this.defaultOperator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class getValueClass() {
        return valueClass;
    }
}
