
package net.ihe.gazelle.app.patientregistryapi.business;


/**
 * Possible contact points use for the patient
 */
public enum ContactPointUse {

    HOME,
    PRIMARY_HOME,
    WORK,
    TEMPORARY,
    EMERGENCY,
    MOBILE,
    BEEPER,
    EMAIL,
    OTHER,
    ANSWERING_SERVICE,
    DIRECT,
    PUBLIC,
    BAD;

    /**
     * get enum member name
     * @return enum member name
     */
    public String value() {
        return name();
    }


}
