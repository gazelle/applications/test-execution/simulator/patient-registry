
package net.ihe.gazelle.app.patientregistryapi.business;


/**
 * Patient Identifier model
 */
public class EntityIdentifier {

    protected String systemIdentifier;
    protected String systemName;
    protected String type;
    protected String value;

    /**
     * default constructor
     */
    public EntityIdentifier() {
    }

    /**
     * complete constructor
     * @param systemIdentifier identifier system id
     * @param systemName identifier system name
     * @param type identifier type
     * @param value identifier value
     */
    public EntityIdentifier(String systemIdentifier, String systemName, String type, String value) {
        this.setSystemIdentifier(systemIdentifier);
        this.setSystemName(systemName);
        this.setType(type);
        this.setValue(value);
    }

    /**
     * get the system identifier associated with the identifier
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemIdentifier() {
        return systemIdentifier;
    }

    /**
     * set the system identifier associated with the identifier
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemIdentifier(String value) {
        this.systemIdentifier = value;
    }

    /**
     * get the system name associated with the identifier
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSystemName() {
        return systemName;
    }

    /**
     * set the system name associated with the identifier
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSystemName(String value) {
        this.systemName = value;
    }

    /**
     * get the identifier type
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * set the identifier type
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

    /**
     * get the identifier value
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * set the identifier value
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EntityIdentifier that = (EntityIdentifier) o;

        if (systemIdentifier != null ? !systemIdentifier.equals(that.systemIdentifier) : that.systemIdentifier != null) {
            return false;
        }
        if (systemName != null ? !systemName.equals(that.systemName) : that.systemName != null) {
            return false;
        }
        if (type != null ? !type.equals(that.type) : that.type != null) {
            return false;
        }
        return value != null ? value.equals(that.value) : that.value == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = systemIdentifier != null ? systemIdentifier.hashCode() : 0;
        result = 31 * result + (systemName != null ? systemName.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
