package net.ihe.gazelle.app.patientregistryapi.application;

import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;

/**
 * Interface for Patient Feed services.
 */
public interface PatientFeedService {

    /**
     * Create a single patient to the service.
     * @param patient Patient to create to the service
     * @return the literal value of the UUID assigned by the service to the Patient.
     * @throws PatientFeedException if the service is not able to correctly feed the patient.
     */
    String createPatient(Patient patient) throws PatientFeedException;

    /**
     * Update a single Patient in patient Registry
     * @param patient Patient to update to the service
     * @param identifier Patient identifier to update
     * @return the updated Patient by the service
     * @throws PatientFeedException if the service is not able to correctly update the patient
     */
    Patient updatePatient(Patient patient, EntityIdentifier identifier) throws PatientFeedException;

    /**
     *
     * @param uuidOfOriginalPatient Patient uuid of the original (keep)
     * @param uuidOfDuplicatedPatient Patient uuid of the duplicated (will be deactivated)
     * @return The merged patient by the service
     * @throws PatientFeedException if the service is not able to correctly merge the patients
     */
    String mergePatient(String uuidOfOriginalPatient, String uuidOfDuplicatedPatient) throws PatientFeedException;

    /**
     *
     * @param uuid of the patient
     * @return true if the patient has been delete or false if the patient does not exist any more
     * @throws PatientFeedException if the service is not able to correctly delete the patient
     */
    boolean deletePatient(String uuid) throws PatientFeedException;

    /**
     * delete all patients with the given identifier
     * @param identifier Patient identifier to delete
     * @return delete identifier
     * @throws PatientFeedException if the service is not able to correctly delete the patient
     */
    EntityIdentifier deletePatient(EntityIdentifier identifier) throws PatientFeedException;
}
