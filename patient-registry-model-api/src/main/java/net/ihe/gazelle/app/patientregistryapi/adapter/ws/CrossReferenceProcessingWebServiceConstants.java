package net.ihe.gazelle.app.patientregistryapi.adapter.ws;

/**
 * This class is used to store all constant values used by Patient Registry in X-ref Processing Service.
 */
public class CrossReferenceProcessingWebServiceConstants {

    public static final String CROSS_REFERENCE_PROCESSING_RESOURCE_URL = "xref-processing-service";
    public static final String CROSS_REFERENCE_PROCESSING_SERVICE = "CrossReferenceService";
    public static final String CROSS_REFERENCE_PROCESSING_SERVICE_PORT = "XRefProcessingServicePort";
    public static final String CROSS_REFERENCE_PATIENT_OUTPUT_NAME = "XRefPatient";
    public static final String CROSS_REFERENCE_PATIENT_SEARCH_OPERATION = "XRefSearch";
    public static final String CROSS_REFERENCE_INPUT_NAME = "CrossRefSourceIdentifier";
    public static final String CROSS_REFERENCE_INPUT_TARGET_NAME = "CrossRefTargetDomains";

    /**
     * Private constructor to hide implicit one.
     */
    private CrossReferenceProcessingWebServiceConstants() {
    }
}

