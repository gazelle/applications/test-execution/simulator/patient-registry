package net.ihe.gazelle.app.patientregistryapi.application;

public class SearchParameter {

    private String key;
    private String value;
    private String modifier;

    private String classType;


    public SearchParameter() {
    }

    public SearchParameter(String name, String modifier, String value, String classType) {
        this.key = name;
        this.value = value;
        this.modifier = modifier;
        this.classType = classType;

    }

    public String getKey() {
        return key;
    }

    public SearchParameter setKey(String key) {
        this.key = key;
        return this;
    }

    public String getValue() {
        return value;
    }

    public SearchParameter setValue(String value) {
        this.value = value;
        return this;
    }

    public String getModifier() {
        return modifier;
    }

    public SearchParameter setModifier(String modifier) {
        this.modifier = modifier;
        return this;
    }

    public String getClassType() {
        return classType;
    }

    public SearchParameter setClassType(String classType) {
        this.classType = classType;
        return this;
    }

    @Override
    public String toString() {
        return "SearchParameter{" +
                "name='" + key + '\'' +
                ", value='" + value + '\'' +
                ", modifier='" + modifier + '\'' +
                ", classType='" + classType + '\'' +
                '}';
    }
}
