package net.ihe.gazelle.app.patientregistryapi.application;

import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchService;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;

import java.util.List;

/**
 * Patient Search Service API. Used to search patients based on their demographics.
 */
public interface PatientSearchService extends SearchService<Patient> {

    /**
     * Retrieve all patient corresponding to the requested Criteria.
     *
     * @param searchCriteria Criteria to match
     * @return the list of patients matching criteria
     * @throws SearchException if an error happens during the search processing.
     */
    List<Patient> search(SearchCriteria searchCriteria) throws SearchException;

    /**
     * Retrieve all patient corresponding to the requested parameters.
     *
     * @param parameters List of parameters to match
     * @return the list of patients matching criteria
     * @throws SearchException if an error happens during the search processing.
     */
    List<Patient> parametrizedSearch(List<SearchParameter> parameters) throws SearchException;
}
