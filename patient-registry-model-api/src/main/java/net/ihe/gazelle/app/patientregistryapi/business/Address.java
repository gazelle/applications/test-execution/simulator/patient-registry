
package net.ihe.gazelle.app.patientregistryapi.business;

import java.util.ArrayList;
import java.util.List;


/**
 * Address of the patient
 */
public class Address {

    private String city;
    private String countryIso3;
    private String postalCode;
    private String state;
    private List<String> lines = new ArrayList<>();
    private AddressUse use;

    /**
     * default constructor
     */
    public Address() {
    }

    /**
     * Complete constructor
     * @param city address city
     * @param countryIso3 address country code
     * @param postalCode address postal code
     * @param state address state
     * @param use address patient use
     */
    public Address(String city, String countryIso3, String postalCode, String state, AddressUse use) {
        this.setCity(city);
        this.setCountryIso3(countryIso3);
        this.setPostalCode(postalCode);
        this.setState(state);
        this.setUse(use);
    }

    /**
     * get address city
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * set address city
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * get country iso code.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryIso3() {
        return countryIso3;
    }

    /**
     * Set country iso Code
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryIso3(String value) {
        this.countryIso3 = value;
    }

    /**
     * get address postal code
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostalCode() {
        return postalCode;
    }

    /**
     * set address postal code
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostalCode(String value) {
        this.postalCode = value;
    }

    /**
     * get address state
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getState() {
        return state;
    }

    /**
     * set address state
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setState(String value) {
        this.state = value;
    }

    /**
     * Gets the value of the lines property.
     *
     * <p>
     * This accessor method returns a snapshot to the live list.
     *
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     *
     * @return the list of lines for this address
     */
    public List<String> getLines() {
        return new ArrayList<>(this.lines);
    }


    /**
     * add a line to an address
     * @param line the line to add
     */
    public void addLine(String line){
        lines.add(line);
    }

    /**
     * add a line to an address
     * @param line the line to add
     * @param index the index to insert the line
     */
    public void addLine(String line, int index){
        this.lines.add(index, line);
    }

    /**
     * remove a line to an address
     * @param line the line to remove
     */
    public void removeLine(String line){
        this.lines.remove(line);
    }

    /**
     * get address use
     * 
     * @return
     *     possible object is
     *     {@link AddressUse }
     *     
     */
    public AddressUse getUse() {
        return use;
    }

    /**
     * set address use
     * 
     * @param value
     *     allowed object is
     *     {@link AddressUse }
     *     
     */
    public void setUse(AddressUse value) {
        this.use = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Address address = (Address) o;

        if (city != null ? !city.equals(address.city) : address.city != null) {
            return false;
        }
        if (countryIso3 != null ? !countryIso3.equals(address.countryIso3) : address.countryIso3 != null) {
            return false;
        }
        if (postalCode != null ? !postalCode.equals(address.postalCode) : address.postalCode != null) {
            return false;
        }
        if (state != null ? !state.equals(address.state) : address.state != null) {
            return false;
        }
        if (!lines.equals(address.lines)) {
            return false;
        }
        return use == address.use;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = city != null ? city.hashCode() : 0;
        result = 31 * result + (countryIso3 != null ? countryIso3.hashCode() : 0);
        result = 31 * result + (postalCode != null ? postalCode.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + lines.hashCode();
        result = 31 * result + (use != null ? use.hashCode() : 0);
        return result;
    }
}
