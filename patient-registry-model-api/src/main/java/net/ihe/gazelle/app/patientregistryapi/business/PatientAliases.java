package net.ihe.gazelle.app.patientregistryapi.business;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PatientAliases {

    private String uuid;
    private String lastModifier;
    private Date lastModifierDateTime;
    private List<Patient> members = new ArrayList<>();

    /**
     * default constructor
     */
    public PatientAliases() {
    }

    /**
     * Constructor for the class
     *
     * @param uuid                 the identifier of the patient aliases
     * @param lastModifier         the name of the last modifier
     * @param lastModifierDateTime the date time of the last update
     */
    public PatientAliases(String uuid, String lastModifier, Date lastModifierDateTime) {
        this.uuid = uuid;
        this.lastModifier = lastModifier;
        this.lastModifierDateTime = lastModifierDateTime;
    }

    /**
     * get uuid gazelle patientAliases
     *
     * @return string uuid
     */
    public String getUuid() {
        return uuid;
    }

    /**
     * set patientAliases gazelle identifier
     *
     * @param uuid the value to set to the uuid property
     */
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    /**
     * get the last modifier of the PatientAliases
     *
     * @return the name as String
     */
    public String getLastModifier() {
        return lastModifier;
    }

    /**
     * set the last modifier of the PatientAliases
     *
     * @param lastModifier the name of the last modifier
     */
    public void setLastModifier(String lastModifier) {
        this.lastModifier = lastModifier;
    }

    /**
     * get the Date Time of the last modifier
     *
     * @return the date time of the last modifier
     */
    public Date getLastModifierDateTime() {
        return lastModifierDateTime;
    }

    /**
     * set the Date Time of the last modifier
     */
    public void setLastModifierDateTime() {
        this.lastModifierDateTime = new Timestamp(new Date().getTime());
    }

    /**
     * get the list of the patients
     *
     * @return the list of the patients
     */
    public List<Patient> getMembers() {
        return members;
    }

    /**
     * Add a list of patient to the current list of patient if it is not already in the list
     *
     * @param patients
     */
    public void addMembers(List<Patient> patients) {
        if (patients != null) {
            for (Patient patient : patients) {
                setLastModifierDateTime();
                if (!this.members.contains(patient)) {
                    this.members.add(patient);
                }
            }
        }
    }

    public void setMembers(List<Patient> patients) {
        if (patients != null) {
            setLastModifierDateTime();
            this.members = patients;
        }
    }


    /**
     * add a patient to a members
     *
     * @param member the patient to add
     */
    public void addMember(Patient member) {
        setLastModifierDateTime();
        this.members.add(member);
    }

    /**
     * add a patient to a members
     *
     * @param member the patient to add
     * @param index  the index to insert the patient
     */
    public void addMember(Patient member, int index) {
        setLastModifierDateTime();
        this.members.add(index, member);
    }

    /**
     * remove a member to a list of members
     *
     * @param member the patient to remove
     */
    public void removeMember(Patient member) {
        setLastModifierDateTime();
        this.members.remove(member);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }

        PatientAliases patientAliases = (PatientAliases) o;

        if (uuid != null ? !uuid.equals(patientAliases.uuid) : patientAliases.uuid != null) {
            return false;
        }
        if (lastModifier != null ? !lastModifier.equals(patientAliases.lastModifier) : patientAliases.lastModifier != null) {
            return false;
        }
        if (lastModifierDateTime != null ? !lastModifierDateTime.equals(patientAliases.lastModifierDateTime) : patientAliases.lastModifierDateTime != null) {
            return false;
        }
        return members.equals(patientAliases.members);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (uuid != null ? uuid.hashCode() : 0);
        result = 31 * result + (lastModifier != null ? lastModifier.hashCode() : 0);
        result = 31 * result + (lastModifierDateTime != null ? lastModifierDateTime.hashCode() : 0);
        result = 31 * result + members.hashCode();
        return result;
    }
}
