package net.ihe.gazelle.app.patientregistryapi.adapter.ws;

/**
 *
 *This class is used to store all constant values used by Patient Registry  Patient Feed Processing Service.
 */
public class PatientProcessingWebserviceConstants {
    public static final String PATIENT_PROCESSING_RESOURCE_URL = "patient-processing-service";
    public static final String PATIENT_PROCESSING_SERVICE = "PatientProcessingService";
    public static final String PATIENT_PROCESSING_SERVICE_PORT = "PatientProcessingServicePort";
    public static final String PATIENT_FEED_OPERATION = "PatientFeed";
    public static final String PATIENT_SEARCH_OPERATION = "PatientSearch";
    public static final String PATIENT_PARAMETERS_SEARCH_OPERATION = "PatientParametersSearch";

    public static final String UUID_OUTPUT_NAME = "UUID";
    public static final String PATIENT_INPUT_NAME = "Patient";

    public static final String PATIENT_IDENTIFIER_INPUT_NAME = "PatientIdentifier";
    public static final String PATIENT_OUTPUT_NAME = "Patient";
    public static final String DELETE_STATUS_OUTPUT_NAME = "Delete Status";

    public static final String DELETED_IDENTIFIER_OUTPUT_NAME = "Deleted Identifier";
    public static final String PATIENT_SEARCH_CRITERIA_INPUT_NAME = "PatientSearchCriteria";

    public static final String PATIENT_PARAMETERS_SEARCH_INPUT_NAME = "PatientParametersSearch";

    public static final String PATIENT_LIST_OUTPUT_NAME = "PatientList";
    public static final String PATIENT_CREATE_OPERATION = "PatientCreate";
    public static final String PATIENT_UPDATE_OPERATION = "PatientUpdate";
    public static final String PATIENT_DELETE_OPERATION = "PatientDelete";
    public static final String PATIENT_IDENTIFIER_DELETE_OPERATION = "PatientIdentifierDelete";
    public static final String PATIENT_MERGE_OPERATION = "PatientMerge";
    public static final String UUID_PATIENT_TO_DELETE_INPUT_NAME ="UUIDOfPatientToDelete" ;

    public static final String IDENTIFIER_PATIENT_TO_DELETE_INPUT_NAME ="IdentifierOfPatientToDelete" ;
    public static final String UUID_OF_ORIGINAL_PATIENT_INPUT_NAME = "UUIDOfOriginalPatient";
    public static final String UUID_OF_DUPLICATED_PATIENT_INPUT_NAME = "UUIDOfDuplicatedPatient";
    public static final String DELETE_STATUS_DONE = "Done";
    public static final String DELETE_STATUS_GONE = "Gone";


    /**
     * Private constructor to hide implicit one.
     */
    private PatientProcessingWebserviceConstants(){
    }
}
