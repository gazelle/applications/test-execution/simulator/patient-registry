
package net.ihe.gazelle.app.patientregistryapi.business;


/**
 * Possible contact points type for the patient
 */
public enum ContactPointType {

    PHONE,
    FAX,
    MODEM,
    MOBILE,
    BEEPER,
    EMAIL,
    DEAF_DEVICE,
    URL,
    OTHER,
    SMS;

    /**
     * get enum member name
     * @return enum member name
     */
    public String value() {
        return name();
    }

}
