package net.ihe.gazelle.app.patientregistryapi.application;

import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.PatientAliases;

import java.util.List;


/**
 * Patient Cross Reference Search Service API
 */
public interface PatientCrossReferenceSearch {

    /**
     * retrieve the patient alias corresponding to the requested
     *
     * @param sourceIdentifier identifier of the reference patient
     * @param targetDomains    list of TargetDomains, the list cardinality must be [0..*]
     * @return a patient aliases that match the patient with sourceIdentifier
     * @throws SearchCrossReferenceException if an error happens during the search processing.
     */
    PatientAliases search(EntityIdentifier sourceIdentifier, List<String> targetDomains) throws SearchCrossReferenceException;

}
