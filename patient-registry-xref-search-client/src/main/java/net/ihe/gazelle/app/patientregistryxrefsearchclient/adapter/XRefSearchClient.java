package net.ihe.gazelle.app.patientregistryxrefsearchclient.adapter;

import com.gitb.core.AnyContent;
import com.gitb.ps.ProcessRequest;
import com.gitb.ps.ProcessResponse;
import com.gitb.ps.ProcessingService;
import com.gitb.tr.BAR;
import com.gitb.tr.TAR;
import com.gitb.tr.TestResultType;
import net.ihe.gazelle.app.patientregistryapi.adapter.ws.CrossReferenceProcessingWebServiceConstants;
import net.ihe.gazelle.app.patientregistryapi.application.PatientCrossReferenceSearch;
import net.ihe.gazelle.app.patientregistryapi.application.SearchCrossReferenceException;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.PatientAliases;
import net.ihe.gazelle.lib.gitbprocessingclient.adapter.GITBClientProcessImpl;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperAnyContentToObject;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperObjectToAnyContent;
import net.ihe.gazelle.lib.gitbutils.adapter.MappingException;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;

import java.net.URL;
import java.util.List;


public class XRefSearchClient implements PatientCrossReferenceSearch {

    private ProcessingService processingClient;

    /**
     * Constructor used for test purposes.
     *
     * @param processingService : processing service to be used by the XRefSearchClient.
     */
    public XRefSearchClient(ProcessingService processingService) {
        this.processingClient = processingService;
    }

    /**
     * Default constructor for the class. The Client will create the GITB Processing Service client based on the URL.
     *
     * @param processingServiceURL : URL of the remote Processing Service.
     */
    public XRefSearchClient(URL processingServiceURL) {
        this.processingClient = new GITBClientProcessImpl(processingServiceURL,
                CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PROCESSING_SERVICE,
                CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PROCESSING_SERVICE_PORT);
    }

    @Override
    public PatientAliases search(EntityIdentifier sourceIdentifier, List<String> targetDomains) throws SearchCrossReferenceException {
        ProcessRequest processRequest = new ProcessRequest();
        processRequest.setOperation(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_SEARCH_OPERATION);

        try {
            AnyContent sourceIdentifierAnyContent =
                    new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_NAME,
                            sourceIdentifier);
            AnyContent targetDomainsAnyContent =
                    new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_INPUT_TARGET_NAME,
                            targetDomains);
            processRequest.getInput().add(sourceIdentifierAnyContent);
            processRequest.getInput().add(targetDomainsAnyContent);
            ProcessResponse processResponse = processingClient.process(processRequest);
            return extractPatientAliasesFromProcessResponse(processResponse);
        } catch (MappingException e) {
            throw new SearchCrossReferenceException("Exception while Mapping with GITB elements !", e);
        } catch (XRefSearchProcessResponseException e) {
            throw new SearchCrossReferenceException("Invalid Response from distant CrossReferenceSearchProcessingService !", e);
        } catch (UnsupportedOperationException e) {
            throw new SearchCrossReferenceException("Invalid operation used on distant CrossReferenceSearchProcessingService !", e);
        } catch (IllegalArgumentException | SearchCrossReferenceException e) {
            throw new SearchCrossReferenceException("Invalid Request sent to distant CrossReferenceSearchProcessingService !", e);
        }
    }


    /**
     * Extract the list of Patients from the ProcessResponse received from the remote ProcessingService.
     *
     * @param processResponse : response received.
     * @return a PatientAliases of {@link PatientAliases} returned by the ProcessingService.
     * @throws XRefSearchProcessResponseException : if the ProcessResponse is not valid for a XRef search request.
     * @throws SearchException                    : if the X-ref Search raised an exception in the remote Repository.
     */
    private PatientAliases extractPatientAliasesFromProcessResponse(ProcessResponse processResponse) throws XRefSearchProcessResponseException,
            SearchCrossReferenceException {
        if (processResponse == null) {
            throw new XRefSearchProcessResponseException("Empty Response from the distant CrossReferenceSearchProcessingService !");
        } else {
            if (processResponse.getReport() != null) {
                if (TestResultType.SUCCESS.equals(processResponse.getReport().getResult())) {
                    return extractPatientAliasesListFromOutputs(processResponse);
                } else if (TestResultType.FAILURE.equals(processResponse.getReport().getResult())) {
                    throw createExceptionFromProcessResponseReport(processResponse.getReport());
                } else {
                    throw new XRefSearchProcessResponseException(String.format("Processing response with unexpected type %s ! Expected type is " +
                            "either %s or %s.", processResponse.getReport().getResult(), TestResultType.SUCCESS, TestResultType.FAILURE));
                }
            } else {
                throw new XRefSearchProcessResponseException("Response from the distant CrossReferenceSearchProcessingService shall contain a " +
                        "report with " +
                        "the success status of the operation !");
            }
        }
    }

    private PatientAliases extractPatientAliasesListFromOutputs(ProcessResponse processResponse) throws XRefSearchProcessResponseException {
        if (processResponse.getOutput().size() == 1) {
            try {
                PatientAliases outputPatientAliases = new MapperAnyContentToObject().getObject(processResponse.getOutput().get(0),
                        PatientAliases.class);
                if (outputPatientAliases != null) {
                    return outputPatientAliases;
                }
                throw new XRefSearchProcessResponseException();
            } catch (MappingException e) {
                throw new XRefSearchProcessResponseException("Error while mapping processing output from distant " +
                        "CrossReferenceSearchProcessingService " +
                        "to PatientAliases !");
            }
        } else {
            throw new XRefSearchProcessResponseException("Response from the distant CrossReferenceSearchProcessingService shall contain a single " +
                    "output for PatientAliases !");
        }
    }

    /**
     * Extract SearchException from ProcessResponse report.
     *
     * @param report : report found in the received response.
     * @return {@link SearchException} to be thrown by the {@link XRefSearchClient}
     * @throws XRefSearchProcessResponseException : if the report is not well formed.
     */
    private SearchCrossReferenceException createExceptionFromProcessResponseReport(TAR report) throws XRefSearchProcessResponseException {
        if (report.getReports() == null || report.getReports().getInfoOrWarningOrError().size() != 1) {
            throw new XRefSearchProcessResponseException("The report from the ProcessResponse shall not be null and shall contain a single error.");
        } else {
            try {
                BAR error = (BAR) report.getReports().getInfoOrWarningOrError().get(0).getValue();
                if (error.getDescription() != null && !error.getDescription().isEmpty()) {
                    return new SearchCrossReferenceException(error.getDescription());
                } else {
                    throw new XRefSearchProcessResponseException("Error from ProcessResponse report must have a valid description !");
                }
            } catch (ClassCastException e) {
                throw new XRefSearchProcessResponseException("Cannot decode error from ProcessResponse report !");
            }
        }
    }
}
