package net.ihe.gazelle.app.patientregistryxrefsearchclient.adapter;

import net.ihe.gazelle.app.patientregistryapi.application.SearchCrossReferenceException;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.PatientAliases;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class XRefSearchClientTest {

    private XRefSearchClient xRefSearchClient;
    private ProcessingServiceMock processingServiceMock;


    /**
     * Initialization of the tested {@link XRefSearchClient}.
     * It is configured to use the mocked remote service {@link ProcessingServiceMock}
     */
    @BeforeEach
    public void init() {
        processingServiceMock = new ProcessingServiceMock();
        xRefSearchClient = new XRefSearchClient(processingServiceMock);
    }


    /**
     * Test X-ref client getting successful response
     *
     * @throws SearchCrossReferenceException
     */
    @Test
    void xRef_search_With_Success() throws SearchCrossReferenceException {
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("test");
        entityIdentifier.setValue("test");
        List<String> targetDomain = new ArrayList<>();
        PatientAliases patientAliases = xRefSearchClient.search(entityIdentifier, targetDomain);
        assertEquals("123456789", patientAliases.getUuid(), "The result uuid shall be equal to the mocked one's : '123456789'");

    }

    /**
     * Test x-Ref Search client getting a response with no report
     */
    @Test
    void xRef_search_With_Success_missing_report() {
        processingServiceMock.setResponseToReturn(ProcessingServiceMock.ResponseTypes.MISSING_REPORT);
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("test");
        entityIdentifier.setValue("test");
        List<String> targetDomain = new ArrayList<>();
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class, () -> xRefSearchClient.search(entityIdentifier,
                targetDomain));
        assertEquals("Response from the distant CrossReferenceSearchProcessingService shall contain a report with " +
                "the success status of the operation !", exception.getCause().getMessage());
    }


    /**
     * Test x-Ref Search client getting a response with failed report
     */
    @Test
    void xRef_search_With_Success_failed_report() {
        processingServiceMock.setResponseToReturn(ProcessingServiceMock.ResponseTypes.FAILED_REPORT);
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("test");
        entityIdentifier.setValue("test");
        List<String> targetDomain = new ArrayList<>();
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class, () -> xRefSearchClient.search(entityIdentifier,
                targetDomain));
        assertEquals("Invalid Request sent to distant CrossReferenceSearchProcessingService !", exception.getMessage());
    }

    /**
     * Test xRef_search client getting a response with multiple outputs
     */
    @Test
    void xRef_search_With_Success_multiple_output() {
        processingServiceMock.setResponseToReturn(ProcessingServiceMock.ResponseTypes.MULTIPLE_OUTPUT);
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("test");
        entityIdentifier.setValue("test");
        List<String> targetDomain = new ArrayList<>();
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class, () -> xRefSearchClient.search(entityIdentifier,
                targetDomain));
        assertEquals("Response from the distant CrossReferenceSearchProcessingService shall contain a single output for PatientAliases !",
                exception.getCause().getMessage());
    }

    /**
     * Test xRef_search client getting a response with invalid output (not a PatientAliases)
     */
    @Test
    void xRef_search_With_Success_invalid_output() {
        processingServiceMock.setResponseToReturn(ProcessingServiceMock.ResponseTypes.INVALID_OUTPUT);
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("test");
        entityIdentifier.setValue("test");
        List<String> targetDomain = new ArrayList<>();
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class, () -> xRefSearchClient.search(entityIdentifier,
                targetDomain));
        assertEquals("Error while mapping processing output from distant CrossReferenceSearchProcessingService " +
                "to PatientAliases !", exception.getCause().getMessage());
    }

    /**
     * Test xRef_search client getting a null response
     */
    @Test
    void xRef_search_null_response() {
        processingServiceMock.setResponseToReturn(ProcessingServiceMock.ResponseTypes.NULL_RESPONSE);
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("test");
        entityIdentifier.setValue("test");
        List<String> targetDomain = new ArrayList<>();
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class, () -> xRefSearchClient.search(entityIdentifier,
                targetDomain));
        assertEquals("Empty Response from the distant CrossReferenceSearchProcessingService !", exception.getCause().getMessage());
    }


    /**
     * Test xRef_search  client getting a response with an unexpected status
     */
    @Test
    void xRef_search_unexpected_processing_status() {
        processingServiceMock.setResponseToReturn(ProcessingServiceMock.ResponseTypes.UNEXPECTED_STATUS);
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("test");
        entityIdentifier.setValue("test");
        List<String> targetDomain = new ArrayList<>();
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class, () -> xRefSearchClient.search(entityIdentifier,
                targetDomain));
        assertEquals("Processing response with unexpected type WARNING ! Expected type is " +
                "either SUCCESS or FAILURE.", exception.getCause().getMessage());
    }

    /**
     * Test xRef_search client getting a response with no error report in report
     */
    @Test
    void xRef_search_no_reports_in_report() {
        processingServiceMock.setResponseToReturn(ProcessingServiceMock.ResponseTypes.NO_REPORTS_IN_REPORT);
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("test");
        entityIdentifier.setValue("test");
        List<String> targetDomain = new ArrayList<>();
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class, () -> xRefSearchClient.search(entityIdentifier,
                targetDomain));
        assertEquals("The report from the ProcessResponse shall not be null and shall contain a single error.", exception.getCause().getMessage());
    }

    /**
     * Test xRef_search client getting a response with no error in a failure report
     */
    @Test
    void xRef_search_no_error_in_failure_report() {
        processingServiceMock.setResponseToReturn(ProcessingServiceMock.ResponseTypes.NO_ERROR_IN_FAILURE_REPORT);
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("test");
        entityIdentifier.setValue("test");
        List<String> targetDomain = new ArrayList<>();
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class, () -> xRefSearchClient.search(entityIdentifier,
                targetDomain));
        assertEquals("The report from the ProcessResponse shall not be null and shall contain a single error.", exception.getCause().getMessage());
    }

    /**
     * Test xRef_search client getting a response with an error with no description
     */
    @Test
    void xRef_search_no_error_description() {
        processingServiceMock.setResponseToReturn(ProcessingServiceMock.ResponseTypes.NO_ERROR_DESCRIPTION);
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("test");
        entityIdentifier.setValue("test");
        List<String> targetDomain = new ArrayList<>();
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class, () -> xRefSearchClient.search(entityIdentifier,
                targetDomain));
        assertEquals("Error from ProcessResponse report must have a valid description !", exception.getCause().getMessage());
    }

    /**
     * Test xRef_search client getting an IllegalArgumentException
     */
    @Test
    void xRef_search_illegalArgumentException() {
        processingServiceMock.setResponseToReturn(ProcessingServiceMock.ResponseTypes.ILLEGAL_ARGUMENT_EXCEPTION);
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("test");
        entityIdentifier.setValue("test");
        List<String> targetDomain = new ArrayList<>();
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class, () -> xRefSearchClient.search(entityIdentifier,
                targetDomain));
        assertEquals("Invalid Request sent to distant CrossReferenceSearchProcessingService !", exception.getMessage());
    }

    /**
     * Test xRef_search client getting an UnsupportedOperationException
     */
    @Test
    void xRef_search_unsupportedOperationException() {
        processingServiceMock.setResponseToReturn(ProcessingServiceMock.ResponseTypes.UNSUPPORTED_OPERATION_EXCEPTION);
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        entityIdentifier.setSystemIdentifier("test");
        entityIdentifier.setValue("test");
        List<String> targetDomain = new ArrayList<>();
        SearchCrossReferenceException exception = assertThrows(SearchCrossReferenceException.class, () -> xRefSearchClient.search(entityIdentifier,
                targetDomain));
        assertEquals("Invalid operation used on distant CrossReferenceSearchProcessingService !", exception.getMessage());
    }

}