package net.ihe.gazelle.app.patientregistryxrefsearchclient.adapter;

import com.gitb.core.AnyContent;
import com.gitb.ps.Void;
import com.gitb.ps.*;
import com.gitb.tr.*;
import com.gitb.tr.ObjectFactory;
import net.ihe.gazelle.app.patientregistryapi.adapter.ws.CrossReferenceProcessingWebServiceConstants;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PatientAliases;
import net.ihe.gazelle.app.patientregistryapi.business.PersonName;
import net.ihe.gazelle.lib.gitbutils.adapter.MapperObjectToAnyContent;
import net.ihe.gazelle.lib.gitbutils.adapter.MappingException;

import java.math.BigInteger;

import static net.ihe.gazelle.app.patientregistryapi.adapter.ws.PatientProcessingWebserviceConstants.PATIENT_INPUT_NAME;
import static net.ihe.gazelle.app.patientregistryapi.adapter.ws.PatientProcessingWebserviceConstants.UUID_OUTPUT_NAME;
import static org.junit.jupiter.api.Assertions.fail;

public class ProcessingServiceMock implements ProcessingService {


    private ResponseTypes responseToReturn;

    /**
     * Default constructor for the class. By default, the service returns a valid success answer.
     */
    public ProcessingServiceMock() {
        responseToReturn = ResponseTypes.CORRECT;
    }

    /**
     * Setter to the responseToReturn property
     *
     * @param responseToReturn
     */
    public void setResponseToReturn(ResponseTypes responseToReturn) {
        this.responseToReturn = responseToReturn;
    }

    @Override
    public GetModuleDefinitionResponse getModuleDefinition(Void parameters) {
        return null;
    }

    @Override
    public ProcessResponse process(ProcessRequest parameters) {

        switch (responseToReturn) {
            case CORRECT:
                return getCorrectProcessResponse();
            case MISSING_REPORT:
                return getResponseMissingReport();
            case FAILED_REPORT:
                return getResponseWithFailedReport("MyError");
            case MULTIPLE_OUTPUT:
                return getResponseWithMultipleOutputs();
            case INVALID_OUTPUT:
                return getResponseWithInvalidOutput();
            case NULL_RESPONSE:
                return null;
            case UNEXPECTED_STATUS:
                return getResponseWithUnexpectedReportStatus();
            case NO_REPORTS_IN_REPORT:
                return getResponseWithoutReports();
            case NO_ERROR_IN_FAILURE_REPORT:
                return getResponseWithoutErrorInFailureReport();
            case NO_ERROR_DESCRIPTION:
                return getResponseWithFailedReport(null);
            case ILLEGAL_ARGUMENT_EXCEPTION:
                throw new IllegalArgumentException();
            case UNSUPPORTED_OPERATION_EXCEPTION:
                throw new UnsupportedOperationException();
            default:
                return null;
        }
    }

    /**
     * Return a correct {@link ProcessResponse} to a X-ref search Request
     *
     * @return a correct {@link ProcessResponse}
     */
    private ProcessResponse getResponseWithoutErrorInFailureReport() {
        ProcessResponse processResponse = new ProcessResponse();

        TAR report = new TAR();
        report.setResult(TestResultType.FAILURE);
        report.setReports(new TestAssertionGroupReportsType());

        report.setCounters(new ValidationCounters());
        report.getCounters().setNrOfAssertions(BigInteger.valueOf(0));
        report.getCounters().setNrOfWarnings(BigInteger.valueOf(0));
        report.getCounters().setNrOfErrors(BigInteger.valueOf(0));

        report.setContext(new AnyContent());
        report.getContext().getItem().add(new AnyContent());
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_OUTPUT_NAME, createPatientAliases()));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    private ProcessResponse getResponseWithoutReports() {
        ProcessResponse processResponse = new ProcessResponse();

        TAR report = new TAR();
        report.setResult(TestResultType.FAILURE);
        report.setReports(new TestAssertionGroupReportsType());

        report.setContext(new AnyContent());
        report.getContext().getItem().add(new AnyContent());
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_OUTPUT_NAME, createPatientAliases()));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    private ProcessResponse getResponseWithUnexpectedReportStatus() {
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.WARNING);
        processResponse.setReport(report);

        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_OUTPUT_NAME, createPatientAliases()));
        } catch (MappingException e) {
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    private ProcessResponse getResponseWithInvalidOutput() {
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.SUCCESS);
        processResponse.setReport(report);
        PersonName personName = new PersonName();
        personName.setFamily("Bars");
        try {
            AnyContent anyContent = new MapperObjectToAnyContent().getAnyContent(PATIENT_INPUT_NAME, personName);
            processResponse.getOutput().add(anyContent);
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    private ProcessResponse getResponseWithMultipleOutputs() {
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.SUCCESS);
        processResponse.setReport(report);

        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_OUTPUT_NAME, createPatientAliases()));
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_OUTPUT_NAME, createPatientAliases()));
        } catch (MappingException e) {
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    private ProcessResponse getResponseWithFailedReport(String myError) {
        com.gitb.tr.ObjectFactory objectFactory = new ObjectFactory();
        ProcessResponse processResponse = new ProcessResponse();

        TAR report = new TAR();
        report.setResult(TestResultType.FAILURE);
        report.setReports(new TestAssertionGroupReportsType());

        BAR itemContent = new BAR();
        itemContent.setDescription(myError);
        report.getReports().getInfoOrWarningOrError().add(objectFactory.createTestAssertionGroupReportsTypeError(itemContent));

        report.setCounters(new ValidationCounters());
        report.getCounters().setNrOfAssertions(BigInteger.valueOf(0));
        report.getCounters().setNrOfWarnings(BigInteger.valueOf(0));
        report.getCounters().setNrOfErrors(BigInteger.valueOf(1));

        report.setContext(new AnyContent());
        report.getContext().getItem().add(new AnyContent());
        processResponse.setReport(report);
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_OUTPUT_NAME, createPatientAliases()));
        } catch (MappingException e){
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Return a {@link ProcessResponse} with no Report
     *
     * @return a {@link ProcessResponse} with no report
     */
    private ProcessResponse getResponseMissingReport() {
        ProcessResponse processResponse = new ProcessResponse();
        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_OUTPUT_NAME, createPatientAliases()));
        } catch (MappingException e) {
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    /**
     * Return a correct {@link ProcessResponse} to a x-ref Search Process Request
     * @return a correct {@link ProcessResponse}
     */
    private ProcessResponse getCorrectProcessResponse() {
        ProcessResponse processResponse = new ProcessResponse();
        TAR report = new TAR();
        report.setResult(TestResultType.SUCCESS);
        processResponse.setReport(report);

        try {
            processResponse.getOutput().add(new MapperObjectToAnyContent().getAnyContent(CrossReferenceProcessingWebServiceConstants.CROSS_REFERENCE_PATIENT_OUTPUT_NAME, createPatientAliases()));
        } catch (MappingException e) {
            fail("Unexpected Mapping exception !", e);
        }
        return processResponse;
    }

    @Override
    public BeginTransactionResponse beginTransaction(BeginTransactionRequest parameters) {
        return null;
    }

    @Override
    public Void endTransaction(BasicRequest parameters) {
        return null;
    }

    /**
     * Enumeration of all types of response this fake service can return
     */
    public enum ResponseTypes {
        CORRECT,
        MISSING_REPORT,
        FAILED_REPORT,
        MULTIPLE_OUTPUT,
        INVALID_OUTPUT,
        NULL_RESPONSE,
        UNEXPECTED_STATUS,
        NO_REPORTS_IN_REPORT,
        NO_ERROR_IN_FAILURE_REPORT,
        NO_ERROR_DESCRIPTION,
        ILLEGAL_ARGUMENT_EXCEPTION,
        UNSUPPORTED_OPERATION_EXCEPTION;
    }

    private PatientAliases createPatientAliases() {
        PatientAliases patientAliases = new PatientAliases();
        patientAliases.setUuid("123456789");
        Patient patient = new Patient();
        PersonName personName = new PersonName();
        personName.setFamily("TestName");
        patient.addName(personName);
        patientAliases.addMember(patient);
        return patientAliases;
    }
}
